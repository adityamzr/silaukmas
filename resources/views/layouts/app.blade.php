@props(['title'])
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
  <meta name="description" content="deskrisi tentang silaukmas"/>
  <link href="{{ asset('css/app.css') }}" rel="stylesheet">
  <link rel="icon" type="image/x-icon" href="/assets/images/LogoBanten.jpg">
  <link href="https://fonts.googleapis.com/css2?family=Bebas+Neue&family=Poppins:wght@200;400;500;600;700&display=swap" rel="stylesheet">
  <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
  {{ $css ?? '' }}
  {!! ReCaptcha::htmlScriptTagJsApi() !!}
  
  <title>{{ $title ?? '' }}</title>
</head>
<body>
    <div class="font-poppins"  >
        <div x-data="data()" class="h-screen flex overflow-hidden " x-cloak @keydown.window.escape="sidemenu = false">
        
            <x-sidebar-layout/>
 
         <div class="flex-1 flex-col relative z-0 overflow-y-auto bg-[#F6F7FB]">
 
             <x-top-navbar-layout/>
         
             <div  class="md:max-w-7xl md:mx-auto px-4 md:px-8 py-12">
                 {{ $slot }}

                 
             </div>
         </div>
     </div>
     <script>
         function data(){
             return {
                sidemenu: false,
                modal: false,
             }

         }
     </script>
    </div>


    <div class="absolute right-5 top-5">
        {{ $alert ?? '' }}
    </div>
    <div class="">
        {{ $modal ?? '' }}
    </div>
    <script src="{{ mix('js/app.js') }}"></script>

    {{ $script ?? '' }}

   

    
    

</body>
</html>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <meta name="description" content="deskrisi tentang silaukmas"/>
  <link href="{{ asset('css/app.css') }}" rel="stylesheet">
  <link rel="stylesheet" href="https://unpkg.com/aos@next/dist/aos.css" />
  <link rel="icon" type="image/x-icon" href="/assets/images/LogoBanten.jpg">
  <link href="https://fonts.googleapis.com/css2?family=Bebas+Neue&family=Poppins:wght@200;400;500;600;700&display=swap" rel="stylesheet">  
  <title>Slaukmas</title>
  {!! NoCaptcha::renderJs() !!}
</head>
<body>
    <div class="font-poppins">
            {{ $slot }}
    </div>

    <div class="absolute right-5 top-5">
      {{ $alert ?? '' }}
  </div>

    <script src="{{ mix('js/app.js') }}"></script>
    
    <script src="https://unpkg.com/aos@next/dist/aos.js"></script>
    <script>
      AOS.init();
    </script>
</body>
</html>
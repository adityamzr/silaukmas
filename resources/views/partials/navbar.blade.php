<nav x-data="{open : true}" class="bg-white border-gray-200 px-2 sm:px-4 py-4 rounded ">
    <div class="container flex flex-wrap justify-between items-center mx-auto">
        <div class="flex">
            <div class=" max-w-[60px] lg:max-w-[80px] w-full h-full mr-6">
                <img src="assets/banten_logo.webp" class="object-contain" alt="banten">
            </div>
            <a href="#" class="flex items-center">
                <x-aplication-logo class="mr-3 max-h-8 lg:max-h-14 h-full w-full"/>
                <span class="self-center text-xl lg:text-3xl font-semibold whitespace-nowrap ">Silaukmas</span>
            </a>
        </div>
        <button @click="open = ! open" type="button"
            class="inline-flex items-center p-2 ml-3 text-sm text-gray-500 rounded-lg md:hidden hover:bg-gray-100 focus:outline-none focus:ring-2 focus:ring-gray-200 "
            aria-controls="mobile-menu" aria-expanded="false">
            <span class="sr-only">Open main menu</span>
            <svg class="w-6 h-6" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
                <path fill-rule="evenodd"
                    d="M3 5a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1zM3 10a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1zM3 15a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1z"
                    clip-rule="evenodd"></path>
            </svg>
            <svg class="hidden w-6 h-6" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
                <path fill-rule="evenodd"
                    d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z"
                    clip-rule="evenodd"></path>
            </svg>
        </button>
        <div :class="{'block': ! open, 'hidden': open}" class="w-full md:block md:w-auto" >
            <ul class="flex flex-col mt-4 md:flex-row md:space-x-8 md:mt-0 md:text-sm md:font-medium md:items-center">

                <li>
                    <a href="#"
                        class=" text-lg block py-2 pr-4 pl-3 text-gray-700 border-b hover:text-gray-800 border-gray-100 hover:bg-gray-50 md:hover:bg-transparent md:border-0 md:p-0 ">Home</a>
                </li>
                <li>
                    <a href="#about"
                        class=" text-lg block py-2 pr-4 pl-3 text-gray-700 border-b hover:text-gray-800 border-gray-100 hover:bg-gray-50 md:hover:bg-transparent md:border-0 md:p-0 ">About</a>
                </li>
                <li>
                    <a href="#tahapan"
                        class=" text-lg block py-2 pr-4 pl-3 text-gray-700 border-b hover:text-gray-800 border-gray-100 hover:bg-gray-50 md:hover:bg-transparent md:border-0 md:p-0 ">Tahapan</a>
                </li>
                <li>
                    <a href="#faq"
                        class=" text-lg block py-2 pr-4 pl-3 text-gray-700 border-b hover:text-gray-800 border-gray-100 hover:bg-gray-50 md:hover:bg-transparent md:border-0 md:p-0 ">FAQ</a>
                </li>
                <li>
                    @auth
                        <a href="{{ (auth()->user()->is_admin == 1 )? '/dashboard' : '/pengambilan-benih' }}" class=" text-lg block py-2 px-3 text-white "><button
                            class="px-3 py-2 bg-primary rounded">Masuk</button>
                        </a>    
                    @else
                        <a href="/login" class=" text-lg block py-2 px-3 text-white "><button
                            class="px-3 py-2 bg-primary rounded">Masuk</button>
                        </a>
                    @endauth
                    
                </li>
            </ul>
        </div>
    </div>
</nav>
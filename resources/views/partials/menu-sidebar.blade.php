<div {{ $attributes->merge(['class' => '']) }}>
    {{-- Profile Admin --}}
    <div class="text-center bg-[#2872E2] py-7 space-y-3 flex-col text-white">
        <div class="max-w-[95px] w-full h-full mx-auto space-y-">
            <img class="w-full h-full object-contain" src="/assets/images/profile.webp" alt="">
        </div>
        <div>
            <h2 class=" uppercase text-2xl font-semibold">{{ auth()->user()->namaLengkap }}</h2>
            <p class="text-xs">{{ (auth()->user()->is_admin == 1 )? 'Admin' : 'Pemohon' }}</p>
            <a href="/edit-profile/{{ auth()->user()->id }}" class="flex text-xs justify-center items-center mx-auto mt-2">
                <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-5" viewBox="0 0 20 20" fill="currentColor">
                    <path fill-rule="evenodd" d="M11.49 3.17c-.38-1.56-2.6-1.56-2.98 0a1.532 1.532 0 01-2.286.948c-1.372-.836-2.942.734-2.106 2.106.54.886.061 2.042-.947 2.287-1.561.379-1.561 2.6 0 2.978a1.532 1.532 0 01.947 2.287c-.836 1.372.734 2.942 2.106 2.106a1.532 1.532 0 012.287.947c.379 1.561 2.6 1.561 2.978 0a1.533 1.533 0 012.287-.947c1.372.836 2.942-.734 2.106-2.106a1.533 1.533 0 01.947-2.287c1.561-.379 1.561-2.6 0-2.978a1.532 1.532 0 01-.947-2.287c.836-1.372-.734-2.942-2.106-2.106a1.532 1.532 0 01-2.287-.947zM10 13a3 3 0 100-6 3 3 0 000 6z" clip-rule="evenodd" />
                  </svg> Edit Profile
            </a>
        </div>
    </div>
    {{-- /Profile Admin --}}
    <ul>
        @if (auth()->user()->is_admin == 1)
        <li
            class="relative px-4 text-base font-normal hover:bg-[#194990] py-1 {{ Request::is('dashboard') ? 'nav-active' : '' }}">
            <span class="{{ Request::is('dashboard') ? 'nav-style' : 'hidden' }}" aria-hidden="true"></span>
            <a href="/dashboard" class="px-2 py-2 rounded-lg flex items-center text-white">
                <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-5" viewBox="0 0 20 20" fill="currentColor">
                    <path d="M10.707 2.293a1 1 0 00-1.414 0l-7 7a1 1 0 001.414 1.414L4 10.414V17a1 1 0 001 1h2a1 1 0 001-1v-2a1 1 0 011-1h2a1 1 0 011 1v2a1 1 0 001 1h2a1 1 0 001-1v-6.586l.293.293a1 1 0 001.414-1.414l-7-7z" />
                </svg>
                <span class="ml-4">Dahboard</span>
            </a>
        </li>
        <li
            class="relative px-4 text-base font-normal hover:bg-[#194990] py-1 {{ Request::is('data-proposal*') ? 'nav-active' : '' }}">
            <span class="{{ Request::is('data-proposal*') ? 'nav-style' : 'hidden' }}" aria-hidden="true"></span>
            <a href="/data-proposal" class="px-2 py-2 rounded-lg flex items-center text-white">
                <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-5" viewBox="0 0 20 20" fill="currentColor">
                    <path fill-rule="evenodd" d="M18 10a8 8 0 11-16 0 8 8 0 0116 0zm-6-3a2 2 0 11-4 0 2 2 0 014 0zm-2 4a5 5 0 00-4.546 2.916A5.986 5.986 0 0010 16a5.986 5.986 0 004.546-2.084A5 5 0 0010 11z" clip-rule="evenodd" />
                </svg>
                <span class="ml-4">Data Proposal</span>
            </a>
        </li>

        <li
            class="relative px-4 text-base font-normal hover:bg-[#194990] py-1 {{ Request::is('maps*') ? 'nav-active' : '' }}">
            <span class="{{ Request::is('maps*') ? 'nav-style' : 'hidden' }}" aria-hidden="true"></span>
            <a href="/maps" class="px-2 py-2 rounded-lg flex items-center text-white">
                <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-5" viewBox="0 0 20 20" fill="currentColor">
                    <path fill-rule="evenodd" d="M12 1.586l-4 4v12.828l4-4V1.586zM3.707 3.293A1 1 0 002 4v10a1 1 0 00.293.707L6 18.414V5.586L3.707 3.293zM17.707 5.293L14 1.586v12.828l2.293 2.293A1 1 0 0018 16V6a1 1 0 00-.293-.707z" clip-rule="evenodd" />
                  </svg>
                <span class="ml-4">Lokasi Pemohon</span>
            </a>
        </li>
        <li
            class="relative px-4 text-base font-normal hover:bg-[#194990] py-1 {{ Request::is('data-master*') ? 'nav-active' : '' }}">
            <span class="{{ Request::is('data-master*') ? 'nav-style' : 'hidden' }}" aria-hidden="true"></span>
            <a href="/data-master" class="px-2 py-2 rounded-lg flex items-center text-white">
                <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-5" viewBox="0 0 20 20" fill="currentColor">
                    <path d="M3 12v3c0 1.657 3.134 3 7 3s7-1.343 7-3v-3c0 1.657-3.134 3-7 3s-7-1.343-7-3z" />
                    <path d="M3 7v3c0 1.657 3.134 3 7 3s7-1.343 7-3V7c0 1.657-3.134 3-7 3S3 8.657 3 7z" />
                  <path d="M17 5c0 1.657-3.134 3-7 3S3 6.657 3 5s3.134-3 7-3 7 1.343 7 3z" />
                  </svg>
                <span class="ml-4">Data Master</span>
            </a>
        </li>
        <li
            class="relative px-4 text-base font-normal hover:bg-[#194990] py-1 {{ Request::is('angket-kepuasan*') ? 'nav-active' : '' }}">
            <span class="{{ Request::is('angket-kepuasan*') ? 'nav-style' : 'hidden' }}" aria-hidden="true"></span>
            <a href="/angket-kepuasan" class="px-2 py-2 rounded-lg flex items-center text-white">
                <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-5" viewBox="0 0 20 20" fill="currentColor">
                    <path fill-rule="evenodd" d="M18 13V5a2 2 0 00-2-2H4a2 2 0 00-2 2v8a2 2 0 002 2h3l3 3 3-3h3a2 2 0 002-2zM5 7a1 1 0 011-1h8a1 1 0 110 2H6a1 1 0 01-1-1zm1 3a1 1 0 100 2h3a1 1 0 100-2H6z" clip-rule="evenodd" />
                  </svg>
                <span class="ml-4">Angket Kepuasan</span>
            </a>
        </li>
        <li
            class="relative px-4 text-base font-normal hover:bg-[#194990] py-1 {{ Request::is('jadwal-proposal') ? 'nav-active' : '' }}">
            <span class="{{ Request::is('jadwal-proposal') ? 'nav-style' : 'hidden' }}" aria-hidden="true"></span>
            <a href="/jadwal-proposal" class="px-2 py-2 rounded-lg flex items-center text-white">
                <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-5" viewBox="0 0 20 20" fill="currentColor">
                    <path fill-rule="evenodd" d="M10 18a8 8 0 100-16 8 8 0 000 16zm1-12a1 1 0 10-2 0v4a1 1 0 00.293.707l2.828 2.829a1 1 0 101.415-1.415L11 9.586V6z" clip-rule="evenodd" />
                  </svg>
                <span class="ml-4">Jadwal Proposal</span>
            </a>
        </li>
        <li
            class="relative px-4 text-base font-normal hover:bg-[#194990] py-1 {{ Request::is('upload-surat') ? 'nav-active' : '' }}">
            <span class="{{ Request::is('upload-surat') ? 'nav-style' : 'hidden' }}" aria-hidden="true"></span>
            <a href="/upload-surat" class="px-2 py-2 rounded-lg flex items-center text-white">
                <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-5" viewBox="0 0 20 20" fill="currentColor">
                    <path fill-rule="evenodd" d="M4 4a2 2 0 012-2h4.586A2 2 0 0112 2.586L15.414 6A2 2 0 0116 7.414V16a2 2 0 01-2 2H6a2 2 0 01-2-2V4z" clip-rule="evenodd" />
                  </svg>
                <span class="ml-4">Upload Surat</span>
            </a>
        </li>
        <li
            class="relative px-4 text-base font-normal hover:bg-[#194990] py-1 {{ Request::is('laporan') ? 'nav-active' : '' }}">
            <span class="{{ Request::is('laporan') ? 'nav-style' : 'hidden' }}" aria-hidden="true"></span>
            <a href="/laporan" class="px-2 py-2 rounded-lg flex items-center text-white">
                <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-5" viewBox="0 0 20 20" fill="currentColor">
                    <path d="M7 3a1 1 0 000 2h6a1 1 0 100-2H7zM4 7a1 1 0 011-1h10a1 1 0 110 2H5a1 1 0 01-1-1zM2 11a2 2 0 012-2h12a2 2 0 012 2v4a2 2 0 01-2 2H4a2 2 0 01-2-2v-4z" />
                  </svg>
                <span class="ml-4">Pelaporan dan database</span>
            </a>
        </li>
        @else
        
        {{-- <li x-data="{open : {{ Request::is('data-administrasi*') ? 'true' : 'false' }}}">
            <div @click="open = ! open"
                class="relative px-4 text-base font-normal py-1 hover:bg-[#194990] cursor-pointer {{ Request::is('data-administrasi*') ? 'nav-active' : '' }}"  >
                <span class="{{ Request::is('data-administrasi*') ? 'nav-style' : 'hidden' }}" aria-hidden="true"></span>
                <span class="{{ Request::is('data-administrasi*') ? 'nav-style' : 'hidden' }}" aria-hidden="true"></span>
                <div class="px-2 py-2 rounded-lg flex items-center text-white justify-between">
                    <div class="flex">
                        <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-5" viewBox="0 0 20 20" fill="currentColor">
                            <path d="M10.894 2.553a1 1 0 00-1.788 0l-7 14a1 1 0 001.169 1.409l5-1.429A1 1 0 009 15.571V11a1 1 0 112 0v4.571a1 1 0 00.725.962l5 1.428a1 1 0 001.17-1.408l-7-14z" />
                          </svg>
                        <h2  class="ml-4 flex justify-between select-none">Pengajuan Proposal</h2>
                    </div>

                    <span :class="{'block': ! open, 'hidden': open}">
                        <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-5" viewBox="0 0 20 20" fill="currentColor">
                            <path fill-rule="evenodd" d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z" clip-rule="evenodd" />
                          </svg>
                    </span>
                    <span :class="{'block':  open, 'hidden': ! open}">
                        <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-5" viewBox="0 0 20 20" fill="currentColor">
                            <path fill-rule="evenodd" d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z" clip-rule="evenodd" />
                          </svg>
                    </span>

                </div>
            </div>
            <div>
                <ul x-show="open" class="text-white text-xs bg-[#2872E2]">
                    <li><a class="hover:bg-gray-500 py-3 pl-6 block {{ Request::is('data-administrasi/pembudidaya-ikan') ? 'nav-active2' : '' }}"
                            href="/data-administrasi/pembudidaya-ikan/">Kelompok Masyarakat Pembudidaya
                            Ikan</a> </li>
                    <li><a class="hover:bg-gray-500 py-3 pl-6 block {{ Request::is('*hukum-adat') ? 'nav-active2' : '' }}" href="/data-administrasi/hukum-adat">Kelompok Masyarakat Hukum Adat </a> </li>
                    <li><a class="hover:bg-gray-500 py-3 pl-6 block {{ Request::is('*swadaya-masyarakat') ? 'nav-active2' : '' }}" href="/data-administrasi/swadaya-masyarakat">Lembaga Swadaya Masyarakat </a> </li>
                    <li><a class="hover:bg-gray-500 py-3 pl-6 block {{ Request::is('*lembaga-pendidikan') ? 'nav-active2' : '' }}" href="/data-administrasi/lembaga-pendidikan">Lembaga Pendidikan </a> </li>
                    <li><a class="hover:bg-gray-500 py-3 pl-6 block {{ Request::is('*lembaga-keagamaan') ? 'nav-active2' : '' }}" href="/data-administrasi/lembaga-keagamaan">Lembaga Keagamaan </a> </li>
                    <li><a class="hover:bg-gray-500 py-3 pl-6 block {{ Request::is('*lembaga-lain') ? 'nav-active2' : '' }}" href="/data-administrasi/lembaga-lain">Lembaga Lain </a> </li>
                </ul>
            </div>
        </li> --}}
        <li
            class="relative px-4 text-base font-normal hover:bg-[#194990] py-1 {{ Request::is('pengumuman*') ? 'nav-active' : '' }}">
            <span class="{{ Request::is('pengumuman*') ? 'nav-style' : 'hidden' }}" aria-hidden="true"></span>
            <a href="/pengumuman" class="px-2 py-2 rounded-lg flex items-center text-white">
                <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-5" viewBox="0 0 20 20" fill="currentColor">
                    <path fill-rule="evenodd" d="M18 3a1 1 0 00-1.447-.894L8.763 6H5a3 3 0 000 6h.28l1.771 5.316A1 1 0 008 18h1a1 1 0 001-1v-4.382l6.553 3.276A1 1 0 0018 15V3z" clip-rule="evenodd" />
                  </svg>
                <span class="ml-4">Pengumuman</span>
            </a>
        </li>
        <li
            class="relative px-4 text-base font-normal hover:bg-[#194990] py-1 {{ Request::is('pengajuan-proposal*') ? 'nav-active' : '' }}">
            <span class="{{ Request::is('pengajuan-proposal*') ? 'nav-style' : 'hidden' }}" aria-hidden="true"></span>
            <a href="/pengajuan-proposal/data-administrasi" class="px-2 py-2 rounded-lg flex items-center text-white">
                <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-5" viewBox="0 0 20 20" fill="currentColor">
                    <path d="M10.894 2.553a1 1 0 00-1.788 0l-7 14a1 1 0 001.169 1.409l5-1.429A1 1 0 009 15.571V11a1 1 0 112 0v4.571a1 1 0 00.725.962l5 1.428a1 1 0 001.17-1.408l-7-14z" />
                  </svg>
                <span class="ml-4">Pengajuan Proposal</span>
            </a>
        </li>

        <li
            class="relative px-4 text-base font-normal hover:bg-[#194990] py-1 {{ Request::is('pengambilan-benih') ? 'nav-active' : '' }}">
            <span class="{{ Request::is('pengambilan-benih') ? 'nav-style' : 'hidden' }}" aria-hidden="true"></span>
            <a href="/pengambilan-benih" class="px-2 py-2 rounded-lg flex items-center text-white">
                <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-5" viewBox="0 0 20 20" fill="currentColor">
                    <path d="M3 1a1 1 0 000 2h1.22l.305 1.222a.997.997 0 00.01.042l1.358 5.43-.893.892C3.74 11.846 4.632 14 6.414 14H15a1 1 0 000-2H6.414l1-1H14a1 1 0 00.894-.553l3-6A1 1 0 0017 3H6.28l-.31-1.243A1 1 0 005 1H3zM16 16.5a1.5 1.5 0 11-3 0 1.5 1.5 0 013 0zM6.5 18a1.5 1.5 0 100-3 1.5 1.5 0 000 3z" />
                  </svg>
                <span class="ml-4">Pengambilan Benih</span>
            </a>
        </li>
        @endif
        

        

        <li
            class="relative text-base font-normal hover:bg-[#194990] {{ Request::is('#') ? 'nav-active' : '' }}">
            <form action="/logout" method="post">
            @csrf
            <button type="submit" class="px-6 py-3 w-full flex items-center text-white hover:bg-[#194990]">
                <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-5" viewBox="0 0 20 20" fill="currentColor">
                    <path fill-rule="evenodd" d="M3 3a1 1 0 011 1v12a1 1 0 11-2 0V4a1 1 0 011-1zm7.707 3.293a1 1 0 010 1.414L9.414 9H17a1 1 0 110 2H9.414l1.293 1.293a1 1 0 01-1.414 1.414l-3-3a1 1 0 010-1.414l3-3a1 1 0 011.414 0z" clip-rule="evenodd" />
                  </svg>
                <span class="ml-4">Logout</span>
            </button>
        </form>
        </li>

    </ul>

</div>
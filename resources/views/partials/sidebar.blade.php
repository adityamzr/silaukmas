<div class="md:hidden">
    <div @click="sidemenu = false"
        class="fixed inset-0 z-30 backdrop-blur bg-black/50 opacity-0 pointer-events-none transition-opacity ease-linear duration-300"
        :class="{'opacity-100 pointer-events-auto': sidemenu, 'opacity-0 pointer-events-none': !sidemenu}"></div>
    <!-- Small Screen Menu -->
    <div class="fixed inset-y-0 left-0 flex flex-col z-40 max-w-xs w-full bg-primary transform ease-in-out duration-300 -translate-x-full"
        :class="{'translate-x-0': sidemenu, '-translate-x-full': !sidemenu}">
        <x-menu-sidebar-layout class="flex-1 h-0 overflow-y-auto" />
    </div>

</div>

<!-- Menu Above Medium Screen -->
<div class="bg-primary w-72 min-h-screen overflow-y-auto hidden md:block shadow relative z-30">
    <x-menu-sidebar-layout />
</div>
<!-- Menu Above Medium Screen -->

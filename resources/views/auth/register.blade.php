<x-guest-layout>
    <div class="h-screen lg:overflow-hidden justify-center flex flex-col">
        <div class="absolute top-4 right-4 ">
            <div class=" max-w-[60px] lg:max-w-[80px] w-full h-full">
                <img src="assets/banten_logo.webp" class="object-contain" alt="banten">
            </div>
        </div>
        <div class="flex">
            <div class="w-6/12 hidden lg:block">
                <div class="h-screen w-full">
                    <img class="object-cover h-full w-full" src="assets/bg_login.jpeg" alt="Background Login register">
                </div>
            </div>
            <div class="w-full lg:w-6/12 flex flex-col lg:justify-center items-center">
                <div class=" px-4 lg:px-24">
                    <div class="flex flex-col lg:flex-row items-center">
                        <div class=" max-w-[60px] lg:max-h-40  max-h-[60px] lg:max-w-[160px]">
                             <x-aplication-logo class="w-full h-full object-contain"/>
                        </div>
                        <div class="mx-0 lg:mx-5">
                            <h1 class="font-bold text-4xl text-center lg:text-left text-primary">Silaukmas</h1>
                            <p>Sistem Informasi Layanan Untuk Masyarakat</p>
                        </div>
                    </div>
                    <div class="mt-12">
                        <form action="{{ route('register') }}" method="post">
                            @csrf

                            {{-- Nama Lengkap --}}
                            <div>
                                <x-label for="namaLengkap" :value="__('Nama Lengkap')" />
                                <x-input class="w-full" type="text" name="namaLengkap" :value="old('namaLengkap')" required autofocus />
                            </div>
                            <div>
                                <x-label for="kelompokMasyarakat" :value="__('Kelompok Pemohon')" />
                                <x-select class="w-full" name="kelompokMasyarakat" :value="old('kelompokMasyarakat')" required>
                                    <option value="Kelompok Masyarakat Pembudidaya Ikan" {{ ('Kelompok Masyarakat Pembudidaya Ikan' == old('kelompokMasyarakat'))? 'selected' : '' }}>Kelompok Masyarakat Pembudidaya Ikan</option>
                                    <option value="Kelompok Masyarakat Hukum Adat" {{ ('Kelompok Masyarakat Hukum Adat' == old('kelompokMasyarakat'))? 'selected' : '' }}>Kelompok Masyarakat Hukum Adat</option>
                                    <option value="Lembaga Swadaya Masyarakat" {{ ('Lembaga Swadaya Masyarakat' == old('kelompokMasyarakat'))? 'selected' : '' }}>Lembaga Swadaya Masyarakat</option>
                                    <option value="Lembaga Pendidikan" {{ ('Lembaga Pendidikan' == old('kelompokMasyarakat'))? 'selected' : '' }}>Lembaga Pendidikan</option>
                                    <option value="Lembaga Keagamaan" {{ ('Lembaga Keagamaan' == old('kelompokMasyarakat'))? 'selected' : '' }}>Lembaga Keagamaan</option>
                                    <option value="Lembaga Lain" {{ ('Lembaga Lain' == old('kelompokMasyarakat'))? 'selected' : '' }}>Lembaga Lain</option>
                                </x-select>
                            </div>

                            {{-- Nomor Telepone --}}
                            <div>
                                <x-label for="noHp" :value="__('Nomor Telepon')" />
                                <x-input class="w-full" type="number" name="noHp" :value="old('noHp')" required />
                            </div>

                            {{-- Email --}}
                            <div>
                                <x-label for="email" :value="__('Email')" />
                                <x-input class="w-full" type="email" name="email" :value="old('email')" required />
                            </div>

                            {{-- password --}}
                            <div>
                                <x-label for="password" :value="__('Password')" />
                                <x-input class="w-full" type="password" name="password" required />
                            </div>

                            <x-button class="bg-primary w-full justify-center text-white mt-3">Daftar</x-button>
                        </form>

                        <p class="mt-4 block">Sudah punya akun?
                            <a href="/login" class="font-bold text-primary hover:text-blue-900 transition-all duration-200 transform"> Masuk Sekarang </a>
                        </p>
                    </div>
                </div>

            </div>
        </div>
    </div>

</x-guest-layout>

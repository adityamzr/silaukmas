<x-guest-layout>
    <div class="h-screen lg:overflow-hidden justify-center flex flex-col">
        <div class="absolute top-4 right-4 ">
            <div class=" max-w-[60px] lg:max-w-[80px] w-full h-full">
                <img src="assets/banten_logo.webp" class="object-contain" alt="banten">
            </div>
        </div>
        <div class="flex">
            <div class="w-6/12 hidden lg:block">
                <div class="h-screen w-full">
                    <img class="object-cover h-full w-full" src="assets/bg_login.jpeg" alt="Background Login register">
                </div>
            </div>
            <div class="w-full lg:w-6/12 flex flex-col lg:justify-center items-center">
               
                <div class=" px-4 lg:px-24">
                    <div class="flex flex-col lg:flex-row items-center">
                        <div class=" max-w-[60px] lg:max-h-40  max-h-[60px] lg:max-w-[160px]">
                            <x-aplication-logo class="w-full h-full object-contain"/>
                        </div>
                        <div class="mx-0 lg:mx-5">
                            <h1 class="font-bold text-4xl text-center lg:text-left text-primary">Silaukmas</h1>
                            <p>Sistem Informasi Layanan Untuk Masyarakat</p>
                        </div>
                    </div>
                    <div class="mt-12">
                        <form action="{{ route('login') }}" method="post">
                            @csrf
                            {{-- email --}}
                            <div>
                                <x-label for="email" :value="__('Email')" />
                                <x-input class="w-full" type="email" name="email" :value="old('email')" required autofocus />
                            </div>

                            {{-- password --}}
                            <div>
                                <x-label for="password" :value="__('Password')" />
                                <x-input class="w-full" type="password" name="password" required />
                            </div>
                            {{-- <a href="#" class="mt-3 block hover:underline">Lupa Password?</a> --}}

                            <div class="text-center my-4"> {!! NoCaptcha::display() !!} </div>

                           {{-- <a href="/dashboard"><x-button type="button" class="bg-primary w-full justify-center text-white">Masuk</x-button></a>  --}}
                           <x-button type="submit" class="bg-primary w-full justify-center text-white">Masuk</x-button>
                        </form>
                        <p class="mt-4 block">Belum punya akun?
                            <a href="/register" class="font-bold text-primary hover:text-blue-900  transition-all duration-200 transform"> Daftar Sekarang </a>
                        </p>
                    </div>
                </div>

            </div>
        </div>
    </div>

    
@if (Session::has('success'))
<x-slot name="alert">
    <div x-data="{open : true}" x-show="open" 
        x-transition:enter="transition ease-out duration-300"
        x-transition:enter-start="opacity-0 scale-90"
        x-transition:enter-end="opacity-100 scale-100"
        x-transition:leave="transition ease-in duration-300"
        x-transition:leave-start="opacity-100 scale-100"
        x-transition:leave-end="opacity-0 scale-90"
    class="flex p-4 mb-4 bg-green-100 rounded-lg shadow-lg" role="alert">
        <svg class="flex-shrink-0 w-5 h-5 text-green-700 dark:text-green-800" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M18 10a8 8 0 11-16 0 8 8 0 0116 0zm-7-4a1 1 0 11-2 0 1 1 0 012 0zM9 9a1 1 0 000 2v3a1 1 0 001 1h1a1 1 0 100-2v-3a1 1 0 00-1-1H9z" clip-rule="evenodd"></path></svg>
        <div class="ml-3 text-sm font-medium text-green-700 dark:text-green-800 pr-3">
        {{Session::get('success')}}
        </div>
        <button @click="open = ! open" x-init="setTimeout(() => open = false, 3000)" type="button" class="ml-auto -mx-1.5 -my-1.5 bg-green-100 text-green-500 rounded-lg focus:ring-2 focus:ring-green-400 p-1.5 hover:bg-green-200 inline-flex h-8 w-8" data-dismiss-target="#alert-2" aria-label="Close">
          <span class="sr-only">Close</span>
          <svg class="w-5 h-5" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z" clip-rule="evenodd"></path></svg>
        </button>
      </div>
      
</x-slot>
@elseif (Session::has('LoginError'))
<x-slot name="alert">
    <div x-data="{open : true}" x-show="open" 
        x-transition:enter="transition ease-out duration-300"
        x-transition:enter-start="opacity-0 scale-90"
        x-transition:enter-end="opacity-100 scale-100"
        x-transition:leave="transition ease-in duration-300"
        x-transition:leave-start="opacity-100 scale-100"
        x-transition:leave-end="opacity-0 scale-90"
    class="flex p-4 mb-4 bg-red-100 rounded-lg shadow-lg" role="alert">
        <svg class="flex-shrink-0 w-5 h-5 text-red-700 dark:text-red-800" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M18 10a8 8 0 11-16 0 8 8 0 0116 0zm-7-4a1 1 0 11-2 0 1 1 0 012 0zM9 9a1 1 0 000 2v3a1 1 0 001 1h1a1 1 0 100-2v-3a1 1 0 00-1-1H9z" clip-rule="evenodd"></path></svg>
        <div class="ml-3 text-sm font-medium text-red-700 dark:text-red-800 pr-3">
        {{ Session::get('LoginError') }}
        </div>
        <button @click="open = ! open" x-init="setTimeout(() => open = false, 3000)" type="button" class="ml-auto -mx-1.5 -my-1.5 bg-red-100 text-red-500 rounded-lg focus:ring-2 focus:ring-red-400 p-1.5 hover:bg-red-200 inline-flex h-8 w-8" data-dismiss-target="#alert-2" aria-label="Close">
          <span class="sr-only">Close</span>
          <svg class="w-5 h-5" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z" clip-rule="evenodd"></path></svg>
        </button>
      </div>
</x-slot>

@endif
@error('g-recaptcha-response')
<x-slot name="alert">
    <div x-data="{open : true}" x-show="open" 
        x-transition:enter="transition ease-out duration-300"
        x-transition:enter-start="opacity-0 scale-90"
        x-transition:enter-end="opacity-100 scale-100"
        x-transition:leave="transition ease-in duration-300"
        x-transition:leave-start="opacity-100 scale-100"
        x-transition:leave-end="opacity-0 scale-90"
    class="flex p-4 mb-4 bg-red-100 rounded-lg shadow-lg" role="alert">
        <svg class="flex-shrink-0 w-5 h-5 text-red-700 dark:text-red-800" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M18 10a8 8 0 11-16 0 8 8 0 0116 0zm-7-4a1 1 0 11-2 0 1 1 0 012 0zM9 9a1 1 0 000 2v3a1 1 0 001 1h1a1 1 0 100-2v-3a1 1 0 00-1-1H9z" clip-rule="evenodd"></path></svg>
        <div class="ml-3 text-sm font-medium text-red-700 dark:text-red-800 pr-3">
        {{$message}}
        </div>
        <button @click="open = ! open" x-init="setTimeout(() => open = false, 3000)" type="button" class="ml-auto -mx-1.5 -my-1.5 bg-red-100 text-red-500 rounded-lg focus:ring-2 focus:ring-red-400 p-1.5 hover:bg-red-200 inline-flex h-8 w-8 dark:bg-red-20" data-dismiss-target="#alert-2" aria-label="Close">
          <span class="sr-only">Close</span>
          <svg class="w-5 h-5" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z" clip-rule="evenodd"></path></svg>
        </button>
      </div>
</x-slot>
@enderror
    

</x-guest-layout>

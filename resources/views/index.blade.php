<x-guest-layout>
    <x-navbar-layout />


    <section class="flex items-center w-full min-h-screen justify-center lg:justify-start bg-ornament bg-no-repeat bg-right-bottom relative ">
        <div
        class="absolute top-3 left-6 h-44 lg:w-72 w-44 lg:h-72 bg-primary/30 rounded-full mix-blend-multiply filter blur-2xl opacity-70 animate-blob">
    </div>
        <div class="container mx-auto">
            <div class="flex flex-col lg:flex-row items-center justify-center pb-0 lg:pb-20">
                <div class="w-full lg:w-6/12" data-aos="fade-up" data-aos-delay="100">
                    <h2 class="font-bold text-xl lg:text-3xl text-center lg:text-left mb-4 lg:mb-8">Sistem Informasi Layanan Akuaqultur Untuk Masyarakat </h2>
                    <h2 class="font-bold text-3xl lg:text-5xl text-center lg:text-left">Silauk<span class="text-primary">mas</span></h2>
                    <div class="mt-6 text-sm lg:text-base flex flex-col space-y-3 text-accent">
                        <h3 class="inline-flex text-gray-900 ">
                            <span class="mr-2">
                                <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-5 fill-primary" viewBox="0 0 20 20"
                                   >
                                    <path fill-rule="evenodd"
                                        d="M5.05 4.05a7 7 0 119.9 9.9L10 18.9l-4.95-4.95a7 7 0 010-9.9zM10 11a2 2 0 100-4 2 2 0 000 4z"
                                        clip-rule="evenodd" />
                                </svg>
                            </span>
                            Jln Raya Labuan, Ds Cipeucang Kec. Cipeucang Kab. Pandeglang 42217

                        </h3>
                        <h3 class="inline-flex text-gray-900 ">
                            <span class="mr-2">
                                <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-5 fill-primary" viewBox="0 0 20 20"
                                   >
                                    <path d="M2.003 5.884L10 9.882l7.997-3.998A2 2 0 0016 4H4a2 2 0 00-1.997 1.884z" />
                                    <path d="M18 8.118l-8 4-8-4V14a2 2 0 002 2h12a2 2 0 002-2V8.118z" />
                                </svg>
                            </span>
                            ppbaplbanten@gmail.com
                        </h3>
                        {{-- <h3 class="inline-flex text-gray-900 ">
                            <span class="mr-2">
                                <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-5 fill-primary" viewBox="0 0 20 20"
                                   >
                                    <path
                                        d="M2 3a1 1 0 011-1h2.153a1 1 0 01.986.836l.74 4.435a1 1 0 01-.54 1.06l-1.548.773a11.037 11.037 0 006.105 6.105l.774-1.548a1 1 0 011.059-.54l4.435.74a1 1 0 01.836.986V17a1 1 0 01-1 1h-2C7.82 18 2 12.18 2 5V3z" />
                                </svg>
                            </span>
                            08211956XXX
                        </h3> --}}
                        <h3 class="inline-flex text-gray-900 ">
                            <span class="mr-2">
                                <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-5 fill-primary" viewBox="0 0 24 24" ><path d="M20.947 8.305a6.53 6.53 0 0 0-.419-2.216 4.61 4.61 0 0 0-2.633-2.633 6.606 6.606 0 0 0-2.186-.42c-.962-.043-1.267-.055-3.709-.055s-2.755 0-3.71.055a6.606 6.606 0 0 0-2.185.42 4.607 4.607 0 0 0-2.633 2.633 6.554 6.554 0 0 0-.419 2.185c-.043.963-.056 1.268-.056 3.71s0 2.754.056 3.71c.015.748.156 1.486.419 2.187a4.61 4.61 0 0 0 2.634 2.632 6.584 6.584 0 0 0 2.185.45c.963.043 1.268.056 3.71.056s2.755 0 3.71-.056a6.59 6.59 0 0 0 2.186-.419 4.615 4.615 0 0 0 2.633-2.633c.263-.7.404-1.438.419-2.187.043-.962.056-1.267.056-3.71-.002-2.442-.002-2.752-.058-3.709zm-8.953 8.297c-2.554 0-4.623-2.069-4.623-4.623s2.069-4.623 4.623-4.623a4.623 4.623 0 0 1 0 9.246zm4.807-8.339a1.077 1.077 0 0 1-1.078-1.078 1.077 1.077 0 1 1 2.155 0c0 .596-.482 1.078-1.077 1.078z"></path><circle cx="11.994" cy="11.979" r="3.003"></circle></svg>
                            </span>
                            @ppbaplprovinsibanten
                        </h3>
                        <h3 class="inline-flex text-gray-900 ">
                            <span class="mr-2">
                                <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-5 fill-primary" viewBox="0 0 24 24"><path d="M20 3H4a1 1 0 0 0-1 1v16a1 1 0 0 0 1 1h8.615v-6.96h-2.338v-2.725h2.338v-2c0-2.325 1.42-3.592 3.5-3.592.699-.002 1.399.034 2.095.107v2.42h-1.435c-1.128 0-1.348.538-1.348 1.325v1.735h2.697l-.35 2.725h-2.348V21H20a1 1 0 0 0 1-1V4a1 1 0 0 0-1-1z"></path></svg>
                            </span>
                            Ppbapl banten
                        </h3>
                        <h3 class="inline-flex text-gray-900 ">
                            <span class="mr-2">
                                <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-5 fill-primary" viewBox="0 0 24 24"><path d="M21.593 7.203a2.506 2.506 0 0 0-1.762-1.766C18.265 5.007 12 5 12 5s-6.264-.007-7.831.404a2.56 2.56 0 0 0-1.766 1.778c-.413 1.566-.417 4.814-.417 4.814s-.004 3.264.406 4.814c.23.857.905 1.534 1.763 1.765 1.582.43 7.83.437 7.83.437s6.265.007 7.831-.403a2.515 2.515 0 0 0 1.767-1.763c.414-1.565.417-4.812.417-4.812s.02-3.265-.407-4.831zM9.996 15.005l.005-6 5.207 3.005-5.212 2.995z"></path></svg>
                            </span>
                            PPBAPL BANTEN
                        </h3>
                        <h3 class="inline-flex text-gray-900 ">
                            <span class="mr-2">
                                <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-5 fill-primary" viewBox="0 0 24 24"><path d="M19.633 7.997c.013.175.013.349.013.523 0 5.325-4.053 11.461-11.46 11.461-2.282 0-4.402-.661-6.186-1.809.324.037.636.05.973.05a8.07 8.07 0 0 0 5.001-1.721 4.036 4.036 0 0 1-3.767-2.793c.249.037.499.062.761.062.361 0 .724-.05 1.061-.137a4.027 4.027 0 0 1-3.23-3.953v-.05c.537.299 1.16.486 1.82.511a4.022 4.022 0 0 1-1.796-3.354c0-.748.199-1.434.548-2.032a11.457 11.457 0 0 0 8.306 4.215c-.062-.3-.1-.611-.1-.923a4.026 4.026 0 0 1 4.028-4.028c1.16 0 2.207.486 2.943 1.272a7.957 7.957 0 0 0 2.556-.973 4.02 4.02 0 0 1-1.771 2.22 8.073 8.073 0 0 0 2.319-.624 8.645 8.645 0 0 1-2.019 2.083z"></path></svg>
                            </span>
                            @ppbapl_banten
                        </h3>
                        <h3 class="inline-flex text-gray-900 ">
                            <span class="mr-2">
                                <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-5 fill-primary" viewBox="0 0 24 24"><path d="M19.59 6.69a4.83 4.83 0 0 1-3.77-4.25V2h-3.45v13.67a2.89 2.89 0 0 1-5.2 1.74 2.89 2.89 0 0 1 2.31-4.64 2.93 2.93 0 0 1 .88.13V9.4a6.84 6.84 0 0 0-1-.05A6.33 6.33 0 0 0 5 20.1a6.34 6.34 0 0 0 10.86-4.43v-7a8.16 8.16 0 0 0 4.77 1.52v-3.4a4.85 4.85 0 0 1-1-.1z"></path></svg>
                            </span>
                            @ppbaplbanten
                        </h3>
                    </div>
                </div>
                <div class="w-full lg:w-6/12 mb-8 lg:mb-0 ">
                   
                    <div class="max-w-xl h-full w-full mx-auto relative" data-aos="fade-up" data-aos-delay="200">
                        <div
                        class="absolute -top-4 right-0 h-44 lg:w-72 w-44 lg:h-72 bg-primary/30 rounded-full mix-blend-multiply filter blur-2xl opacity-70 animate-blob">
                    </div>
                        <div class="bg-[#89ED12] rounded-lg drop-shadow-3xl p-4 lg:p-8 mt-8 lg:mt-14  text-sm lg:text-base">
                            <h3 class=" text-base lg:text-2xl text-white font-semibold inline-flex items-center mb-3">
                                <span class="bg-blue-100 rounded-full h-10 w-10 flex items-center justify-center mr-3 ">
                                    <svg xmlns="http://www.w3.org/2000/svg" class=" max-h-6 max-w-6 w-full h-full stroke-blue-500" fill="none" viewBox="0 0 24 24"
                                         stroke-width="2">
                                        <path stroke-linecap="round" stroke-linejoin="round"
                                            d="M11 5.882V19.24a1.76 1.76 0 01-3.417.592l-2.147-6.15M18 13a3 3 0 100-6M5.436 13.683A4.001 4.001 0 017 6h1.832c4.1 0 7.625-1.234 9.168-3v14c-1.543-1.766-5.067-3-9.168-3H7a3.988 3.988 0 01-1.564-.317z" />
                                    </svg></span>
                                Informasi jadwal pengajuan proposal </h3>
                            <div class="text-gray-600 space-y-2">
                                {{-- timeline --}}
                                @if ($jadwal->status == 1)
                                <ol class="relative border-l border-blue-200 ml-5 mt-3">
                                    <li class="mb-10 ml-4">
                                        <div
                                            class="absolute w-3 h-3 bg-blue-500 rounded-full mt-1.5 -left-1.5 border border-white ">
                                        </div>
                                        <h3 class="text-base font-semibold text-white ">Pengajuan Proposal</h3>
                                        <p class="mb-4 text-md font-normal text-white ">Pengajuan proposal akan dibuka dari tanggal <span class="font-semibold">{{ $pengajuan['awalProposal'] }}</span>  sampai dengan <span class="font-semibold"> {{ $pengajuan['akhirProposal'] }}</span></p>
                                       
                                    </li>
                                    <li class="mb-10 ml-4">
                                        <div
                                            class="absolute w-3 h-3 bg-blue-500 rounded-full mt-1.5 -left-1.5 border border-white ">
                                        </div>
                                        <h3 class="text-base font-semibold text-white ">Verifikasi Proposal</h3>
                                        <p class="mb-4 text-md font-normal text-white ">Verifikasi proposal akan dilakukan dari tanggal <span class="font-semibold">{{ $pengajuan['awalVerifikasi'] }}</span>  sampai dengan <span class="font-semibold"> {{ $pengajuan['akhirVerifikasi'] }}</span></p>
                                    </li>
                                    <li class="ml-4">
                                        <div
                                            class="absolute w-3 h-3 bg-blue-500 rounded-full mt-1.5 -left-1.5 border border-white ">
                                        </div>
                                        <h3 class="text-base font-semibold text-white ">Pengambilan Benih</h3>
                                        <p class="mb-4 text-md font-normal text-white ">Pengambilan Benih akan dimulai dari tanggal <span class="font-semibold">{{ $pengajuan['awalPengambilan'] }}</span>  sampai dengan <span class="font-semibold"> {{ $pengajuan['akhirPengambilan'] }}</span></p>
                                    </li>
                                </ol>

                                @else

                                <p>Pengajuan Proposal Sedang Ditutup.</p>
                                    
                                @endif
                        
                              
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </section>
    

    <section id="about">
        <div class="container mx-auto ">
            <h2 class="title" data-aos="fade-right">About</h2>
            <p class="text-base lg:text-xl text-accent"  data-aos="fade-up" data-aos-delay="100">Aplikasi pelayanan bantuan benih ikan Dinas Kelautan dan Perikanan (DKP) Provinsi Banten kepada masyarakat Banten, yang yang dikelola dan disalurkan oleh UPTD Produksi Perikanan Budidaya Air Payau dan Laut (PPBAPL) sebagai UPTD yang membawahi sektor perikanan budidaya, yang diharapkan dapat mendukung peningkatan produksi perikanan budidaya dan kemandirian pangan daerah dari sektor perikanan budidaya.</p>
        </div>
    </section>

    <section class="relative" >
        <div class="container mx-auto">
            <div class="text-center ">
                <div class="bg-ornament bg-no-repeat py-7 bg-right w-fit px-12 mx-auto">
                    <h2 class="title" data-aos="fade-in" data-aos-delay="200">Keunggulan</h2>
                </div>
            </div>
            <div class="grid grid-cols-1 lg:grid-cols-3 gap-4 pt-10 ">
                {{-- item card --}}
                <div
                    data-aos="fade-up"
                  
                    class="max-w-[380px] h-[228px] w-full rounded-lg p-5 mx-auto flex flex-col justify-between items-center drop-shadow-3xl bg-white">
                    <div class="bg-yellow-50 rounded-full flex justify-center items-center">
                        <svg xmlns="http://www.w3.org/2000/svg" class="h-20 w-20 fill-yellow-500 p-4" viewBox="0 0 20 20" >
                            <path d="M2 5a2 2 0 012-2h7a2 2 0 012 2v4a2 2 0 01-2 2H9l-3 3v-3H4a2 2 0 01-2-2V5z" />
                            <path d="M15 7v2a4 4 0 01-4 4H9.828l-1.766 1.767c.28.149.599.233.938.233h2l3 3v-3h2a2 2 0 002-2V9a2 2 0 00-2-2h-1z" />
                          </svg>
                    </div>
                    <p class="text-center text-accent">Mendekatkan pelayanan UPTD PPBAPL kepada masyarakat yang berminat dengan perikanan budidaya</p>
                </div>
                {{-- item card --}}
                <div
                data-aos="fade-up"
                data-aos-delay="100"
                    class="max-w-[380px] h-[228px] w-full rounded-lg p-5 mx-auto flex flex-col justify-between items-center drop-shadow-3xl bg-white">
                    <div class="bg-blue-50 rounded-full flex justify-center items-center">
                        <svg xmlns="http://www.w3.org/2000/svg" class="h-20 w-20 fill-primary p-4" viewBox="0 0 20 20" >
                            <path d="M13 6a3 3 0 11-6 0 3 3 0 016 0zM18 8a2 2 0 11-4 0 2 2 0 014 0zM14 15a4 4 0 00-8 0v3h8v-3zM6 8a2 2 0 11-4 0 2 2 0 014 0zM16 18v-3a5.972 5.972 0 00-.75-2.906A3.005 3.005 0 0119 15v3h-3zM4.75 12.094A5.973 5.973 0 004 15v3H1v-3a3 3 0 013.75-2.906z" />
                          </svg>
                        </div>
                        <p class="text-center text-accent">Memudahkan akses kepada masyarakat dalam mengajukan usulan bantuan benih ikan</p>
                    </div>
                    {{-- item card --}}
                    <div
                    data-aos="fade-up"
                    data-aos-delay="200"
                    class="max-w-[380px] h-[228px] w-full rounded-lg p-5 mx-auto flex flex-col justify-between items-center drop-shadow-3xl bg-white">
                    <div class="bg-emerald-50 rounded-full flex justify-center items-center">
                        <svg xmlns="http://www.w3.org/2000/svg" class="h-20 w-20 fill-emerald-500 p-4" viewBox="0 0 20 20" >
                            <path fill-rule="evenodd" d="M5.05 4.05a7 7 0 119.9 9.9L10 18.9l-4.95-4.95a7 7 0 010-9.9zM10 11a2 2 0 100-4 2 2 0 000 4z" clip-rule="evenodd" />
                          </svg>
                    </div>
                    <p class="text-center text-accent">Memudahkan masyarakat mengetahui proses perjalanan usulan bantuan benih ikan</p>
                </div>
            </div>
        </div>
    </section>

    <section class="px-0" id="tahapan">
        <div class="bg-primary py-12">
            <h2 class="title text-white text-center py-6">Alur</h2>
                <div class="bg-white max-w-[600px] h-full w-full object-contain drop-shadow-lg rounded-full mx-auto flex justify-center items-center overflow-hidden">
                       <img class="w-full h-full object-contain p-8" src="/assets/images/alur.jpg" alt="alur">
                </div>
            <div class="container mx-auto px-6 mt-14">
                <ol class="relative border-l border-gray-200" data-aos="fade-up">

                    <li class="mb-10 ml-10">
                        <span
                            class="flex absolute -left-3 justify-center items-center w-6 h-6 bg-blue-200 rounded-full ring-8 ring-white ">
                            <span class="text-sm font-semibold text-blue-600">1</span>
                        </span>
                        <p class="mb-4  font-normal text-gray-200 ">Calon Penerima Bantuan dapat menyampaikan usulan bantuan benih langsung kepada Dinas Kelautan dan Perikanan dan melampirkan surat rekomendasi dari Dinas Kab/Kota, namun dikecualikan bagi lembaga lain dapat langsung mengusulkan ke Dinas Kelautan dan Perikanan dan ditembuskan ke dinas kab/kota; </p>
                    </li>

                    <li class="mb-10 ml-10">
                        <span
                            class="flex absolute -left-3 justify-center items-center w-6 h-6 bg-blue-200 rounded-full ring-8 ring-white ">
                            <span class="text-sm font-semibold text-blue-600">2</span>
                        </span>
                        <p class="mb-4  font-normal text-gray-200 ">Dinas Kab/Kota dapat mengusulkan Kelompok Masyarakat Calon Penerima Bantuan kepada Dinas Kelautan dan Perikanan Provinsi Banten;</p>
                    </li>
         
                    <li class="mb-10 ml-10">
                        <span
                            class="flex absolute -left-3 justify-center items-center w-6 h-6 bg-blue-200 rounded-full ring-8 ring-white ">
                            <span class="text-sm font-semibold text-blue-600">3</span>
                        </span>
                        <p class="mb-4  font-normal text-gray-200 ">Kepala Dinas Kelautan dan Perikanan mendisposisikan usulan bantuan benih kepada UPTD Produksi Perikanan Budidaya Air Payau dan Laut ( PPBAPL);</p>
                    </li>
         
                    <li class="mb-10 ml-10">
                        <span
                            class="flex absolute -left-3 justify-center items-center w-6 h-6 bg-blue-200 rounded-full ring-8 ring-white ">
                            <span class="text-sm font-semibold text-blue-600">4</span>
                        </span>
                        <p class="mb-4  font-normal text-gray-200 ">UPTD PPBAPL melakukan verifikasi terhadap calon penerima bantuan benih;</p>
                    </li>
         
                    <li class="mb-10 ml-10">
                        <span
                            class="flex absolute -left-3 justify-center items-center w-6 h-6 bg-blue-200 rounded-full ring-8 ring-white ">
                            <span class="text-sm font-semibold text-blue-600">5</span>
                        </span>
                        <p class="mb-4  font-normal text-gray-200 ">Kepala UPTD PPBAPL membuat surat penetapan penerima bantuan benih;</p>
                    </li>

                    <li class="mb-10 ml-10">
                        <span
                            class="flex absolute -left-3 justify-center items-center w-6 h-6 bg-blue-200 rounded-full ring-8 ring-white ">
                            <span class="text-sm font-semibold text-blue-600">6</span>
                        </span>
                        <p class="mb-4 font-normal text-gray-200 ">Pelaksanaan penyaluran bantuan dilakukan setelah stok benih tersedia, atau proses produksi benih telah selesai dan siap didistribusikan;</p>
                    </li>

                    <li class="mb-10 ml-10">
                        <span
                            class="flex absolute -left-3 justify-center items-center w-6 h-6 bg-blue-200 rounded-full ring-8 ring-white ">
                            <span class="text-sm font-semibold text-blue-600">7</span>
                        </span>
                        <p class="mb-4 font-normal text-gray-200 ">Kepala UPTD PPBAPL melaporkan kegiatan distribusi bantuan benih kepada Kepala Dinas Kelautan dan Perikanan yang ditembuskan kepada Dinas Kab/Kota.</p>
                    </li>
         
                </ol>
            </div>
        </div>
    </section>

    <section id="faq">
        <div class="container mx-auto" data-aos="fade-up">
            <h2 class="title text-center mb-12">FAQ</h2>
            <div x-data="{selected:0}">
                {{-- item --}}
                <h2 @click="selected !== 0 ? selected = 0 : selected = null">
                    <div
                        class="cursor-pointer select-none flex justify-between items-center p-5 w-full font-medium text-left rounded-t-xl border border-b-0 border-gray-200 focus:ring-4 focus:ring-gray-200 hover:text-gray-900 hover:bg-gray-300 bg-gray-100 transition-all "
                        :class="{'bg-gray-300 ': selected == 0, 'bg-gray-100': selected !== 0}"
                        >
                        <span>Apa saja persyaratan administrasi yang dibutuhkan untuk mengusulkan bantuan benih ikan?</span>

                        <svg  xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" viewBox="0 0 20 20" fill="currentColor">
                            <path :class="{'block': selected == 0, 'hidden': selected !== 0}" fill-rule="evenodd" d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z" clip-rule="evenodd" />
                            <path :class="{'block': selected !== 0, 'hidden': selected == 0}" fill-rule="evenodd" d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z" clip-rule="evenodd" />
                          </svg>
                         
                    </div>
                </h2>
                <div x-show="selected == 0"  class="">
                    <div class="p-5 border border-b-0 border-gray-200 ">
                        <ul class="mb-2 text-gray-500 dark:text-gray-700 space-y-2 text-sm">
                            <li>a. Tergabung dalam kelompok pembudidaya, atau Koperasi dan bergerak di bidang usaha perikanan skala mikro atau kecil;</li>
                            <li>b. telah terdaftar di desa/kelurahan setempat/Dinas terkait;</li>
                            <li>c. jumlah anggota paling sedikit 10 (sepuluh) orang;</li>
                            <li>d. tidak menerima bantuan sejenis dari pihak lain dibuktikan dengan surat pernyataan yang ditandatangani oleh calon penerima dan diketahui oleh penyuluh/kepala desa/dinas (lampiran formulir 3);</li>
                            <li>e. hasil produksi yang dihasilkan oleh penerima bantuan tidak untuk tujuan ekspor;</li>
                            <li>f.  penerima bantuan bukan untuk perseorangan ataupun pribadi;</li>
                            <li>g. ketua/penanggungjawab penerima bantuan menandatangani surat pernyataan bermaterai, yang memuat kesediaan menerima, mengelola, memanfaatkan bantuan benih dan melaporkannya dan;</li>
                            <li>h. ketua/penanggungjawab penerima bantuan menandatangani Berita Acara Serah Terima (BAST) barang dari pimpinan pengelola bantuan atau yang dikuasakan;</li>
                            <li>i.  Mengunggah proposal bantuan benih melalui aplikasi Silauk Mas.</li>
                        </ul>
                    </div>
                </div>

                {{-- item --}}
                <h2 @click="selected !== 1 ? selected = 1 : selected = null">
                    <div
                        class="cursor-pointer select-none flex justify-between items-center p-5 w-full font-medium text-left border border-b-0 border-gray-200 focus:ring-4 focus:ring-gray-200 hover:text-gray-900 hover:bg-gray-300 bg-gray-100 transition-all "
                        :class="{'bg-gray-300 ': selected == 1, 'bg-gray-100': selected !== 1}"
                        >
                        <span>Apa saja persyaratan lokasi yang harus dimiliki oleh pengusul bantuan benih?</span>

                        <svg  xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" viewBox="0 0 20 20" fill="currentColor">
                            <path :class="{'block': selected == 1, 'hidden': selected !== 1}" fill-rule="evenodd" d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z" clip-rule="evenodd" />
                            <path :class="{'block': selected !== 1, 'hidden': selected == 1}" fill-rule="evenodd" d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z" clip-rule="evenodd" />
                          </svg>
                         
                    </div>
                </h2>
                <div x-show="selected == 1"  class="">
                    <div class="p-5 border border-b-0 border-gray-200 ">
                        <ul class="mb-2 text-gray-500 dark:text-gray-700 space-y-2 text-sm">
                            <li>a. lahan usaha perikanan budidaya yang telah dipersiapkan dan peruntukannya untuk kegiatan perikanan budidaya; </li>
                            <li>b. kepenguasaan lahan jelas dan bebas konflik, lahan milik sendiri atau sewa minimal 1 (satu) tahun yang dibuktikan dengan dokumen pendukung dan ;</li>
                            <li>c. mempunyai aksesibilitas dan mudah dijangkau yang dibuktikan dengan titik koordinat lokasi budidaya.</li>
                        </ul>
                    </div>
                </div>

                {{-- item --}}
                <h2 @click="selected !== 2 ? selected = 2 : selected = null">
                    <div
                        class="cursor-pointer select-none flex justify-between items-center p-5 w-full font-medium text-left border  border-gray-200 focus:ring-4 focus:ring-gray-200 hover:text-gray-900 hover:bg-gray-300 bg-gray-100 transition-all  "
                        :class="{'bg-gray-300 ': selected == 2, 'bg-gray-100': selected !== 2}"
                        >
                        <span>Siapa saja yang bisa mengusulkan bantuan benih ikan?</span>

                        <svg  xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" viewBox="0 0 20 20" fill="currentColor">
                            <path :class="{'block': selected == 2, 'hidden': selected !== 2}" fill-rule="evenodd" d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z" clip-rule="evenodd" />
                            <path :class="{'block': selected !== 2, 'hidden': selected == 2}" fill-rule="evenodd" d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z" clip-rule="evenodd" />
                          </svg>
                         
                    </div>
                </h2>
                <div x-show="selected == 2"  class="">
                    <div class="p-5 border border-t-0 border-gray-200 rounded-b-xl">
                        <ul class="mb-2 text-gray-500 dark:text-gray-700 space-y-2 text-sm">
                            <li>a.	Kelompok pembudidaya ikan (Pokdakan);</li>
                            <li>b.	kelompok masyarakat hukum adat;</li>
                            <li>c.	lembaga swadaya masyarakat;</li>
                            <li>d.	lembaga pendidikan;</li>
                            <li>e.	lembaga keagamaan.</li>
                        </ul>
                    </div>
                </div>

                {{-- item --}}
                <h2 @click="selected !== 3 ? selected = 3 : selected = null">
                    <div
                        class="cursor-pointer select-none flex justify-between items-center p-5 w-full font-medium text-left border  border-gray-200 focus:ring-4 focus:ring-gray-200 hover:text-gray-900 hover:bg-gray-300 bg-gray-100 transition-all  "
                        :class="{'bg-gray-300 ': selected == 3, 'bg-gray-100': selected !== 3}"
                        >
                        <span>Apa saja jenis ikan yang diusulkan oleh masyarakat?</span>

                        <svg  xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" viewBox="0 0 20 20" fill="currentColor">
                            <path :class="{'block': selected == 3, 'hidden': selected !== 3}" fill-rule="evenodd" d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z" clip-rule="evenodd" />
                            <path :class="{'block': selected !== 3, 'hidden': selected == 3}" fill-rule="evenodd" d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z" clip-rule="evenodd" />
                          </svg>
                         
                    </div>
                </h2>
                <div x-show="selected == 3"  class="">
                    <div class="p-5 border border-t-0 border-gray-200 rounded-b-xl">
                        <ul class="mb-2 text-gray-500 dark:text-gray-700 space-y-2 text-sm">
                            <li>a.	Ikan Nila</li>
                            <li>b.	Ikan Mas</li>
                            <li>c.	Ikan Lele</li>
                            <li>d.	Ikan Patin</li>
                            <li>e.	Ikan Gurame</li>
                            <li>f.	Ikan Bandeng</li>
                        </ul>
                    </div>
                </div>

                {{-- item --}}
                <h2 @click="selected !== 4 ? selected = 4 : selected = null">
                    <div
                        class="cursor-pointer select-none flex justify-between items-center p-5 w-full font-medium text-left border  border-gray-200 focus:ring-4 focus:ring-gray-200 hover:text-gray-900 hover:bg-gray-300 bg-gray-100 transition-all  "
                        :class="{'bg-gray-300 ': selected == 4, 'bg-gray-100': selected !== 4}"
                        >
                        <span>Berapakah jumlah dan ukuran ikan yang dapat diusulkan sebagai bantuan benih ikan ?</span>

                        <svg  xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" viewBox="0 0 20 20" fill="currentColor">
                            <path :class="{'block': selected == 4, 'hidden': selected !== 4}" fill-rule="evenodd" d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z" clip-rule="evenodd" />
                            <path :class="{'block': selected !== 4, 'hidden': selected == 4}" fill-rule="evenodd" d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z" clip-rule="evenodd" />
                          </svg>
                         
                    </div>
                </h2>
                <div x-show="selected == 4"  class="">
                    <div class="p-5 border border-t-0 border-gray-200 rounded-b-xl">
                        <ul class="mb-2 text-gray-500 dark:text-gray-700 space-y-2 text-sm">
                            <li>Untuk jumlah ikan yang diberikan tergantung hasil verifikasi lapangan yang dilakukan tim bantuan beinh ikan UPTD PPBAPL salah satunya dengan mempertimbangkan luas yang dimiliki oleh pengusul bantuan benih ikan sedangkan untuk ukuran ikan yang disalurkan berukuran kurang lebih 5 cm namun tergantung dengan ketersediaan benih ikan di UPTD PPBAPL.</li>
                        </ul>
                    </div>
                </div>

       
            </div>
        </div>
    </section>


    <footer class="bg-primary py-6 mt-12">
        <h3 class="text-center text-white text-sm lg:text-base">Copyright © 2022. All rights reserved.</h3>
    </footer>
</x-guest-layout>

<x-app-layout :title="__('Dashboard')">
    {{-- Title --}}
   <div>
        <h2 class="text-3xl lg:text-4xl mb-3">Papan Informasi</h2>
        <p class="text-accent">Periode Tanggal <strong> {{  $pengajuan['awalProposal'] }}</strong> Sampai dengan Tanggal <strong>{{  $pengajuan['akhirProposal'] }}</strong></p>
       
    </div>
    <div class="mt-14">
        <h2 class="text-2xl lg:text-3xl mb-3 font-medium">Statistik Proposal</h2>
        <div class=" grid grid-cols-1 lg:grid-cols-3 gap-8">
    
            {{-- card --}}
            <div class="bg-white rounded-2xl drop-shadow-3xl p-6 w-full h-[220px] flex">
                <h2 class="mr-4 bg-blue-100 rounded-xl h-10 w-10 inline-flex items-center justify-center">
                    <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-5 fill-blue-600" viewBox="0 0 20 20" >
                        <path fill-rule="evenodd" d="M10 9a3 3 0 100-6 3 3 0 000 6zm-7 9a7 7 0 1114 0H3z" clip-rule="evenodd" />
                      </svg>
                        
                </h2>
                <div class="flex flex-col justify-between">
                    <h2 class="font-medium text-xl">Total Pemohon Proposal</h2>
                    <h2 class="font-semibold text-6xl">{{ $pemohons->count() }}</h2>
                </div>
            </div>
    
            {{-- card --}}
            <div class="bg-white rounded-2xl drop-shadow-3xl p-6 w-full h-[220px] flex ml-0 mt-6 lg:mt-0">
                <h2 class="mr-4 bg-green-100 rounded-xl h-10 w-10 inline-flex items-center justify-center">
                    <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-5 fill-green-600" viewBox="0 0 20 20">
                        <path fill-rule="evenodd" d="M10 18a8 8 0 100-16 8 8 0 000 16zm3.707-9.293a1 1 0 00-1.414-1.414L9 10.586 7.707 9.293a1 1 0 00-1.414 1.414l2 2a1 1 0 001.414 0l4-4z" clip-rule="evenodd" />
                      </svg>
                </h2>
                <div class="flex flex-col justify-between">
                    <h2 class="font-medium text-xl">Total Diterima Proposal</h2>
                    <h2 class="font-semibold text-6xl">{{ $pemohons->where('validasiPemohonan', '2')->count() }}</h2>
                </div>
            </div>
    
            {{-- card --}}
            <div class="bg-white rounded-2xl drop-shadow-3xl p-6 w-full h-[220px] flex">
                <h2 class="mr-4 bg-[#F5E2E2] rounded-xl h-10 w-10 inline-flex items-center justify-center">
                    <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-5 fill-[#D25353]" viewBox="0 0 20 20 ">
                        <path fill-rule="evenodd" d="M8.257 3.099c.765-1.36 2.722-1.36 3.486 0l5.58 9.92c.75 1.334-.213 2.98-1.742 2.98H4.42c-1.53 0-2.493-1.646-1.743-2.98l5.58-9.92zM11 13a1 1 0 11-2 0 1 1 0 012 0zm-1-8a1 1 0 00-1 1v3a1 1 0 002 0V6a1 1 0 00-1-1z" clip-rule="evenodd" />
                      </svg>
                        
                </h2>
                <div class="flex flex-col justify-between">
                    <h2 class="font-medium text-xl">Total Proposal Di Tolak</h2>
                    <h2 class="font-semibold text-6xl">{{ $pemohons->where('validasiPemohonan', '3')->count() }}</h2>
                </div>
            </div>
    
        </div>
    </div>

</x-app-layout>
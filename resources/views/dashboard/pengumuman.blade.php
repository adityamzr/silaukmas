<x-app-layout :title="__('Pengumuman')">
    <div>
        <h2 class="text-3xl lg:text-4xl mb-3">Pengumuman</h2>
        <p class="text-accent sr-only"></p>
    </div>
    <div class="bg-white rounded-lg drop-shadow-3xl p-4 lg:p-8 mt-8 lg:mt-14  text-sm lg:text-base">
        <h3 class=" text-base lg:text-xl text-gray-900 font-semibold inline-flex items-center mb-3">
            <span class="bg-blue-100 rounded-full h-10 w-10 flex items-center justify-center mr-3 ">
                <svg xmlns="http://www.w3.org/2000/svg" class=" max-h-6 max-w-6 w-full h-full stroke-blue-500" fill="none" viewBox="0 0 24 24"
                     stroke-width="2">
                    <path stroke-linecap="round" stroke-linejoin="round"
                        d="M11 5.882V19.24a1.76 1.76 0 01-3.417.592l-2.147-6.15M18 13a3 3 0 100-6M5.436 13.683A4.001 4.001 0 017 6h1.832c4.1 0 7.625-1.234 9.168-3v14c-1.543-1.766-5.067-3-9.168-3H7a3.988 3.988 0 01-1.564-.317z" />
                </svg></span>
            Informasi jadwal pengajuan proposal </h3>
        <div class="text-gray-600 space-y-2">
            {{-- timeline --}}

            @if ($jadwal->status == 1)
            <ol class="relative border-l border-blue-200 ml-5 mt-3">
                <li class="mb-10 ml-4">
                    <div
                        class="absolute w-3 h-3 bg-blue-500 rounded-full mt-1.5 -left-1.5 border border-white ">
                    </div>
                    <h3 class="text-md font-semibold text-gray-900 ">Pengajuan Proposal</h3>
                    <p class="mb-4 text-sm font-normal text-gray-500 ">Pengajuan proposal akan dibuka dari tanggal <span class="font-semibold">{{ $pengajuan['awalProposal'] }}</span>  sampai dengan <span class="font-semibold"> {{ $pengajuan['akhirProposal'] }}</span></p>
                    <p class="text-sm">Download surat keterangan penerima bantuan disini <a class="text-blue-500 underline hover:text-blue-700"  href="/downloadSuratKebijakan">Surat Keterangan</a> </p>
                   
                </li>
                <li class="mb-10 ml-4">
                    <div
                        class="absolute w-3 h-3 bg-blue-500 rounded-full mt-1.5 -left-1.5 border border-white ">
                    </div>
                    <h3 class="text-md font-semibold text-gray-900 ">Verifikasi Proposal</h3>
                    <p class="mb-4 text-sm font-normal text-gray-500 ">Verifikasi proposal akan dilakukan dari tanggal <span class="font-semibold">{{ $pengajuan['awalVerifikasi'] }}</span>  sampai dengan <span class="font-semibold"> {{ $pengajuan['akhirVerifikasi'] }}</span></p>
                </li>
                <li class="ml-4">
                    <div
                        class="absolute w-3 h-3 bg-blue-500 rounded-full mt-1.5 -left-1.5 border border-white ">
                    </div>
                    <h3 class="text-md font-semibold text-gray-900 ">Pengambilan Benih</h3>
                    <p class="mb-4 text-sm font-normal text-gray-500 ">Pengambilan Benih akan dimulai dari tanggal <span class="font-semibold">{{ $pengajuan['awalPengambilan'] }}</span>  sampai dengan <span class="font-semibold"> {{ $pengajuan['akhirPengambilan'] }}</span></p>
                </li>
                
            </ol>

            @else

            <p>Pengajuan Proposal Sedang Ditutup.</p>
                
            @endif
                
    
            {{-- /timeline --}}

            {{-- <p>{!! $message !!}</p>
            <p>Download surat keterangan penerima bantuan disini <a class="text-blue-500 underline hover:text-blue-700"  href="/downloadSuratKebijakan">Surat Keterangan</a> </p> --}}
        </div>
    </div>

    <div class="bg-white rounded-lg drop-shadow-3xl p-4 lg:p-8 mt-8 lg:mt-14  text-sm lg:text-base">
        <h3 class="mb-3 text-xl text-gray-900 font-medium">Daftar Proposal</h3>
        
        <table id="table_id" class="stripe hover" style="width:100%; padding-top: 1em;  padding-bottom: 1em;">
            <thead>
                <tr>
                    <th>No</th>
                    <th>No Proposal</th>
                    <th>Kategory</th>
                    <th>Jenis Benih</th>
                    <th>Status</th>
                    <th>Detail</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($pemohons as $pemohon)
                    <tr class="text-center">
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $pemohon->noProposal }}</td>
                        <td>{{ $pemohon->user->kelompokMasyarakat }}</td>
                        <td>{{ $pemohon->benih->jenisBenih }}</td>
                        <td>
                            @if ($pemohon->validasiPemohonan == 1)
                            <span class="bg-yellow-100 text-yellow-800 whitespace-nowrap text-xs font-semibold px-2.5 py-0.5 rounded inline-flex">Dalam Proses</span>
                            @elseif ($pemohon->validasiPemohonan == 2 || $pemohon->validasiPemohonan == 4)
                                <a href="/downloadSurat/{{ $pemohon->noProposal }}">
                                    <x-button class="bg-primary text-sm text-white px-2 py-1  mx-auto">
                                        Download Surat
                                        </x-button>
                                     </a>
                            @elseif ($pemohon->validasiPemohonan == 3)
                            <span class="bg-red-100 text-red-800 text-xs font-semibold px-2.5 py-0.5 rounded">Ditolak</span>
                            @endif
                        </td>
                        <td>{{ $pemohon->keterangan }}</td>
                    </tr>
                @endforeach
                
            </tbody>
        </table>
    </div>

    <x-slot name="css">
        <link href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" rel="stylesheet">
        <!--Responsive Extension Datatables CSS-->
        <link href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.dataTables.min.css" rel="stylesheet"> 
    </x-slot>
    <x-slot name="script">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js" integrity="sha512-894YE6QWD5I59HgZOGReFYm4dnWc1Qt5NtvYSaNcOP+u1T9qYdvdihz0PPSiiqn/+/3e7Jo4EaG7TubfWGUrMQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
        <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.11.5/js/jquery.dataTables.js"></script>
        <script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
    
        <script>
        $(document).ready(function() {
                
            var table = $('#table_id').DataTable({
                    info : false,
                    ordering: false,
                    "scrollX": true,
                    language: { 
                        search: "Cari : ",
                        searchPlaceholder: "Cari disini ...",
                        lengthMenu: "Menampilkan _MENU_ Data",
                        "paginate": {
                                    "previous": "Kembali",
                                    "next": "Lanjut",
                                    }
                    }
                })
                .columns.adjust()
                .responsive.recalc();
                    
            });                
        </script>
    </x-slot>

    @if (Session::has('success'))
    {{-- Alert --}}
<x-slot name="alert">
   <div x-data="{open : true }" x-show="open" 
       x-transition:enter="transition ease-out duration-300 "
       x-transition:enter-start="opacity-0 scale-90"
       x-transition:enter-end="opacity-100 scale-100"
       x-transition:leave="transition ease-in duration-300"
       x-transition:leave-start="opacity-100 scale-100"
       x-transition:leave-end="opacity-0 scale-90"

       class="flex p-4 mb-4 bg-green-100 rounded-lg dark:bg-green-200 shadow-lg" role="alert">
       <svg class="flex-shrink-0 w-5 h-5 text-green-700 dark:text-green-800" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M18 10a8 8 0 11-16 0 8 8 0 0116 0zm-7-4a1 1 0 11-2 0 1 1 0 012 0zM9 9a1 1 0 000 2v3a1 1 0 001 1h1a1 1 0 100-2v-3a1 1 0 00-1-1H9z" clip-rule="evenodd"></path></svg>
       <div class="ml-3 text-sm font-medium text-green-700 dark:text-green-800">
         Data Profile Berhasil Diupdate
       </div>
       <button @click="open = ! open" x-init="setTimeout(() => open = false, 4000)" type="button" class="ml-auto -mx-1.5 -my-1.5 bg-green-100 text-green-500 rounded-lg focus:ring-2 focus:ring-green-400 p-1.5 hover:bg-green-200 inline-flex h-8 w-8 dark:bg-green-200 dark:text-green-600 dark:hover:bg-green-300" data-dismiss-target="#alert-3" aria-label="Close">
         <span class="sr-only">Close</span>
         <svg class="w-5 h-5" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z" clip-rule="evenodd"></path></svg>
       </button>
     </div>
</x-slot>
@endif



</x-app-layout>
<x-app-layout :title="__('Pengajuan Proposal')">
    {{-- Title --}}
   <div class="mb-4">
       <h2 class="text-3xl lg:text-4xl mb-3">Konfirmasi Persetujuan Proposal
    </h2>
   </div>




    <div class="bg-white rounded-lg border-2 border-gray-400/30 py-5 px-5 lg:px-10 mt-4">

        <div class="border-b border-primary/25">
            <h2 class="font-semibold text-2xl lg:text-3xl pb-5 capitalize">{{ $administrasi->namaKetua }}</h2>
        </div>

        <form action="/konfirmasi-proposal/{{ $noProposal }}" method="post">
            @csrf
            <div>
                <x-label for="hariPengambilan" :value="__('Hari Pengambilan')" />
                <x-input class="w-full" type="text" name="hariPengambilan"  />
            </div>
            <div>
                <x-label for="tglPengambilan" :value="__('Tanggal Pengambilan')" />
                <x-input class="w-full" type="date" name="tglPengambilan"  />
            </div>
            <div>
                <x-label for="jamPengambilan" :value="__('Jam Pengambilan')" />
                <x-input class="w-full" type="time" name="jamPengambilan"  />
            </div>
            <div>
                <x-label for="lokasiPengambilan" :value="__('Lokasi Pengambilan')" />
                <x-input class="w-full" type="text" name="lokasiPengambilan"  />
            </div>
            <div>
                <x-label for="pegawaiPengurus" :value="__('Pegawai Pengurus')" />
                <x-input class="w-full" type="text" name="pegawaiPengurus"  />
            </div>

            {{-- Benih --}}

            <div class="grid grid-cols-12 gap-6">
                <div class="col-span-5">
                    <x-label for="jenisBenih" :value="__('Jenis Benih Ikan')" />
                    <x-input class="w-full" type="text" name="jenis_benih_ikan" value="{{ $benih->jenisBenih }}"  />
                </div>

                <span class="flex items-end mb-2.5 justify-center">:</span>

                <div class="col-span-5">
                    <x-label for="jumlahBenih" :value="__('Jumlah yang disetujui')" />
                    <x-input class="w-full" type="number" name="jumlah_benih_disetujui" />
                </div>
                <h2 class="flex items-end mb-2.5">ekor</h2>
            </div>

            
           
            {{-- Navigasi --}}
            <div class="flex pt-8 pb-4 mt-12 border-t border-primary/25 justify-between gap-x-3">

                <!-- Button Dummy -->
                <a href="/data-proposal/" class="max-w-[280px] w-full">
                    <x-button type="button" class="border-red-600 text-red-600 max-w-[280px] w-full justify-center">
                    Kembali
                </x-button></a>

                <!-- Button Asli -->
                {{-- <x-button type="button" class="border-red-600 text-red-600 max-w-[280px] w-full justify-center">
                    Kembali
                </x-button> --}}

                 <!-- Button Dummy -->
                {{-- <a href="/masyarakat-pembudidaya-ikan/data-lokasi" class="max-w-[280px] w-full"><x-button type="button" class=" text-white bg-primary max-w-[280px] w-full justify-center">
                    Selanjutnya
                </x-button></a> --}}

                <!-- Button Asli -->
                <x-button type="submit" class=" text-white bg-primary max-w-[280px] w-full justify-center">
                    Konfirmasi
                </x-button>

            </div>

        </form>
    </div>



</x-app-layout>
<x-app-layout :title="__('Pengajuan Proposal')">
    {{-- Title --}}
   <div>
       <h2 class="text-3xl lg:text-4xl mb-3 capitalize">Data Proposal <span class="font-medium">{{ $administrasi->namaKetua }}</span> </h2>
   </div>

    <div class="bg-white rounded-lg border-2 border-gray-400/30 mt-4 py-5 px-5 lg:px-10">

        <div class="border-b border-primary/25">
            <h2 class="font-semibold text-2xl lg:text-3xl pb-5">Profile Kelompok</h2>
        </div>

            {{-- Nama Ketua Kelompok --}}
            <div>
                <x-label for="namaKetua" :value="__('Nama Ketua / Penaggung Jawab')" />
                <x-input readonly class="w-full" type="text" name="namaKetua" :value="old('namaKetua', ($administrasi)?$administrasi->namaKetua : '')"  />
            </div>

            @if ($proposal != 'lembaga-lain')
           
            {{-- Nama Anggota --}}
            <x-label for="namaAnggota1" :value="__('Nama Anggota')" />
            <div class="grid grid-cols-1 lg:grid-cols-2 gap-x-3">
                @foreach ($anggotas as $anggota)
                        <div id="box{{ $loop->iteration }}">
                            <x-input class="w-full mt-3" type="text"  name="namaAnggota[]" :value="$anggota->namaAnggota" disabled />
                        </div>   
                @endforeach
    
            </div>
                 
            @endif

            {{--Alamat Lengkap --}}
            <div>
                <x-label for="alamatLengkap" :value="__('Alamat Lengkap')" />
                <x-textarea class="w-full" type="text" name="alamatLengkap" disabled>
                    {{ old('alamatLengkap',($administrasi)?$administrasi->alamatLengkap : '') }}
                </x-textarea>
            </div>

            <div class="grid grid-cols-1 lg:grid-cols-2 gap-x-3">
                {{-- Tahun Pendirian Kelompok --}}
                <div>
                    <x-label for="tahunPendirian" :value="__('Tahun Pendirian Kelompok')" />
                    <x-input readonly class="w-full" type="number" name="tahunPendirian" :value="old('tahunPendirian', ($administrasi)?$administrasi->tahunPendirian : '')"  />
                </div>

                {{-- Nomor Telepon --}}
                <div>
                    <x-label for="noTelp" :value="__('No. Telp')" />
                    <x-input readonly class="w-full" type="number" name="noTelp" :value="old('noTelp', ($administrasi)?$administrasi->noTelp : '')"  />
                </div>
            </div>

            {{-- Email --}}
            <div>
                <x-label for="email" :value="__('Email')" />
                <x-input readonly class="w-full" type="email" name="email" :value="old('email', ($administrasi)?$administrasi->email : '')"  />
            </div>
            
            {{-- Kordinat Lokasi --}}
            <div>
                <x-label for="koordinatLokasi" >
                    Lokasi Kolam
                </x-label>
                <iframe title="Maps Lokasi Proposal" src="https://maps.google.com/maps?q={{$administrasi->latlong->latitude}},{{$administrasi->latlong->longitude}}&hl=id&z=18&amp;output=embed" id="maps" width="100%" height="550" style="border:0;" allowfullscreen="" loading="lazy"></iframe>

            </div>
            
            {{-- Upload KTP --}}
            <div>
                <x-label for="fotoKtp" >
                    Data KTP
                </x-label>
                <div class="flex pt-2 pb-0 items-center">
                    <a href="/view/ktp/{{ $administrasi->noProposal }}" target="_blank"  class="btn mb-1 text-center cursor-pointer inline-block focus:outline-none py-2 px-5 text-xs rounded-md shadow-sm text-red-600 bg-transparent border border-red-600 font-semibold">Lihat KTP</a>
                </div>
            </div>

            {{-- Pernyataan Kesanggupan Pemohonan --}}
            <div class="border-b border-primary/25 pt-8">
                <h2 class="font-semibold text-2xl lg:text-3xl pb-5">Pernyataan Kesanggupan Pemohonan</h2>
            </div>

            {{-- Download Format --}}
            <div class="flex pt-2 pb-3 items-center">
                <div>
                    <x-label for="fotoKtp" >
                        Data Pernyataan Kesanggupan
                    </x-label>
                    <div class="flex pt-2 pb-0 items-center">
                        <a href="/view/kesanggupan/{{ $administrasi->noProposal }}" target="_blank"  class="btn mb-1 text-center cursor-pointer inline-block focus:outline-none py-2 px-5 text-xs rounded-md shadow-sm text-red-600 bg-transparent border border-red-600 font-semibold">Lihat File</a>
                    </div>
                </div>
            </div>

             {{-- Pernyataan Tidak Menerima Bantuan Sejenis --}}
             <div class="border-b border-primary/25 pt-8">
                <h2 class="font-semibold text-2xl lg:text-3xl pb-5">Pernyataan Tidak Menerima Bantuan Sejenis</h2>
            </div>

            {{-- Download Format --}}
            <div class="flex pt-2 pb-3 items-center">
                <div>
                    <x-label for="fotoKtp" >
                        Data Pernyataan Kesanggupan
                    </x-label>
                    <div class="flex pt-2 pb-0 items-center">
                        <a href="/view/bantuan-sejenis/{{ $administrasi->noProposal }}" target="_blank"  class="btn mb-1 text-center cursor-pointer inline-block focus:outline-none py-2 px-5 text-xs rounded-md shadow-sm text-red-600 bg-transparent border border-red-600 font-semibold">Lihat File</a>
                    </div>
                </div>
            </div>

            {{-- Keterangan Pembudidaya Terdaftar --}}
            <div class="border-b border-primary/25 pt-8">
                <h2 class="font-semibold text-2xl lg:text-3xl pb-5">Keterangan Pembudidaya Terdaftar</h2>
            </div>

            {{-- Instansi Tempat Pendaftaran --}}
            <div>
                <x-label for="instansiPendaftar" :value="__('Instansi Tempat Pendaftaran')" />
                <x-textarea class="w-full" type="text" name="instansiPendaftar" disabled>
                    {{ old('instansiPendaftar', ($administrasi)?$administrasi->instansiPendaftar : '') }}
                </x-textarea>
            </div>

            <div class="grid grid-cols-1 lg:grid-cols-2 gap-x-3">
                {{-- Tanggal Surat --}}
                <div>
                    <x-label for="tglSuratAdministrasi" :value="__('Tanggal Surat')" />
                    <x-input readonly class="w-full" type="date" name="tglSuratAdministrasi" :value="old('tglSuratAdministrasi', ($administrasi)?$administrasi->tglSurat : '')"  />
                </div>

                {{-- No. Surat --}}
                <div>
                    <x-label for="noSurat" :value="__('No. Surat')" />
                    <x-input readonly class="w-full" type="text" name="noSurat" :value="old('noSurat', ($administrasi)?$administrasi->noSurat : '')"  />
                </div>
            </div>

            {{-- Pejabat Penanda Tangan --}}
            <div>
                <x-label for="ttdPejabat" :value="__('Pejabat Penanda Tangan')" />
                <x-input readonly class="w-full" type="text" name="ttdPejabat" :value="old('ttdPejabat', ($administrasi)?$administrasi->ttdPejabat : '')"  />
            </div>

            {{-- Upload File --}}
            <div>
                <x-label for="fotoKtp" >
                    Data Bukti
                </x-label>
                <div class="flex pt-2 pb-0 items-center">
                    <a href="/view/bukti-terdaftar/{{ $administrasi->noProposal }}" target="_blank"  class="btn mb-1 text-center cursor-pointer inline-block focus:outline-none py-2 px-5 text-xs rounded-md shadow-sm text-red-600 bg-transparent border border-red-600 font-semibold">Lihat Bukti</a>
                </div>
            </div>

            {{-- Bukti Penetapan Masyarakat hukum adat --}}
            @if ($proposal == 'hukum-adat')
                <div class="border-b border-primary/25 pt-14">
                    <h2 class="font-semibold text-2xl lg:text-3xl pb-5">Bukti Penetapan Masyarakat Hukum Adat</h2>
                </div>

                {{-- Upload File --}}
                <div>
                    <x-label for="fotoKtp" >
                        Data Dokumen
                    </x-label>
                    <div class="flex pt-2 pb-0 items-center">
                        <a href="/view/bukti-hukum-adat/{{ $administrasi->noProposal }}" target="_blank"  class="btn mb-1 text-center cursor-pointer inline-block focus:outline-none py-2 px-5 text-xs rounded-md shadow-sm text-red-600 bg-transparent border border-red-600 font-semibold">Lihat Dokumen</a>
                    </div>
                </div>
            @elseif($proposal == 'swadaya-masyarakat')
                {{-- Bukti Penetapan SK Berbadan Hukum  --}}
                <div class="border-b border-primary/25 pt-14">
                    <h2 class="font-semibold text-2xl lg:text-3xl pb-5">Bukti Penetapan SK Berbadan Hukum </h2>
                </div>

                {{-- Upload File --}}
                <div>
                    <x-label for="fotoKtp" >
                        Data Dokumen
                    </x-label>
                    <div class="flex pt-2 pb-0 items-center">
                        <a href="/view/bukti-swadaya/{{ $administrasi->noProposal }}" target="_blank"  class="btn mb-1 text-center cursor-pointer inline-block focus:outline-none py-2 px-5 text-xs rounded-md shadow-sm text-red-600 bg-transparent border border-red-600 font-semibold">Lihat Dokumen</a>
                    </div>
                </div>
                @elseif ($proposal == 'lembaga-pendidikan')
                    {{-- Bukti terdaftar di Kementerian Pendidikan, agama, Pendidikan Tinggi  --}}
                    <div class="border-b border-primary/25 pt-14">
                        <h2 class="font-semibold text-2xl lg:text-3xl pb-5">Bukti terdaftar di Kementerian Pendidikan, agama, Pendidikan Tinggi </h2>
                    </div>

                    {{-- Upload File --}}
                    <div>
                        <x-label for="fotoKtp" >
                            Data Dokumen
                        </x-label>
                        <div class="flex pt-2 pb-0 items-center">
                            <a href="/view/bukti-pendidikan/{{ $administrasi->noProposal }}" target="_blank"  class="btn mb-1 text-center cursor-pointer inline-block focus:outline-none py-2 px-5 text-xs rounded-md shadow-sm text-red-600 bg-transparent border border-red-600 font-semibold">Lihat Dokumen</a>
                        </div>
                    </div>
                @elseif ($proposal == 'lembaga-keagamaan')
                    {{-- Bukti terdaftar di Kementerian Agama  --}}
                    <div class="border-b border-primary/25 pt-14">
                        <h2 class="font-semibold text-2xl lg:text-3xl pb-5">Bukti terdaftar di Kementerian Agama </h2>
                    </div>

                    {{-- Upload File --}}
                    <div>
                        <x-label for="fotoKtp" >
                            Data Dokumen
                        </x-label>
                        <div class="flex pt-2 pb-0 items-center">
                            <a href="/view/bukti-keagamaan/{{ $administrasi->noProposal }}" target="_blank"  class="btn mb-1 text-center cursor-pointer inline-block focus:outline-none py-2 px-5 text-xs rounded-md shadow-sm text-red-600 bg-transparent border border-red-600 font-semibold">Lihat Dokumen</a>
                        </div>
                    </div>
            @endif

    </div>

    <div class="bg-white rounded-lg mt-4 border-2 border-gray-400/30 py-5 px-5 lg:px-10">

        <div class="border-b border-primary/25">
            <h2 class="font-semibold text-2xl lg:text-3xl pb-5">Rekomendasi Budidaya</h2>
        </div>
            {{-- Upload Surat --}}
            <div>
                <x-label for="fotoKtp" >
                    Data Surat
                </x-label>
                <div class="flex pt-2 pb-0 items-center">
                    <a href="/view/surat/{{ $administrasi->noProposal }}" target="_blank"  class="btn mb-1 text-center cursor-pointer inline-block focus:outline-none py-2 px-5 text-xs rounded-md shadow-sm text-red-600 bg-transparent border border-red-600 font-semibold">Lihat Surat</a>
                </div>
            </div>

            {{-- Upload Upload Sertifikat --}}
            <div>
                <x-label for="fotoKtp" >
                    Data Sertifikat
                </x-label>
                <div class="flex pt-2 pb-0 items-center">
                    <a href="/view/sertifikat/{{ $administrasi->noProposal }}" target="_blank"  class="btn mb-1 text-center cursor-pointer inline-block focus:outline-none py-2 px-5 text-xs rounded-md shadow-sm text-red-600 bg-transparent border border-red-600 font-semibold">Lihat Sertifikat</a>
                </div>
            </div>
    </div>

    <div class="bg-white rounded-lg border-2 border-gray-400/30 py-5 px-5 mt-4 lg:px-10">
        <input type="hidden" id="category" value="{{ $lokasi->kategoriKolam }}">
        <input type="hidden" id="noProposal" value="{{ $lokasi->noProposal }}">
              {{-- Status Lahan Title --}}
              <div class="border-b border-primary/25 pt-6">
                <h2 class="font-semibold text-2xl lg:text-3xl pb-5">Kategori Lahan</h2>
            </div>
            <div class="grid grid-cols-1 lg:grid-cols-2 gap-x-3" id="containerJenis">
                
            </div>

            {{-- Status Lahan Title --}}
            <div class="border-b border-primary/25 pt-8">
                <h2 class="font-semibold text-2xl lg:text-3xl pb-5">Status Lahan</h2>
            </div>

            {{-- Status lahan--}}
            <div>
                <x-label for="statusLahan" >
                Status Lahan <span class="text-xs">( Sewa / Pribadi )</span>
                </x-label>
                <x-input readonly class="w-full" type="text" name="statusLahan" :value="old('statusLahan', ($lokasi)?$lokasi->statusLahan : '')"  />
            </div>

            {{-- Upload Bukti --}}
            <div>
                <x-label for="fotoKtp" >
                    Data Bukti Lahan
                </x-label>
                <div class="flex pt-2 pb-0 items-center">
                    <a href="/view/bukti-lahan/{{ $administrasi->noProposal }}" target="_blank"  class="btn mb-1 text-center cursor-pointer inline-block focus:outline-none py-2 px-5 text-xs rounded-md shadow-sm text-red-600 bg-transparent border border-red-600 font-semibold">Lihat Bukti</a>
                </div>
            </div>

            {{-- Akses Lahan Title --}}
            <div class="border-b border-primary/25 pt-14">
                <h2 class="font-semibold text-2xl lg:text-3xl pb-5">Akses Lahan</h2>
            </div>

            {{-- Jatak dari jalan utama--}}
            <div>
                <x-label for="jarakLahan" :value="__('Jarak Dari Jalan Utama')" />
                <x-input readonly class="w-full" type="text" name="jarakLahan" :value="old('jarakLahan', ($lokasi)?$lokasi->jarakLahan : '')"  />
            </div>

             {{-- Kondisi jalan--}}
             <div>
                <x-label for="kondisiJalan" >
                Kondisi Jalan <span class="text-xs">(Aspal / Beton / Paving / Batu / Tanah / Pasir )</span>
                </x-label>
                <x-input readonly class="w-full" type="text" name="kondisiJalan" :value="old('kondisiJalan', ($lokasi)?$lokasi->kondisiJalan : '')"  />
            </div>

             {{-- Akse Kendaraan--}}
             <div>
                <x-label for="aksesKendaraan" >
                Akses Kendaraan <span class="text-xs">(Roda Dua / Roda Empat / Jalan Kaki )</span>
                </x-label>
                <x-input readonly class="w-full" type="text" name="aksesKendaraan" :value="old('aksesKendaraan', ($lokasi)?$lokasi->aksesKendaraan : '')"  />
            </div>

            {{-- Upload Kondisi Jalan --}}
            <div>
                <x-label for="fotoKtp" >
                    Bukti Kondisi Jalan
                </x-label>
                <div class="flex pt-2 pb-0 items-center">
                    <a href="/view/bukti-kondisi-lahan/{{ $administrasi->noProposal }}" target="_blank"  class="btn mb-1 text-center cursor-pointer inline-block focus:outline-none py-2 px-5 text-xs rounded-md shadow-sm text-red-600 bg-transparent border border-red-600 font-semibold">Lihat Bukti</a>
                </div>
            </div>
    </div>

    <div  class="bg-white rounded-lg border-2 mt-4 border-gray-400/30 py-5 px-5 lg:px-10">

        <div class="border-b border-primary/25">
            <h2 class="font-semibold text-2xl lg:text-3xl pb-5">Pemohonan Benih</h2>
        </div>

                {{-- Jenis benih=--}}
            <div>
                <x-label for="jenisBenih" :value="__('Jenis Benih')" />
                <x-input readonly class="w-full" type="text" name="jenisBenih" :value="old('jenisBenih', ($pemohonan)?$pemohonan->jenisBenih : '')"  />
            </div>

            {{-- Jumalah benih--}}
            <div>
                <x-label for="jumlahBenih" :value="__('Jumlah Benih')" />
                <x-input readonly class="w-full" type="number" name="jumlahBenih" :value="old('jumlahBenih', ($pemohonan)?$pemohonan->jumlahBenih : '')"  />
            </div>
            
            {{-- Nomor Surat Pemohon--}}
            <div>
                <x-label for="noSuratPemohon" :value="__('Nomor Surat Pemohon')" />
                <x-input readonly class="w-full" type="text" name="noSuratPemohon" :value="old('noSuratPemohon', ($pemohonan)?$pemohonan->noSuratPemohon : '')"  />
            </div>

            {{-- Tanggal Surat Pemohon--}}
            <div>
                <x-label for="tglSuratPemohon" :value="__('Tanggal Surat Pemohon')" />
                <x-input readonly class="w-full" type="date" name="tglSuratPemohon" :value="old('tglSuratPemohon', ($pemohonan)?$pemohonan->tglSurat : '')"  />
            </div>

            {{-- Penandatanganan Surat Pemohon--}}
            <div>
                <x-label for="ttdSurat" :value="__('Pendandatangan Surat Pemohon')" />
                <x-input readonly class="w-full" type="text" name="ttdSurat" :value="old('ttdSurat', ($pemohonan)?$pemohonan->ttdSurat : '')"  />
            </div>

            {{-- Upload Surat --}}
            <div>
                <x-label for="fotoKtp" >
                    Bukti Surat
                </x-label>
                <div class="flex pt-2 pb-0 items-center">
                    <a href="/view/surat-pemohon/{{ $administrasi->noProposal }}" target="_blank"  class="btn mb-1 text-center cursor-pointer inline-block focus:outline-none py-2 px-5 text-xs rounded-md shadow-sm text-red-600 bg-transparent border border-red-600 font-semibold">Lihat Bukti</a>
                </div>
            </div>

            {{-- Navigasi --}}
            <div class="flex pt-8 pb-4 mt-12 border-t border-primary/25 justify-between gap-x-3">
             
                <x-button @click="modal = ! modal" type="button" class="border-red-600 text-red-600 max-w-[280px] w-full justify-center">
                    Tolak
                </x-button>

                {{-- <!-- Button Dummy -->
                <a  onclick="return confirm('Tolak pengajuan proposal?')" href="/tolak/{{ $administrasi->noProposal }}" class="max-w-[280px] w-full">
                    <x-button type="button" class="border-red-600 text-red-600 max-w-[280px] w-full justify-center">
                    Tolak
                </x-button></a> --}}

                <a href="/terima/{{ $administrasi->noProposal }}" class="max-w-[280px] w-full">
                <x-button type="button" class=" text-white bg-primary max-w-[280px] w-full justify-center">
                    Terima
                </x-button></a>

            </div>
        </form>
    </div>

        <template x-teleport="body">
            <x-modal>
                <form action="" method="">
                    @csrf
                    <div class="px-4 sm:px-6">
                        <p class="text-sm text-gray-500"> Apakah anda yakin menolak proposal <span
                                class="font-semibold"> {{ $administrasi->namaKetua }}</span> ?</p>
                        <p class="text-sm text-gray-500">Silahkan masukan alasan penolakan proposal</p>
                        <x-textarea name="tolak" class="w-full mt-3 h-36" />
                    </div>

                    <div class="bg-gray-50 px-4 py-3 sm:px-6 sm:flex sm:flex-row-reverse">
                        <button type="button"
                            class="w-full inline-flex justify-center rounded-md border border-transparent shadow-sm px-4 py-2 bg-red-600 text-base font-medium text-white hover:bg-red-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-red-500 sm:ml-3 sm:w-auto sm:text-sm">Tolak
                            Proposal</button>
                        <button @click="modal = ! modal" type="button"
                            class="mt-3 w-full inline-flex justify-center rounded-md border border-gray-300 shadow-sm px-4 py-2 bg-white text-base font-medium text-gray-700 hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500 sm:mt-0 sm:ml-3 sm:w-auto sm:text-sm">Cancel</button>
                    </div>
                </form>
            </x-modal>
        </template>

    <script>  
        $(document).ready(function(){  
            var kategori = $('#category').val()
            var no = $('#noProposal').val()
            console.log(kategori)
;            $_token = "{{ csrf_token() }}";
                $.ajax({
                    headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') },
                    url: "{{ url('/ajaxKolam2') }}",
                    type: 'POST',
                    cache: false,
                    data: {  '_token': $_token, 'jenisKolam': kategori, 'noProposal' : no  },
                    success: function(data) {
                        $('#containerJenis').html('');
                        $('#containerJenis').html(data);
                    }
                });
        });  
    
    </script>
</x-app-layout>
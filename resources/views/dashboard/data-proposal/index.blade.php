<x-app-layout :title="__('Data Pengajuan Proposal')">
    {{-- Title --}}
   <div>
        <h2 class="text-3xl lg:text-4xl mb-3">Data Pengajuan Proposal</h2>
        <p class="text-accent">Merupakan data pengajuan proposal</p>
    </div>

    <div class="bg-white rounded-xl drop-shadow-3xl p-4 lg:p-8 mt-14  text-sm lg:text-base">

        <table id="table_id" class="stripe hover " style="width:100%; padding-top: 1em;  padding-bottom: 1em;" >
            <thead>
                <tr>
                    <th>No</th>
                    <th>Nama Pemohon</th>
                    <th>Kategory</th>
                    <th>Jenis Benih</th>
                    <th>Jumlah Benih</th>
                    <th>Syarat Administrasi</th>
                    <th>Syarat Teknis</th>
                    <th>Syarat Lokasi</th>
                    <th>Data Pemohon</th>
                    <th>Aksi</th>
                  
                </tr>
            </thead>
            <tbody>
                @foreach ($proposals as $proposal)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $proposal->administrasi->namaKetua }}</td>
                        <td>{{ $proposal->user->kelompokMasyarakat }}</td>
                        <td>{{ $proposal->benih->jenisBenih }}</td>
                        <td>{{ $proposal->benih->jumlahBenih }}</td>
                        <td>{{ ($proposal->syaratAdministrasi == 1) ? 'Lengkap' : 'Tidak Lengkap'  }}</td>
                        <td>{{ ($proposal->syaratTeknis == 1) ? 'Lengkap' : 'Tidak Lengkap'  }}</td>
                        <td>{{ ($proposal->syaratLokasi == 1) ? 'Lengkap' : 'Tidak Lengkap'  }}</td>
                        <td >
                            <div x-data="{modal2: false}">

                                <a href="/data-proposal/{{ $proposal->noProposal }}">
                                    <x-button class="bg-primary text-sm text-white px-2 py-1  mx-auto">
                                    Lihat Data
                                    </x-button>
                                </a>
                            </div>
                        </td>
                        <td>
                            @if ($proposal->validasiPemohonan == 2 || $proposal->validasiPemohonan == 4)
                            <span class="bg-green-100 text-green-800 text-xs font-semibold mr-2 px-2.5 py-0.5 rounded">Selesai</span>
                            
                            @elseif ($proposal->validasiPemohonan == 3)
                            
                            <span class="bg-red-100 text-red-800 text-xs font-semibold mr-2 px-2.5 py-0.5 rounded">Ditolak</span>
                            @else
                            <div class="flex space-x-2">
                              
                                
                                <x-button class="bg-red-500 text-white flex justify-center items-center py-1 px-1 w-full open-modal" data-id="{{ $proposal->noProposal }}" data-nama="{{ $proposal->administrasi->namaKetua }}">
                                    <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-5" viewBox="0 0 20 20" fill="currentColor">
                                        <path fill-rule="evenodd" d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z" clip-rule="evenodd" />
                                    </svg>
                                </x-button>
                                {{-- <a onclick="return confirm('Tolak pengajuan proposal?')" href="/tolak/{{ $proposal->noProposal }}" >
                                <x-button class="bg-red-500 text-white block py-1 px-1 w-full">
                                    <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-5" viewBox="0 0 20 20" fill="currentColor">
                                        <path fill-rule="evenodd" d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z" clip-rule="evenodd" />
                                    </svg>
                                </x-button></a> --}}
                                <a href="/terima/{{ $proposal->noProposal }}" >
                                <x-button class="bg-green-500 text-white block py-1 px-1 w-full">
                                    <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-5" viewBox="0 0 20 20" fill="currentColor">
                                        <path fill-rule="evenodd" d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z" clip-rule="evenodd" />
                                    </svg>
                                </x-button></a>
                            </div> 
                            @endif
                            
                        </td>
                    </tr>


                @endforeach
            </tbody>
        </table>
{{-- Modal --}}
<x-slot name="modal">
    <div  class="relative z-50 modal">
        <div class="fixed inset-0 bg-gray-500/50 filter backdrop-blur-lg bg-opacity-75"></div>
        <div class="fixed z-10 inset-0 overflow-y-auto">
            <div class="flex items-end sm:items-center justify-center min-h-full p-4 text-center sm:p-0">

                <div class="relative bg-white rounded-lg text-left overflow-hidden shadow-xl transform sm:my-8 sm:max-w-lg sm:w-full"
                   >
                    <div class="bg-white px-4 pt-5 pb-4 sm:p-6 sm:pb-4">
                        <div class="sm:flex items-center">
                            <div
                                class="mx-auto flex-shrink-0 flex items-center justify-center h-12 w-12 rounded-full bg-red-100 sm:mx-0 sm:h-10 sm:w-10">
                                <!-- Heroicon name: outline/exclamation -->
                                <svg class="h-6 w-6 text-red-600" xmlns="http://www.w3.org/2000/svg" fill="none"
                                    viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" aria-hidden="true">
                                    <path stroke-linecap="round" stroke-linejoin="round"
                                        d="M12 9v2m0 4h.01m-6.938 4h13.856c1.54 0 2.502-1.667 1.732-3L13.732 4c-.77-1.333-2.694-1.333-3.464 0L3.34 16c-.77 1.333.192 3 1.732 3z" />
                                </svg>
                            </div>
                            <div class="mt-3 text-center sm:mt-0 sm:ml-4 sm:text-left w-full">
                                <h3 class="text-lg leading-6 font-medium text-gray-900" id="modal-title">Tolak Proposal</h3>
                                <div class="mt-2">
                                </div>

                            </div>
                        </div>
                    </div>
                
                    <form action="/tolak-proposal" method="post">
                        @csrf
                        <div class="px-4 sm:px-6">
                            <p class="text-sm text-gray-500"> Apakah anda yakin menolak proposal <span
                                    class="font-semibold" id="namaKetua"></span> ?</p>
                            <p class="text-sm text-gray-500">Silahkan masukan alasan penolakan proposal</p>
                            <input type="hidden" name="noProposal" id="noProposal">
                            <x-textarea name="keterangan" class="w-full mt-3 h-36" />
                        </div>
    
                        <div class="bg-gray-50 px-4 py-3 sm:px-6 sm:flex sm:flex-row-reverse">
                            <button type="submit"
                                class="w-full inline-flex justify-center rounded-md border border-transparent shadow-sm px-4 py-2 bg-red-600 text-base font-medium text-white hover:bg-red-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-red-500 sm:ml-3 sm:w-auto sm:text-sm ">Tolak
                                Proposal</button>
                            <button type="button"
                                class="mt-3 w-full inline-flex justify-center rounded-md border border-gray-300 shadow-sm px-4 py-2 bg-white text-base font-medium text-gray-700 hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500 sm:mt-0 sm:ml-3 sm:w-auto sm:text-sm close-modal">Cancel</button>
                        </div>
                    </form>
                    
                </div>
            </div>
        </div>
    </div>
    </x-slot>
       

    </div>



    <x-slot name="css">
        <link href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" rel="stylesheet">
        <!--Responsive Extension Datatables CSS-->
        <link href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.dataTables.min.css" rel="stylesheet"> 
    </x-slot>
    
    <x-slot name="script">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js" integrity="sha512-894YE6QWD5I59HgZOGReFYm4dnWc1Qt5NtvYSaNcOP+u1T9qYdvdihz0PPSiiqn/+/3e7Jo4EaG7TubfWGUrMQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
        <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.11.5/js/jquery.dataTables.js"></script>
        <script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
    
        <script>

                $(".open-modal").click(function(){
                    $(".modal").addClass("is-visible");
                    var id=$(this).data('id');
                    var namaKetua=$(this).data('nama');
                    $('#namaKetua').text(namaKetua);
                    $('#noProposal').val(id);
                });

                $(".close-modal").click(function(){
                    $(".modal").removeClass("is-visible");
                });

        $(document).ready(function() {
                
            var table = $('#table_id').DataTable({
                    "scrollX": true,
                    info : false,
                    ordering: false,
                    language: { 
                        search: "Cari : ",
                        searchPlaceholder: "Cari disini ...",
                        lengthMenu: "Menampilkan _MENU_ Data",
                        "paginate": {
                                    "previous": "Kembali",
                                    "next": "Lanjut",
                                    }
                    }
                })
                .columns.adjust()
                .responsive.recalc();
                    
            });                

           
        </script>
    </x-slot>

</x-app-layout>
<x-app-layout :title="__('Peta Kolam')">
    {{-- @dd($petas) --}}
    {{-- Title --}}
    <div>
        <h2 class="text-3xl lg:text-4xl mb-3">Peta kolam</h2>
        <p class="text-accent">Informasi Lokasi kolam</p>
    </div>

    <div class="bg-white rounded-xl drop-shadow-3xl p-4 lg:p-8 mt-8 lg:mt-14  text-sm lg:text-base">
        <div id="map"></div>
    </div>

    <x-slot name="css">
        <link rel="stylesheet" href="https://unpkg.com/leaflet@1.8.0/dist/leaflet.css" integrity="sha512-hoalWLoI8r4UszCkZ5kL8vayOGVae1oxXe/2A4AO6J9+580uKHDO3JdHb7NzwwzK5xr/Fs0W40kiNHxM9vyTtQ==" crossorigin=""/>
        <script src="https://unpkg.com/leaflet@1.8.0/dist/leaflet.js" integrity="sha512-BB3hKbKWOc9Ez/TAwyWxNXeoV9c1v6FIeYiBieIWkpLjauysF18NzgR1MBNBXf8/KABdlkX68nAhlwcDFLGPCQ==" crossorigin=""></script>
    </x-slot>

    <x-slot name="script">
        <script>
            var lokasi = {!! json_encode($petas->toArray()) !!};
            var map = L.map('map').setView([-6.917464, 107.619125], 13);
            var tiles = L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
                maxZoom: 18,
                attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, ' +
                    'Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
                id: 'mapbox/streets-v11',
                tileSize: 512,
                zoomOffset: -1
            }).addTo(map);

                lokasi.forEach(function(peta){
                    console.log(peta.latitude)
                    console.log(peta.longitude)
                    var marker = L.marker([peta.latitude, peta.longitude]).addTo(map)
                    .bindPopup('<b>Data Lokasi </b><br /> <a href="/data-proposal/'+peta.noProposal+'">'+ peta.administrasi.namaKetua + '</a>'  ).openPopup();
                });
        </script>
    </x-slot>
</x-app-layout>
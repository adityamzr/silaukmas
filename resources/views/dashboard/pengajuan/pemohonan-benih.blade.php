<x-app-layout :title="__('Pengajuan Proposal')">
    {{-- Title --}}
   <div>
       <h2 class="text-3xl lg:text-4xl mb-3">Pengajuan Proposal</h2>
       <p class="text-accent">{{ $judul }} </p>
   </div>

   {{-- Step Bar --}}
   <div class="my-4 mx-2">
    <ol class="grid grid-cols-2 lg:grid-cols-6 w-full mx-auto">
        
    <x-step :active="('bg-primary')" :name="('Data Administrasi')">
        <svg xmlns="http://www.w3.org/2000/svg" class="h-8 w-8 fill-white" viewBox="0 0 20 20">
            <path d="M13 6a3 3 0 11-6 0 3 3 0 016 0zM18 8a2 2 0 11-4 0 2 2 0 014 0zM14 15a4 4 0 00-8 0v3h8v-3zM6 8a2 2 0 11-4 0 2 2 0 014 0zM16 18v-3a5.972 5.972 0 00-.75-2.906A3.005 3.005 0 0119 15v3h-3zM4.75 12.094A5.973 5.973 0 004 15v3H1v-3a3 3 0 013.75-2.906z" />
          </svg>
    </x-step>

    <x-step :active="('bg-primary')" :name="('Data Teknis')">
        <svg xmlns="http://www.w3.org/2000/svg" class="h-8 w-8 fill-white" viewBox="0 0 20 20">
            <path fill-rule="evenodd" d="M6 2a2 2 0 00-2 2v12a2 2 0 002 2h8a2 2 0 002-2V7.414A2 2 0 0015.414 6L12 2.586A2 2 0 0010.586 2H6zm5 6a1 1 0 10-2 0v2H7a1 1 0 100 2h2v2a1 1 0 102 0v-2h2a1 1 0 100-2h-2V8z" clip-rule="evenodd" />
          </svg>
    </x-step>

    <x-step :active="('bg-primary')" :name="('Data Lokasi')">
        <svg xmlns="http://www.w3.org/2000/svg" class="h-8 w-8 fill-white" viewBox="0 0 20 20">
            <path fill-rule="evenodd" d="M5.05 4.05a7 7 0 119.9 9.9L10 18.9l-4.95-4.95a7 7 0 010-9.9zM10 11a2 2 0 100-4 2 2 0 000 4z" clip-rule="evenodd" />
          </svg>
    </x-step>

    <x-step :active="('bg-primary')" :name="('Pemohonan Benih')">
        <svg xmlns="http://www.w3.org/2000/svg" class="h-8 w-8 fill-white" viewBox="0 0 20 20" fill="currentColor">
            <path fill-rule="evenodd" d="M6 2a2 2 0 00-2 2v12a2 2 0 002 2h8a2 2 0 002-2V7.414A2 2 0 0015.414 6L12 2.586A2 2 0 0010.586 2H6zm5 6a1 1 0 10-2 0v2H7a1 1 0 100 2h2v2a1 1 0 102 0v-2h2a1 1 0 100-2h-2V8z" clip-rule="evenodd" />
          </svg>
     </x-step>

        <li class="relative mb-6 sm:mb-0">
            <div class="flex items-center">
                <div class="flex z-10 justify-center items-center w-14 h-14 bg-accent rounded-full shrink-0">
                    <svg xmlns="http://www.w3.org/2000/svg" class="h-8 w-8 fill-white" viewBox="0 0 20 20">
                        <path d="M9 2a1 1 0 000 2h2a1 1 0 100-2H9z" />
                        <path fill-rule="evenodd" d="M4 5a2 2 0 012-2 3 3 0 003 3h2a3 3 0 003-3 2 2 0 012 2v11a2 2 0 01-2 2H6a2 2 0 01-2-2V5zm3 4a1 1 0 000 2h.01a1 1 0 100-2H7zm3 0a1 1 0 000 2h3a1 1 0 100-2h-3zm-3 4a1 1 0 100 2h.01a1 1 0 100-2H7zm3 0a1 1 0 100 2h3a1 1 0 100-2h-3z" clip-rule="evenodd" />
                      </svg>
                </div>
            </div>
            <div class="mt-3 sm:pr-8">
                <h3 class="text-lg  text-black font-semibold ">Lampiran</h3>
            </div>
        </li> 
        <li class="text-3xl font-medium flex flex-col items-center mr-auto">
            <h1>80</h1>
            <span>%</span>
        </li>
    </ol>
</div>
{{-- /Step Bar --}}

    <div class="bg-white rounded-lg border-2 border-gray-400/30 py-5 px-5 lg:px-10">

        <div class="border-b border-primary/25">
            <h2 class="font-semibold text-2xl lg:text-3xl pb-5">Pemohonan Benih</h2>
        </div>

        <form action="{{ route('benih') }}" method="post" enctype="multipart/form-data">
            @csrf
                {{-- Jenis benih=--}}

                <div class="grid grid-cols-1 lg:grid-cols-2 gap-x-3">
                    <div>
                        <x-label for="jenisBenih" :value="__('Jenis Benih')" />
                        <x-select class="w-full" name="jenisBenih" :value="old('jenisBenih')" required>
                            @foreach ($jenisBenihs as $jenisBenih)
                            <option value="{{ $jenisBenih->valueComboBox }}" {{ ($jenisBenih->valueComboBox == old('jenisBenih', ($pemohonan)?$pemohonan->jenisBenih : ''))? 'selected' : '' }}>{{ $jenisBenih->valueComboBox }}</option>
                            @endforeach
                        </x-select>
                    </div>
        
                    {{-- Jumalah benih--}}
                    <div>
                        <x-label for="jumlahBenih" :value="__('Jumlah Benih')" />
                        <x-input class="w-full" type="number" name="jumlahBenih" :value="old('jumlahBenih', ($pemohonan)?$pemohonan->jumlahBenih : '')"  />
                    </div>
                    
                    {{-- Nomor Surat Pemohon--}}
                    <div>
                        <x-label for="noSuratPemohon" :value="__('Nomor Surat Pemohon')" />
                        <x-input class="w-full" type="text" name="noSuratPemohon" :value="old('noSuratPemohon', ($pemohonan)?$pemohonan->noSuratPemohon : '')"  />
                    </div>
        
                    {{-- Tanggal Surat Pemohon--}}
                    <div>
                        <x-label for="tglSurat" :value="__('Tanggal Surat Pemohon')" />
                        <x-input class="w-full" type="date" name="tglSurat" :value="old('tglSurat', ($pemohonan)?$pemohonan->tglSurat : '')"  />
                    </div>

                    {{-- Penandatanganan Surat Pemohon--}}
                    <div>
                        <x-label for="ttdSurat" :value="__('Pendandatangan Surat Pemohon')" />
                        <x-input class="w-full" type="text" name="ttdSurat" :value="old('ttdSurat', ($pemohonan)?$pemohonan->ttdSurat : '')"  />
                    </div>
                </div>

           
            {{-- Navigasi --}}
            <div class="flex pt-8 pb-4 mt-12 border-t border-primary/25 justify-between gap-x-3">

                <!-- Button Dummy -->
                <a href="/pengajuan-proposal/data-lokasi" class="max-w-[280px] w-full">
                    <x-button type="button" class="border-red-600 text-red-600 max-w-[280px] w-full justify-center">
                    Kembali
                </x-button></a>

                <!-- Button Asli -->
                {{-- <x-button type="button" class="border-red-600 text-red-600 max-w-[280px] w-full justify-center">
                    Kembali
                </x-button> --}}

                <x-button type="submit" class=" text-white bg-primary max-w-[280px] w-full justify-center">
                    Selanjutnya
                </x-button>

            </div>

        </form>
    </div>

    @if (Session::has('success'))
    {{-- Alert --}}
    <x-slot name="alert">
        <div x-data="{open : true }" x-show="open" 
            x-transition:enter="transition ease-out duration-300"
            x-transition:enter-start="opacity-0 scale-90"
            x-transition:enter-end="opacity-100 scale-100"
            x-transition:leave="transition ease-in duration-300"
            x-transition:leave-start="opacity-100 scale-100"
            x-transition:leave-end="opacity-0 scale-90"

            class="flex p-4 mb-4 bg-green-100 rounded-lg dark:bg-green-200 shadow-lg" role="alert">
            <svg class="flex-shrink-0 w-5 h-5 text-green-700 dark:text-green-800" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M18 10a8 8 0 11-16 0 8 8 0 0116 0zm-7-4a1 1 0 11-2 0 1 1 0 012 0zM9 9a1 1 0 000 2v3a1 1 0 001 1h1a1 1 0 100-2v-3a1 1 0 00-1-1H9z" clip-rule="evenodd"></path></svg>
            <div class="ml-3 text-sm font-medium text-green-700 dark:text-green-800">
              Data Lokasi berhasil disimpan
            </div>
            <button @click="open = ! open" x-init="setTimeout(() => open = false, 4000)" type="button" class="ml-auto -mx-1.5 -my-1.5 bg-green-100 text-green-500 rounded-lg focus:ring-2 focus:ring-green-400 p-1.5 hover:bg-green-200 inline-flex h-8 w-8 dark:bg-green-200 dark:text-green-600 dark:hover:bg-green-300" data-dismiss-target="#alert-3" aria-label="Close">
              <span class="sr-only">Close</span>
              <svg class="w-5 h-5" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z" clip-rule="evenodd"></path></svg>
            </button>
          </div>
    </x-slot>
    @endif


</x-app-layout>

{{-- // Sungai? --}}

<div>
    <x-label for="namaSungai" :value="__('Nama Sungai')" />
    <x-input class="w-full" type="text" name="namaSungai" :value="old('namaSungai',($sungai)?$sungai->namaSungai:'')"  />
</div>

{{-- Jumlah sungai --}}
<div>
    <x-label for="lebarSungai" :value="__('Lebar Sungai')" />
    <x-input class="w-full" type="text" name="lebarSungai" :value="old('lebarSungai',($sungai)?$sungai->lebarSungai:'')"  />
</div>

{{-- Keladaman Sungai --}}
<div>
    <x-label for="kedalamanSungai" :value="__('Kedalaman Sungai')" />
    <x-input class="w-full" type="text" name="kedalamanSungai" :value="old('kedalamanSungai',($sungai)?$sungai->kedalamanSungai:'')"  />
</div>

{{-- Tanah Sumber Air --}}
<div>
    <x-label for="panjangSungai" :value="__('Panjang Sungai')" />
    <x-input class="w-full" type="text" name="panjangSungai" :value="old('panjangSungai',($sungai)?$sungai->panjangSungai:'')"  />
</div>
{{-- Lokasi --}}
<div>
    <x-label for="longitude" :value="__('Longitude')" />
    <x-input class="w-full" type="text" name="longitude" :value="old('longitude',($latlong)?$latlong->longitude:'')"  />
</div>
<div>
    <x-label for="latitude" :value="__('Latitude')" />
    <x-input class="w-full" type="text" name="latitude" :value="old('latitude',($latlong)?$latlong->latitude:'')"  />
</div>


<div class="col-span-2 mt-3">
    <p class="font-semibold italic">*Silahkan kunjungi halaman berikut untuk dapat mengetahui 
      latitude dan longitude lokasi anda <a class="underline ml-2 text-blue-500 hover:text-blue-900 not-italic" href="https://www.latlong.net/" target="_blank">www.latlong.net</a></p>  
  </div>
  
  




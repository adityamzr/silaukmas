 
     {{-- Luas kolam --}}
    <div>
        <x-label for="luasKolam" :value="__('Luas Kolam (m2)')" />
        <x-input class="w-full" type="text" name="luasKolam" :value="old('luasKolam',($kolam)?$kolam->luasKolam:'')"  />
    </div>

    {{-- Jumlah kolam --}}
    <div>
        <x-label for="jumlahKolam" :value="__('Jumlah Kolam')" />
        <x-input class="w-full" type="text" name="jumlahKolam" :value="old('jumlahKolam',($kolam)?$kolam->jumlahKolam:'')"  />
    </div>

    {{-- Keladaman kolam --}}
    <div>
        <x-label for="kedalamanKolam" :value="__('Kedalaman Kolam (cm)')" />
        <x-input class="w-full" type="text" name="kedalamanKolam" :value="old('kedalamanKolam',($kolam)?$kolam->kedalamanKolam:'')"  />
    </div>
    {{-- Bangunan kolam --}}
    <div>
        <x-label for="bangunanKolam" :value="__('Bangunan Kolam')" />
        <x-select class="w-full" name="bangunanKolam" :value="old('bangunanKolam')" required>
            @foreach ($bangunanKolams as $bangunanKolam)
            <option value="{{ $bangunanKolam->valueComboBox }}" {{ ($bangunanKolam->valueComboBox == old('bangunanKolam',($kolam)?$kolam->bangunanKolam:''))? 'selected' : '' }}>{{ $bangunanKolam->valueComboBox }}</option>
            @endforeach
        </x-select>
    </div>

    {{-- Tanah Sumber Air --}}
    <div>
        <x-label for="tanahSumber" :value="__('Sumber Air')" />
        <x-select class="w-full" name="tanahSumber" :value="old('tanahSumber',($kolam)?$kolam->tanahSumber:'')" required>
         
            <option value="Air Laut" {{ old('tanahSumber') == "Air Laut" ? 'selected' : '' }}> Air Laut</option>
            <option value="Air Hujan"  {{ old('tanahSumber') == "Air Hujan" ? 'selected' : '' }}> Air Hujan</option>
            <option value="Air Permukaan"  {{ old('tanahSumber') == "Air Permukaan" ? 'selected' : '' }}> Air Permukaan</option>
            <option value="Air Tanah"  {{ old('tanahSumber') == "Air Tanah" ? 'selected' : '' }}> Air Tanah</option>
            <option value="Mata Air"  {{ old('tanahSumber') == "Mata Air" ? 'selected' : '' }}> Mata Air</option>
         
        </x-select>
    </div>

    {{-- <div>
        <x-label for="tanahSumber" :value="__('Sumber Air')" />
        <x-input class="w-full" type="text" name="tanahSumber" :value="old('tanahSumber',($kolam)?$kolam->tanahSumber:'')"  />
    </div> --}}

    <div></div>

    <div>
        <x-label for="longitude" :value="__('longitude')" />
        <x-input class="w-full" type="text" name="longitude" :value="old('longitude',($latlong)?$latlong->longitude:'')"  />
    </div>
    <div>
        <x-label for="latitude" :value="__('latitude')" />
        <x-input class="w-full" type="text" name="latitude" :value="old('latitude',($latlong)?$latlong->latitude:'')"  />
    </div>
    <div class="col-span-2 mt-3">
        <p class="font-semibold italic">*Silahkan kunjungi halaman berikut untuk dapat mengetahui 
          latitude dan longitude lokasi anda <a class="underline ml-2 text-blue-500 hover:text-blue-900 not-italic" href="https://www.latlong.net/" target="_blank">www.latlong.net</a></p>  
      </div>

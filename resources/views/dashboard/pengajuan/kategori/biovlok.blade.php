
{{-- // bioflok --}}

<div>
    <x-label for="jumlahBiovlok" :value="__('Jumlah Biovlok')" />
    <x-input class="w-full" type="text" name="jumlahBiovlok" :value="old('jumlahBiovlok',($biovlok)?$biovlok->jumlahBiovlok:'')"  />
</div>

{{-- Jumlah Biovlok --}}
<div>
    <x-label for="diameterBiovlok" :value="__('Diameter Biovlok')" />
    <x-input class="w-full" type="text" name="diameterBiovlok" :value="old('diameterBiovlok',($biovlok)?$biovlok->diameterBiovlok:'')"  />
</div>

{{-- Keladaman Biovlok --}}
<div>
    <x-label for="luasBiovlok" :value="__('Luas Biovlok')" />
    <x-input class="w-full" type="text" name="luasBiovlok" :value="old('luasBiovlok',($biovlok)?$biovlok->luasBiovlok:'')"  />
</div>

{{-- Keladaman Biovlok --}}
<div>
    <x-label for="sumberAirBiovlok" :value="__('Sumber Air Biovlok')" />
    <x-input class="w-full" type="text" name="sumberAirBiovlok" :value="old('sumberAirBiovlok',($biovlok)?$biovlok->sumberAirBiovlok:'')"  />
</div>
{{-- Lokasi --}}
<div>
    <x-label for="longitude" :value="__('longitude')" />
    <x-input class="w-full" type="text" name="longitude" :value="old('longitude',($latlong)?$latlong->longitude:'')"  />
</div>
<div>
    <x-label for="latitude" :value="__('Latitude')" />
    <x-input class="w-full" type="text" name="latitude" :value="old('latitude',($latlong)?$latlong->latitude:'')"  />
</div>

<div class="col-span-2 mt-3">
    <p class="font-semibold italic">*Silahkan kunjungi halaman berikut untuk dapat mengetahui 
      latitude dan longitude lokasi anda <a class="underline ml-2 text-blue-500 hover:text-blue-900 not-italic" href="https://www.latlong.net/" target="_blank">www.latlong.net</a></p>  
  </div>
  
  

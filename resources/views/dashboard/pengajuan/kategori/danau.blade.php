

{{-- // Danau --}}

<div>
    <x-label for="namaSitu" :value="__('Nama Situ')" />
    <x-input class="w-full" type="text" name="namaSitu" :value="old('namaSitu',($danau)?$danau->namaSitu:'')"  />
</div>

{{-- Jumlah Situ --}}
<div>
    <x-label for="luasSitu" :value="__('Lebar Situ')" />
    <x-input class="w-full" type="text" name="luasSitu" :value="old('luasSitu',($danau)?$danau->luasSitu:'')"  />
</div>

{{-- Keladaman Situ --}}
<div>
    <x-label for="kedalamanSitu" :value="__('Kedalaman Situ')" />
    <x-input class="w-full" type="text" name="kedalamanSitu" :value="old('kedalamanSitu',($danau)?$danau->kedalamanSitu:'')"  />
</div>
<div>
    
</div>
{{-- Keladaman Situ --}}
<div>
    <x-label for="longitude" :value="__('longitude')" />
    <x-input class="w-full" type="text" name="longitude" :value="old('longitude',($latlong)?$latlong->longitude:'')"  />
</div>
<div>
    <x-label for="latitude" :value="__('latitude')" />
    <x-input class="w-full" type="text" name="latitude" :value="old('latitude',($latlong)?$latlong->latitude:'')"  />
</div>

<div class="sr-only">
</div>

<div class="col-span-2 mt-3">
  <p class="font-semibold italic">*Silahkan kunjungi halaman berikut untuk dapat mengetahui 
    latitude dan longitude lokasi anda <a class="underline ml-2 text-blue-500 hover:text-blue-900 not-italic" href="https://www.latlong.net/" target="_blank">www.latlong.net</a></p>  
</div>


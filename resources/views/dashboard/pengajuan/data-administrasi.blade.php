<x-app-layout :title="__('Pengajuan Proposal')">
    {{-- Title --}}
   <div>
       <h2 class="text-3xl lg:text-4xl mb-3">Pengajuan Proposal</h2>
       <p class="text-accent"> {{ $proposal }}</p>
   </div>

   {{-- Step Bar --}}
   <div class="my-4 mx-2">
    <ol class="grid grid-cols-2 lg:grid-cols-6 w-full mx-auto">
        
    <x-step :active="('bg-primary')" :name="('Data Administrasi')">
        <svg xmlns="http://www.w3.org/2000/svg" class="h-8 w-8 fill-white" viewBox="0 0 20 20">
            <path d="M13 6a3 3 0 11-6 0 3 3 0 016 0zM18 8a2 2 0 11-4 0 2 2 0 014 0zM14 15a4 4 0 00-8 0v3h8v-3zM6 8a2 2 0 11-4 0 2 2 0 014 0zM16 18v-3a5.972 5.972 0 00-.75-2.906A3.005 3.005 0 0119 15v3h-3zM4.75 12.094A5.973 5.973 0 004 15v3H1v-3a3 3 0 013.75-2.906z" />
          </svg>
    </x-step>

    <x-step :active="('bg-accent')" :name="('Data Teknis')">
        <svg xmlns="http://www.w3.org/2000/svg" class="h-8 w-8 fill-white" viewBox="0 0 20 20">
            <path fill-rule="evenodd" d="M6 2a2 2 0 00-2 2v12a2 2 0 002 2h8a2 2 0 002-2V7.414A2 2 0 0015.414 6L12 2.586A2 2 0 0010.586 2H6zm5 6a1 1 0 10-2 0v2H7a1 1 0 100 2h2v2a1 1 0 102 0v-2h2a1 1 0 100-2h-2V8z" clip-rule="evenodd" />
          </svg>
    </x-step>

    <x-step :active="('bg-accent')" :name="('Data Lokasi')">
        <svg xmlns="http://www.w3.org/2000/svg" class="h-8 w-8 fill-white" viewBox="0 0 20 20">
            <path fill-rule="evenodd" d="M5.05 4.05a7 7 0 119.9 9.9L10 18.9l-4.95-4.95a7 7 0 010-9.9zM10 11a2 2 0 100-4 2 2 0 000 4z" clip-rule="evenodd" />
          </svg>
    </x-step>

    <x-step :active="('bg-accent')" :name="('Pemohonan Benih')">
        <svg xmlns="http://www.w3.org/2000/svg" class="h-8 w-8 fill-white" viewBox="0 0 20 20" fill="currentColor">
            <path fill-rule="evenodd" d="M6 2a2 2 0 00-2 2v12a2 2 0 002 2h8a2 2 0 002-2V7.414A2 2 0 0015.414 6L12 2.586A2 2 0 0010.586 2H6zm5 6a1 1 0 10-2 0v2H7a1 1 0 100 2h2v2a1 1 0 102 0v-2h2a1 1 0 100-2h-2V8z" clip-rule="evenodd" />
          </svg>
     </x-step>

        <li class="relative mb-6 sm:mb-0">
            <div class="flex items-center">
                <div class="flex z-10 justify-center items-center w-14 h-14 bg-accent rounded-full shrink-0">
                    <svg xmlns="http://www.w3.org/2000/svg" class="h-8 w-8 fill-white" viewBox="0 0 20 20">
                        <path d="M9 2a1 1 0 000 2h2a1 1 0 100-2H9z" />
                        <path fill-rule="evenodd" d="M4 5a2 2 0 012-2 3 3 0 003 3h2a3 3 0 003-3 2 2 0 012 2v11a2 2 0 01-2 2H6a2 2 0 01-2-2V5zm3 4a1 1 0 000 2h.01a1 1 0 100-2H7zm3 0a1 1 0 000 2h3a1 1 0 100-2h-3zm-3 4a1 1 0 100 2h.01a1 1 0 100-2H7zm3 0a1 1 0 100 2h3a1 1 0 100-2h-3z" clip-rule="evenodd" />
                      </svg>
                </div>
            </div>
            <div class="mt-3 sm:pr-8">
                <h3 class="text-lg  text-black font-semibold ">Lampiran</h3>
            </div>
        </li> 
        <li class="text-3xl font-medium flex flex-col items-center mr-auto">
            <h1>20</h1>
            <span>%</span>
        </li>
    </ol>
</div>
{{-- /Step Bar --}}

    <div class="bg-white rounded-lg border-2 border-gray-400/30 py-5 px-5 lg:px-10">

        <div class="border-b border-primary/25">
            <h2 class="font-semibold text-2xl lg:text-3xl pb-5">Profile Kelompok</h2>
        </div>

        <form action="{{ route('administrasi') }}" method="post" enctype="multipart/form-data">
            @csrf
            {{-- Nama Ketua Kelompok --}}
            <div>
                <x-label for="namaKetua" :value="__('Nama Ketua / Penaggung Jawab')" />
                <x-input class="w-full" type="text" name="namaKetua" :value="old('namaKetua', ($administrasi)?$administrasi->namaKetua : '')" autofocus />
            </div>

            <div>
                <x-label for="nik" :value="__('NIK Penanggung jawab')" />
                <x-input class="w-full" type="number" name="nikKetua" :value="old('nikKetua', ($administrasi)?$administrasi->nikKetua : '')"  />
            </div>

            @if ($proposal != 'Lembaga Lain')
           
            {{-- Nama Anggota --}}
            <x-label for="namaAnggota1" :value="__('Nama Anggota')" /> 
                <input type="hidden" name="jmlAnggota" id="jmlAnggota" value="{{ old('jmlAnggota', ($anggotas->count() == 0)? '1' : $anggotas->count()) }}">
                <button type="button" id="kurang" class="btn mb-1 text-center cursor-pointer inline-block focus:outline-none py-2 px-3 text-xs rounded-md shadow-sm text-red-600 bg-transparent border border-red-600 font-semibold">Kurang</button>
                <button type="button" id="tambah" class="btn mb-1 text-center cursor-pointer inline-block focus:outline-none py-2 px-3 text-xs rounded-md shadow-sm text-white bg-primary font-semibold">Tambah</button>
                <div class="grid grid-cols-1 lg:grid-cols-2 gap-x-3" id="containerAnggota">
                    @forelse ($anggotas as $anggota)
                        <div id="box{{ $loop->iteration }}">
                            <x-input class="w-full mt-3" type="text"  name="namaAnggota[]" :value="$anggota->namaAnggota"  />
                        </div>   
                    @empty
                    @for ($i = 1; $i <= old('jmlAnggota', '1'); $i++)
                        <div id="box{{ $i }}">
                            <x-input class="w-full mt-3" type="text" placeholder="Anggota {{ $i }}"  name="namaAnggota[]" :value="old('namaAnggota[{{ $i-1 }}]')"  />
                        </div>   
                    @endfor
                    @endforelse
                    
                </div>
                 
            @endif

            {{--Alamat Lengkap --}}
            <div>
                <x-label for="alamatLengkap" :value="__('Alamat Lengkap')" />
                <x-textarea class="w-full" type="text" name="alamatLengkap" >
                    {{ old('alamatLengkap',($administrasi)?$administrasi->alamatLengkap : '') }}
                </x-textarea>
            </div>

            <div class="grid grid-cols-1 lg:grid-cols-2 gap-x-3">
                {{-- Tahun Pendirian Kelompok --}}
                <div>
                    <x-label for="tahunPendirian" :value="__('Tahun Pendirian Kelompok')" />
                    <x-input class="w-full" type="number" name="tahunPendirian" :value="old('tahunPendirian', ($administrasi)?$administrasi->tahunPendirian : '')"  />
                </div>

                {{-- Nomor Telepon --}}
                <div>
                    <x-label for="noTelp" :value="__('No. Telp')" />
                    <x-input class="w-full" type="number" name="noTelp" :value="old('noTelp', ($administrasi)?$administrasi->noTelp : '')"  />
                </div>
            </div>

            {{-- Email --}}
            <div>
                <x-label for="email" :value="__('Email')" />
                <x-input class="w-full" type="email" name="email" :value="auth()->user()->email"  readonly/>
            </div>
            
            {{-- Kordinat Lokasi
            <div>
                <x-label for="latitude" >
                    Koordinat Latitude <span class="text-xs">( contoh : -6.193125 )</span>
                </x-label>
                <x-input class="w-full" type="text" name="latitude" :value="old('latitude', ($administrasi)?$administrasi->latitude : '')"  />
            </div>
            <div>
                <x-label for="longitude" >
                    Koordinat Longitude <span class="text-xs">( contoh :106.821810 )</span>
                </x-label>
                <x-input class="w-full" type="text" name="longitude" :value="old('longitude', ($administrasi)?$administrasi->longitude : '')"  />
            </div> --}}

            {{-- Upload KTP --}}
            <div>
                <x-label for="fotoKtp" >
                    Upload KTP Ketua dan Anggota <span class="text-xs">( Disatukan Dalam .PDF )</span>
                    <p class="text-xs text-red-600">Max. 2Mb !</p>
                </x-label>
                <div class="flex items-center justify-center md:justify-start" x-data="{ files: null, input: ''  }">
    
                    {{-- input --}}
                    <label for="fotoKtp" class="text-sm w-7/12 rounded-md shadow-sm h-[38px] border-gray-300  bg-gray-100 text-black  focus:outline-none focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50 px-2 py-2 border mb-1 overflow-x-hidden">
                        <input x-model="input" name="fotoKtp" type="file" id="fotoKtp" class="sr-only" x-on:change="files = Object.values($event.target.files)" {{ (isset($administrasi->fotoKtp)) ? '' : 'required' }}>
                        <span class="" x-text="files ? files.map(file => file.name).join(', ') : '{{ (isset($administrasi->fotoKtp)) ? 'Sudah Upload...' : '' }}'"></span>
                    </label>
    
                    {{-- navigasi file kosong --}}
                    <template x-if="!files">
                        <div class="w-5/12 max-w-[250px] mx-auto">
                            <label class="btn ml-2 mb-1 w-full text-center cursor-pointer inline-block focus:outline-none py-2 text-sm rounded-md shadow-sm text-primary bg-transparent border border-primary btn" for="fotoKtp">{{ (isset($administrasi->fotoKtp)) ? 'Ubah' : 'Choose File...' }}</label>
                        </div>
                    </template>
    
                    {{-- navigasi file isi --}}
                    <template x-if="files">
                        <div class="w-5/12 flex">
                            <label class="ml-2 mb-1 w-6/12 text-center cursor-pointer inline-block focus:outline-none py-2 text-sm rounded-md shadow-sm text-primary bg-transparent border border-primary " for="fotoKtp">Ubah</label>
                            <label id="btnReset" class="ml-2 mb-1 w-6/12 text-center cursor-pointer inline-block focus:outline-none py-2 text-sm rounded-md shadow-sm text-red-600 bg-transparent border border-red-600 " x-on:click="input = ''; files= false ">Hapus</label>
                        </div>
                    </template>
                </div>
                @error('fotoKtp')
                <p class="text-xs text-red-600 font-normal">{{ $message }}</p>  
                @enderror

            </div>

            {{-- Keterangan Pembudidaya Terdaftar --}}
            <div class="border-b border-primary/25 pt-14">
                <h2 class="font-semibold text-2xl lg:text-3xl pb-5">Keterangan Pembudidaya Terdaftar</h2>
            </div>

            {{-- Instansi Tempat Pendaftaran --}}
            <div>
                <x-label for="instansiPendaftar" :value="__('Instansi Tempat Pendaftaran')" />
                <x-textarea class="w-full" type="text" name="instansiPendaftar" >
                    {{ old('instansiPendaftar', ($administrasi)?$administrasi->instansiPendaftar : '') }}
                </x-textarea>
            </div>

            <div class="grid grid-cols-1 lg:grid-cols-2 gap-x-3">
                {{-- Tanggal Surat --}}
                <div>
                    <x-label for="tglSurat" :value="__('Tanggal Surat')" />
                    <x-input class="w-full" type="date" name="tglSurat" :value="old('tglSurat', ($administrasi)?$administrasi->tglSurat : '')"  />
                </div>

                {{-- No. Surat --}}
                <div>
                    <x-label for="noSurat" :value="__('No. Surat')" />
                    <x-input class="w-full" type="text" name="noSurat" :value="old('noSurat', ($administrasi)?$administrasi->noSurat : '')"  />
                </div>
            </div>

            {{-- Pejabat Penanda Tangan --}}
            <div>
                <x-label for="ttdPejabat" :value="__('Pejabat Penanda Tangan')" />
                <x-input class="w-full" type="text" name="ttdPejabat" :value="old('ttdPejabat', ($administrasi)?$administrasi->ttdPejabat : '')"  />
            </div>

            {{-- Upload File --}}
            <div>
                <x-label for="fileBukti">
                    Upload Bukti <span class="text-xs">( .PDF )</span>
                    <p class="text-xs text-red-600">Max. 2Mb !</p>
                </x-label>
                <div class="flex items-center justify-center md:justify-start" x-data="{ files: null, input: ''  }">
    
                    {{-- input --}}
                    <label for="fileBukti" class="text-sm w-7/12 rounded-md shadow-sm border-gray-300 h-[38px] bg-gray-100 text-black  focus:outline-none focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50 px-2 py-2 border mb-1 overflow-x-hidden">
                        <input x-model="input" name="fileBukti" id="fileBukti" type="file" class="sr-only" x-on:change="files = Object.values($event.target.files)" {{ (isset($administrasi->fileBukti)) ? '' : 'required' }}>
                        <span class="" x-text="files ? files.map(file => file.name).join(', ') : '{{ (isset($administrasi->fileBukti)) ? 'Sudah Upload...' : '' }}'"></span>
                    </label>
                   
    
                    {{-- navigasi file kosong --}}
                    <template x-if="!files">
                        <div class="w-5/12 max-w-[250px] mx-auto">
                            <label class="btn ml-2 mb-1 w-full text-center cursor-pointer inline-block focus:outline-none py-2 text-sm rounded-md shadow-sm text-primary bg-transparent border border-primary btn" for="fileBukti">{{ (isset($administrasi->fileBukti)) ? 'Ubah' : 'Choose File...' }}</label>
                        </div>
                    </template>
    
                    {{-- navigasi file isi --}}
                    <template x-if="files">
                        <div class="w-5/12 flex">
                            <label class="ml-2 mb-1 w-6/12 text-center cursor-pointer inline-block focus:outline-none py-2 text-sm rounded-md shadow-sm text-primary bg-transparent border border-primary " for="fileBukti">Ubah</label>
                            <label id="btnReset" class="ml-2 mb-1 w-6/12 text-center cursor-pointer inline-block focus:outline-none py-2 text-sm rounded-md shadow-sm text-red-600 bg-transparent border border-red-600 " x-on:click="input = ''; files= false ">Hapus</label>
                        </div>
                    </template>
                </div>
                @error('fileBukti')
                <p class="text-xs text-red-600 font-normal">{{ $message }}</p> 
                @enderror
            </div>

            {{-- Bukti Penetapan Masyarakat hukum adat --}}
            @if ($proposal == 'Kelompok Masyarakat Hukum Adat')
                <div class="border-b border-primary/25 pt-14">
                    <h2 class="font-semibold text-2xl lg:text-3xl pb-5">Bukti Penetapan Masyarakat Hukum Adat</h2>
                </div>

                {{-- Upload File --}}
                <div>
                    <x-label for="dokumenHukumAdat">
                        Upload Dokumen <span class="text-xs">( .PDF )</span>
                        <p class="text-xs text-red-600">Max. 2Mb !</p>
                    </x-label>
                    <div class="flex items-center justify-center md:justify-start" x-data="{ files: null, input: ''  }">
        
                        {{-- input --}}
                        <label for="dokumenHukumAdat" class="text-sm w-7/12 rounded-md shadow-sm border-gray-300 h-[38px] bg-gray-100 text-black  focus:outline-none focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50 px-2 py-2 border mb-1 overflow-x-hidden">
                            <input x-model="input" name="dokumenHukumAdat" id="dokumenHukumAdat" type="file" class="sr-only" x-on:change="files = Object.values($event.target.files)"{{ (isset($administrasi->dokumenHukumAdat)) ? '' : 'required' }}>
                            <span class="" x-text="files ? files.map(file => file.name).join(', ') : '{{ (isset($administrasi->dokumenHukumAdat)) ? 'Sudah Upload...' : '' }}'"></span>
                        </label>
        
                        {{-- navigasi file kosong --}}
                        <template x-if="!files">
                            <div class="w-5/12 max-w-[250px] mx-auto">
                                <label class="ml-2 mb-1 w-full text-center cursor-pointer inline-block focus:outline-none py-2 text-sm rounded-md shadow-sm text-primary bg-transparent border border-primary btn" for="dokumenHukumAdat">{{ (isset($administrasi->dokumenHukumAdat)) ? 'Ubah' : 'Choose File...' }}</label>
                            </div>
                        </template>
        
                        {{-- navigasi file isi --}}
                        <template x-if="files">
                            <div class="w-5/12 flex">
                                <label class="ml-2 mb-1 w-6/12 text-center cursor-pointer inline-block focus:outline-none py-2 text-sm rounded-md shadow-sm text-primary bg-transparent border border-primary " for="dokumenHukumAdat">Ubah</label>
                                <label id="btnReset" class="ml-2 mb-1 w-6/12 text-center cursor-pointer inline-block focus:outline-none py-2 text-sm rounded-md shadow-sm text-red-600 bg-transparent border border-red-600 " x-on:click="input = ''; files= false ">Hapus</label>
                            </div>
                        </template>
                    </div>
                    @error('dokumenHukumAdat')
                    <p class="text-xs text-red-600 font-normal">{{ $message }}</p> 
                    @enderror 
                </div>
            @elseif($proposal == 'Lembaga Swadaya Masyarakat')
                {{-- Bukti Penetapan SK Berbadan Hukum  --}}
                <div class="border-b border-primary/25 pt-14">
                    <h2 class="font-semibold text-2xl lg:text-3xl pb-5">Bukti Penetapan SK Berbadan Hukum </h2>
                </div>

                {{-- Upload File --}}
                <div>
                    <x-label for="dokumenSwadaya">
                        Upload Dokumen <span class="text-xs">( .PDF )</span>
                        <p class="text-xs text-red-600">Max. 2Mb !</p>
                    </x-label>
                    <div class="flex items-center justify-center md:justify-start" x-data="{ files: null, input: ''  }">
        
                        {{-- input --}}
                        <label for="dokumenSwadaya" class="text-sm w-7/12 rounded-md shadow-sm border-gray-300 h-[38px] bg-gray-100 text-black  focus:outline-none focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50 px-2 py-2 border mb-1 overflow-x-hidden">
                            <input x-model="input" name="dokumenSwadaya" id="dokumenSwadaya" type="file" class="sr-only" x-on:change="files = Object.values($event.target.files)"{{ (isset($administrasi->dokumenSwadaya)) ? '' : 'required' }}>
                            <span class="" x-text="files ? files.map(file => file.name).join(', ') : '{{ (isset($administrasi->dokumenSwadaya)) ? 'Sudah Upload...' : '' }}'"></span>
                        </label>
        
                        {{-- navigasi file kosong --}}
                        <template x-if="!files">
                            <div class="w-5/12 max-w-[250px] mx-auto">
                                <label class="ml-2 mb-1 w-full text-center cursor-pointer inline-block focus:outline-none py-2 text-sm rounded-md shadow-sm text-primary bg-transparent border border-primary btn" for="dokumenSwadaya">{{ (isset($administrasi->dokumenSwadaya)) ? 'Ubah' : 'Choose File...' }}</label>
                            </div>
                        </template>
        
                        {{-- navigasi file isi --}}
                        <template x-if="files">
                            <div class="w-5/12 flex">
                                <label class="ml-2 mb-1 w-6/12 text-center cursor-pointer inline-block focus:outline-none py-2 text-sm rounded-md shadow-sm text-primary bg-transparent border border-primary " for="dokumenSwadaya">Ubah</label>
                                <label id="btnReset" class="ml-2 mb-1 w-6/12 text-center cursor-pointer inline-block focus:outline-none py-2 text-sm rounded-md shadow-sm text-red-600 bg-transparent border border-red-600 " x-on:click="input = ''; files= false ">Hapus</label>
                            </div>
                        </template>
                    </div>
                    @error('dokumenSwadaya')
                    <p class="text-xs text-red-600 font-normal">{{ $message }}</p> 
                    @enderror 
                </div>
                @elseif ($proposal == 'Lembaga Pendidikan')
                    {{-- Bukti terdaftar di Kementerian Pendidikan, agama, Pendidikan Tinggi  --}}
                    <div class="border-b border-primary/25 pt-14">
                        <h2 class="font-semibold text-2xl lg:text-3xl pb-5">Bukti terdaftar di Kementerian Pendidikan, agama, Pendidikan Tinggi </h2>
                    </div>

                    {{-- Upload File --}}
                    <div>
                        <x-label for="dokumenPendidikan">
                            Upload Dokumen <span class="text-xs">( .PDF )</span>
                            <p class="text-xs text-red-600">Max. 2Mb !</p>
                        </x-label>
                        <div class="flex items-center justify-center md:justify-start" x-data="{ files: null, input: ''  }">
            
                            {{-- input --}}
                            <label for="dokumenPendidikan" class="text-sm w-7/12 rounded-md shadow-sm border-gray-300 h-[38px] bg-gray-100 text-black  focus:outline-none focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50 px-2 py-2 border mb-1 overflow-x-hidden">
                            <input x-model="input" name="dokumenPendidikan" id="dokumenPendidikan" type="file" class="sr-only" x-on:change="files = Object.values($event.target.files)"{{ (isset($administrasi->dokumenPendidikan)) ? '' : 'required' }}>
                            <span class="" x-text="files ? files.map(file => file.name).join(', ') : '{{ (isset($administrasi->dokumenPendidikan)) ? 'Sudah Upload...' : '' }}'"></span>
                        </label>
        
                        {{-- navigasi file kosong --}}
                        <template x-if="!files">
                            <div class="w-5/12 max-w-[250px] mx-auto">
                                <label class="ml-2 mb-1 w-full text-center cursor-pointer inline-block focus:outline-none py-2 text-sm rounded-md shadow-sm text-primary bg-transparent border border-primary btn" for="dokumenPendidikan">{{ (isset($administrasi->dokumenPendidikan)) ? 'Ubah' : 'Choose File...' }}</label>
                            </div>
                        </template>
            
                            {{-- navigasi file isi --}}
                            <template x-if="files">
                                <div class="w-5/12 flex">
                                    <label class="ml-2 mb-1 w-6/12 text-center cursor-pointer inline-block focus:outline-none py-2 text-sm rounded-md shadow-sm text-primary bg-transparent border border-primary " for="dokumenPendidikan">Ubah</label>
                                    <label id="btnReset" class="ml-2 mb-1 w-6/12 text-center cursor-pointer inline-block focus:outline-none py-2 text-sm rounded-md shadow-sm text-red-600 bg-transparent border border-red-600 " x-on:click="input = ''; files= false ">Hapus</label>
                                </div>
                            </template>
                        </div>
                        @error('dokumenPendidikan')
                        <p class="text-xs text-red-600 font-normal">{{ $message }}</p> 
                        @enderror
                    </div>
                @elseif ($proposal == 'Lembaga Keagamaan')
                    {{-- Bukti terdaftar di Kementerian Agama  --}}
                    <div class="border-b border-primary/25 pt-14">
                        <h2 class="font-semibold text-2xl lg:text-3xl pb-5">Bukti terdaftar di Kementerian Agama </h2>
                    </div>

                    {{-- Upload File --}}
                    <div>
                        <x-label for="uploadDokumen">
                            Upload Dokumen <span class="text-xs">( .PDF )</span>
                            <p class="text-xs text-red-600">Max. 2Mb !</p>
                        </x-label>
                        <div class="flex items-center justify-center md:justify-start" x-data="{ files: null, input: ''  }">
            
                            {{-- input --}}
                            <label for="dokumenKeagamaan" class="text-sm w-7/12 rounded-md shadow-sm border-gray-300 h-[38px] bg-gray-100 text-black  focus:outline-none focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50 px-2 py-2 border mb-1 overflow-x-hidden">
                                <input x-model="input" name="dokumenKeagamaan" id="dokumenKeagamaan" type="file" class="sr-only" x-on:change="files = Object.values($event.target.files)"{{ (isset($administrasi->dokumenKeagamaan)) ? '' : 'required' }}>
                                <span class="" x-text="files ? files.map(file => file.name).join(', ') : '{{ (isset($administrasi->dokumenKeagamaan)) ? 'Sudah Upload...' : '' }}'"></span>
                            </label>
            
                            {{-- navigasi file kosong --}}
                            <template x-if="!files">
                                <div class="w-5/12 max-w-[250px] mx-auto">
                                    <label class="ml-2 mb-1 w-full text-center cursor-pointer inline-block focus:outline-none py-2 text-sm rounded-md shadow-sm text-primary bg-transparent border border-primary btn" for="dokumenKeagamaan">{{ (isset($administrasi->dokumenKeagamaan)) ? 'Ubah' : 'Choose File...' }}</label>
                                </div>
                            </template>
            
                            {{-- navigasi file isi --}}
                            <template x-if="files">
                                <div class="w-5/12 flex">
                                    <label class="ml-2 mb-1 w-6/12 text-center cursor-pointer inline-block focus:outline-none py-2 text-sm rounded-md shadow-sm text-primary bg-transparent border border-primary " for="dokumenKeagamaan">Ubah</label>
                                    <label id="btnReset" class="ml-2 mb-1 w-6/12 text-center cursor-pointer inline-block focus:outline-none py-2 text-sm rounded-md shadow-sm text-red-600 bg-transparent border border-red-600 " x-on:click="input = ''; files= false ">Hapus</label>
                                </div>
                            </template>
                        </div>
                        @error('dokumenKeagamaan')
                        <p class="text-xs text-red-600 font-normal">{{ $message }}</p> 
                        @enderror
                    </div>
            @endif

           
            {{-- Navigasi --}}
            <div class="flex pt-8 pb-4 mt-12 border-t border-primary/25 justify-between gap-x-3">
                
                <x-button onclick="window.location.href='/batal'" type="button" class="border-red-600 text-red-600 max-w-[280px] w-full justify-center">
                    Batal
                </x-button>


                <!-- Button Asli -->
                <x-button type="submit" class=" text-white bg-primary max-w-[280px] w-full justify-center">
                    Selanjutnya
                </x-button>

            </div>
        </form>
    </div>

    <x-slot name="script">

        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js" integrity="sha512-894YE6QWD5I59HgZOGReFYm4dnWc1Qt5NtvYSaNcOP+u1T9qYdvdihz0PPSiiqn/+/3e7Jo4EaG7TubfWGUrMQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
        <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.11.5/js/jquery.dataTables.js"></script>
        <script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
    
    <script>
            $('#tambah').click(function(){  
                var i = $('#jmlAnggota').val(); 
                i++; 
                $('#containerAnggota').append('<div id="box'+i+'">  <input type="text" placeholder="Anggota '+i+'" name="namaAnggota[]" class="w-full mt-3 text-sm rounded-md shadow-sm border-gray-300  bg-gray-100 text-black  focus:outline-none focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50 px-2 py-2 border mb-1"> </div>');

               $('#jmlAnggota').val(i);
            }); 
            $('#kurang').click(function(){  
               
                var i= $('#jmlAnggota').val(); 
                if(i>1){
                $('#box' +i).remove(); 
                i--;  
                }
                $('#jmlAnggota').val(i);
            }); 
    </script>
    </x-slot>


</x-app-layout>
<x-app-layout :title="__('Edit Profile')">
    <div>
        <h2 class="text-3xl lg:text-4xl mb-3">Edit Profile</h2>
        <p class="text-accent sr-only"></p>
    </div>
    <div class="bg-white rounded-lg drop-shadow-3xl p-4 lg:p-8 mt-8 lg:mt-14  text-sm lg:text-base">

        <form action="/edit-profile/{{ $user->id }}" method="post">
            @csrf

            <div>
                <x-label for="namaLengkap" :value="__('Nama Lengkap')" />
                <x-input class="w-full" type="text" name="namaLengkap" value="{{ $user->namaLengkap}}"  />
            </div>

            <div>
                <x-label for="email" :value="__('Email')" />
                <x-input class="w-full" type="email" name="email" value="{{ $user->email}}"  />
            </div>

            <div>
                <x-label for="noHp" :value="__('Nomor Handphone')" />
                <x-input class="w-full" type="number" name="noHp" value="{{ $user->noHp}}"  />
            </div>
      
            <x-button type="submit" class=" text-white bg-primary max-w-[280px] w-full justify-center mt-3">
                Simpan
            </x-button>
        </form>
    </div>



</x-app-layout>
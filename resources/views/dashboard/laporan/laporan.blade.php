<x-app-layout :title="__('Laporan')">
    <div>
        <h2 class="text-3xl lg:text-4xl mb-3">Laporan</h2>
        <p class="text-accent sr-only"></p>
    </div>
            <div class="mt-24 text-xs bg-white rounded-xl drop-shadow-3xl p-4 lg:p-8 lg:text-sm">
                <table id="example" class="display" style="width:100%">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Tanggal permohonan proposal</th>
                            <th>Nama kelompok</th>
                            <th>Ketua kelompok</th>
                            <th>Alamat kelompok</th>
                            <th>Tahun Pendirian</th>
                            <th>Instansi tempat pendaftaran</th>
                            {{-- <th>Rekomendasi budidaya</th> --}}
                            <th>Kategori kolam</th>
                            <th>Sumber air</th>
                            <th>Status lahan</th>
                            <th>Kondisi jalan</th>
                            <th>Akses kendaraan</th>
                            <th>Jenis benih yang dimohon</th>
                            <th>Jumlah usulan benih</th>
                            <th>Jumlah benih yang disetujui</th>
                            <th>Tanggal proposal disetujui</th>
                            <th>Petugas lapangan</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($proposals as $proposal)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td> {{ $proposal->created_at->format('d M Y') }}</td>
                            <td>{{ $proposal->user->kelompokMasyarakat }}</td>
                            <td> {{ $proposal->administrasi->namaKetua }}</td>
                            <td> {{ $proposal->administrasi->alamatLengkap }}</td>
                            <td> {{ $proposal->administrasi->tahunPendirian }}</td>
                            <td> {{ $proposal->administrasi->instansiPendaftar }}</td>
                               {{-- <td>Rekomendasi budidaya</td> --}}
                            <td>{{ App\Http\Controllers\LaporanController::lokasi($proposal->lokasi->kategoriKolam) }}</td>
                            <td> 
                                 @if ($proposal->lokasi->kategoriKolam == '1')
                                {{ $proposal->kolam->tanahSumber }}
                            @elseif ($proposal->lokasi->kategoriKolam == '4')
                                {{ $proposal->biovlog->sumberAirBiovlok }}
                            @else
                                -
                            @endif</td>
                            <td> {{ $proposal->lokasi->statusLahan }}</td>
                            <td> {{ $proposal->lokasi->kondisiJalan }}</td>
                            <td> {{ $proposal->lokasi->aksesKendaraan }}</td>
                            <td>  {{ $proposal->benih->jenisBenih }}</td>
                            <td> {{ $proposal->benih->jumlahBenih }}</td>
                            <td> {{ $proposal->jumlah_benih_disetujui }}</td>
                            <td>{{ $proposal->created_at->format('d M Y') }}</td>
                            <td> {{ $proposal->pegawaiPengurus }}</td>
                        </tr>
                       @endforeach
                    </tbody>
                </table>
            </div>


            <x-slot name="css">

                <link href="https://cdn.datatables.net/1.12.1/css/jquery.dataTables.min.css" rel="stylesheet">
                <link href="https://cdn.datatables.net/buttons/2.2.3/css/buttons.dataTables.min.css
                " rel="stylesheet">
               
            </x-slot>


            <x-slot name="script">

                <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
                <script src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js"></script>
                <script src="https://cdn.datatables.net/buttons/2.2.3/js/dataTables.buttons.min.js"></script>
                <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
                <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
                <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
                <script src="https://cdn.datatables.net/buttons/2.2.3/js/buttons.html5.min.js"></script>
                <script src="https://cdn.datatables.net/buttons/2.2.3/js/buttons.print.min.js"></script>


                <script>
                    $(document).ready(function() {
                        $('#example').DataTable( {
                            "scrollX": true,
                            dom: 'Bfrtip',
                            buttons: [
                               'excel'
                            ]
                        } );
                    } );
                </script>


            </x-slot>

</x-app-layout>








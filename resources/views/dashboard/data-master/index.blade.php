<x-app-layout :title="__('Data Pengajuan Proposal')">
    {{-- Title --}}
   <div>
    <div class="flex space-x-2">
        <h2 class="text-3xl lg:text-4xl mb-3">Data Master </h2>
    </div> 
        <p class="text-accent">Merupakan seluruh data master pada inputan sistem</p>
    </div>


    <div class="bg-white rounded-xl drop-shadow-3xl p-4 lg:p-8 mt-14  text-sm lg:text-base">
        <div class="flex justify-end">
            <a href="/data-master/tambah" >
                <x-button class="bg-blue-600 hover:bg-blue-700 text-white block py-2 px-3 mb-4">
                    <span>
                        <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-5" viewBox="0 0 20 20" fill="currentColor">
                            <path fill-rule="evenodd" d="M10 3a1 1 0 011 1v5h5a1 1 0 110 2h-5v5a1 1 0 11-2 0v-5H4a1 1 0 110-2h5V4a1 1 0 011-1z" clip-rule="evenodd" />
                          </svg>    
                    </span> Tambah
                </x-button></a>
        </div>
        <table id="table_id" class="stripe hover" style="width:100%; padding-top: 1em;  padding-bottom: 1em;">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Combo Box</th>
                    <th>Value</th>
                    <th>Aksi</th>
                  
                </tr>
            </thead>
            <tbody>
                @foreach ($comboBox  as $combobox)
                    <tr class="text-center">
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $combobox->kategori }}</td>
                        <td>{{ $combobox->valueComboBox }}</td>
                        <td>
                            <div class="flex space-x-2 justify-center">
                                <a onclick="return confirm('Hapus Value ComboBox ?')" href="/data-master/delete/{{ $combobox->id }}" >
                                <x-button class="bg-red-500 text-white block py-1 px-1 w-full">
                                    <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-5" viewBox="0 0 20 20" fill="currentColor">
                                        <path fill-rule="evenodd" d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z" clip-rule="evenodd" />
                                    </svg>
                                </x-button></a>
                               
                                <a href="/data-master/edit/{{ $combobox->id }}">
                                    <x-button class="bg-yellow-500 text-white block py-1 px-1 w-full">
                                        <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-5" viewBox="0 0 20 20" fill="currentColor">
                                            <path d="M13.586 3.586a2 2 0 112.828 2.828l-.793.793-2.828-2.828.793-.793zM11.379 5.793L3 14.172V17h2.828l8.38-8.379-2.83-2.828z" />
                                          </svg>
                                    </x-button>
                                </a>
                            </div> 
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>

    </div>

    @if (Session::has('success'))
         {{-- Alert --}}
    <x-slot name="alert">
        <div x-data="{open : true }" x-show="open" 
            x-transition:enter="transition ease-out duration-300 "
            x-transition:enter-start="opacity-0 scale-90"
            x-transition:enter-end="opacity-100 scale-100"
            x-transition:leave="transition ease-in duration-300"
            x-transition:leave-start="opacity-100 scale-100"
            x-transition:leave-end="opacity-0 scale-90"

            class="flex p-4 mb-4 bg-green-100 rounded-lg dark:bg-green-200 shadow-lg" role="alert">
            <svg class="flex-shrink-0 w-5 h-5 text-green-700 dark:text-green-800" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M18 10a8 8 0 11-16 0 8 8 0 0116 0zm-7-4a1 1 0 11-2 0 1 1 0 012 0zM9 9a1 1 0 000 2v3a1 1 0 001 1h1a1 1 0 100-2v-3a1 1 0 00-1-1H9z" clip-rule="evenodd"></path></svg>
            <div class="ml-3 text-sm font-medium text-green-700 dark:text-green-800">
              Data berhasil disimpan
            </div>
            <button @click="open = ! open" x-init="setTimeout(() => open = false, 4000)" type="button" class="ml-auto -mx-1.5 -my-1.5 bg-green-100 text-green-500 rounded-lg focus:ring-2 focus:ring-green-400 p-1.5 hover:bg-green-200 inline-flex h-8 w-8 dark:bg-green-200 dark:text-green-600 dark:hover:bg-green-300" data-dismiss-target="#alert-3" aria-label="Close">
              <span class="sr-only">Close</span>
              <svg class="w-5 h-5" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z" clip-rule="evenodd"></path></svg>
            </button>
          </div>
    </x-slot>
    @endif

    <x-slot name="css">
        <link href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" rel="stylesheet">
        <!--Responsive Extension Datatables CSS-->
        <link href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.dataTables.min.css" rel="stylesheet"> 
    </x-slot>
    
    <x-slot name="script">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js" integrity="sha512-894YE6QWD5I59HgZOGReFYm4dnWc1Qt5NtvYSaNcOP+u1T9qYdvdihz0PPSiiqn/+/3e7Jo4EaG7TubfWGUrMQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
        <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.11.5/js/jquery.dataTables.js"></script>
        <script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
    
        <script>

        $(document).ready(function() {
                
            var table = $('#table_id').DataTable({
                    "scrollX": true,
                    info : false,
                    language: { 
                        search: "Cari : ",
                        searchPlaceholder: "Cari disini ...",
                        lengthMenu: "Menampilkan _MENU_ Data",
                        "paginate": {
                                    "previous": "Kembali",
                                    "next": "Lanjut",
                                    }
                    }
                })
                .columns.adjust()
                .responsive.recalc();
                    
            });                

           
        </script>
    </x-slot>

</x-app-layout>
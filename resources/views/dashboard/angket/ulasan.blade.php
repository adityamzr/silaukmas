<x-app-layout :title="__('Angket Kepuasan')">
    {{-- Title --}}
   <div>
       <h2 class="text-3xl lg:text-4xl mb-3">Angket Kepuasan</h2>
       <p class="text-accent">Kritik Dan Saran Membangun.</p>
   </div>

    <div class="bg-white rounded-xl drop-shadow-3xl p-4 lg:p-8 mt-14  text-sm lg:text-base">

        <div class="border-b border-primary/25">
            <h2 class="font-semibold text-2xl lg:text-3xl pb-5">Angket Kepuasan Pemohon</h2>
        </div>

        <form action="{{ route('ulasan') }}" method="post">
            @csrf
            <div>
                <x-label for="kritik" :value="__('Tingkat Kepuasan')" />
                <div class="rate">
                    <input type="radio" id="star5" name="rate" value="5" />
                    <label for="star5" title="text">5 stars</label>
                    <input type="radio" id="star4" name="rate" value="4" />
                    <label for="star4" title="text">4 stars</label>
                    <input type="radio" id="star3" name="rate" value="3" />
                    <label for="star3" title="text">3 stars</label>
                    <input type="radio" id="star2" name="rate" value="2" />
                    <label for="star2" title="text">2 stars</label>
                    <input type="radio" id="star1" name="rate" value="1" />
                    <label for="star1" title="text">1 star</label>
                  </div>
            </div>

             {{--Kritik --}}
             <div>
                <x-label for="kritik" :value="__('Kritik')" />
                <x-textarea class="w-full" type="text" name="kritik" rows="12">
                    {{ old('kritik') }}
                </x-textarea>
                <input type="hidden" name="noProposal" value="{{ $noProposal }}">
            </div>
             {{--Saran --}}
             <div>
                <x-label for="saran" :value="__('Saran')" />
                <x-textarea class="w-full" type="text" name="saran" rows="12">
                    {{ old('saran') }}
                </x-textarea>
            </div>
           
            {{-- Navigasi --}}
            <div class="flex pt-8 pb-4 mt-12 border-t border-primary/25 justify-between gap-x-3">

                <!-- Button Dummy -->
                <a href="/angket-kepuasan" class="max-w-[280px] w-full">
                    <x-button type="button" class="border-red-600 text-red-600 max-w-[280px] w-full justify-center">
                    Kembali
                </x-button></a>

                <!-- Button Asli -->
                {{-- <x-button type="button" class="border-red-600 text-red-600 max-w-[280px] w-full justify-center">
                    Kembali
                </x-button> --}}

                <x-button type="submit" class=" text-white bg-primary max-w-[280px] w-full justify-center">
                    Simpan
                </x-button>

            </div>

        </form>
    </div>
    <x-slot name="css">
        <link rel="stylesheet" href="/css/rating.css">
    
    </x-slot>

    @if (Session::has('success'))
    {{-- Alert --}}
    <x-slot name="alert">
        <div x-data="{open : true }" x-show="open" 
            x-transition:enter="transition ease-out duration-300"
            x-transition:enter-start="opacity-0 scale-90"
            x-transition:enter-end="opacity-100 scale-100"
            x-transition:leave="transition ease-in duration-300"
            x-transition:leave-start="opacity-100 scale-100"
            x-transition:leave-end="opacity-0 scale-90"

            class="flex p-4 mb-4 bg-green-100 rounded-lg dark:bg-green-200 shadow-lg" role="alert">
            <svg class="flex-shrink-0 w-5 h-5 text-green-700 dark:text-green-800" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M18 10a8 8 0 11-16 0 8 8 0 0116 0zm-7-4a1 1 0 11-2 0 1 1 0 012 0zM9 9a1 1 0 000 2v3a1 1 0 001 1h1a1 1 0 100-2v-3a1 1 0 00-1-1H9z" clip-rule="evenodd"></path></svg>
            <div class="ml-3 text-sm font-medium text-green-700 dark:text-green-800">
              Data Lokasi berhasil disimpan
            </div>
            <button @click="open = ! open" x-init="setTimeout(() => open = false, 4000)" type="button" class="ml-auto -mx-1.5 -my-1.5 bg-green-100 text-green-500 rounded-lg focus:ring-2 focus:ring-green-400 p-1.5 hover:bg-green-200 inline-flex h-8 w-8 dark:bg-green-200 dark:text-green-600 dark:hover:bg-green-300" data-dismiss-target="#alert-3" aria-label="Close">
              <span class="sr-only">Close</span>
              <svg class="w-5 h-5" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z" clip-rule="evenodd"></path></svg>
            </button>
          </div>
    </x-slot>
    @endif


</x-app-layout>
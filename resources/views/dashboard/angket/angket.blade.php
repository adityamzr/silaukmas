<x-app-layout :title="__('Angket Kepuasan')">
    {{-- Title --}}
   <div>
       <h2 class="text-3xl lg:text-4xl mb-3">Angket Kepuasan</h2>
       <p class="text-accent">Kritik dan Saran Untuk Membangun !</p>
   </div>


   <div class="bg-white rounded-xl drop-shadow-3xl p-4 lg:p-8 mt-14  text-sm lg:text-base">

    <table id="table_id" class="stripe hover" style="width:100%; padding-top: 1em;  padding-bottom: 1em;">
        <thead>
            <tr>
                <th>No Proposal</th>
                <th>Kategory</th>
                <th>Status</th>
              
            </tr>
        </thead>
        <tbody>
            @foreach ($angkets as $angket)
            <tr class="text-center">
                <td>{{ $angket->noProposal }}</td>
                <td>{{ $angket->user->kelompokMasyarakat }}</td>
                <td>
                    @if ($angket->validasiPemohonan == 4)
                    <span class="bg-green-100 text-green-800 whitespace-nowrap text-xs font-semibold px-2.5 py-0.5 rounded inline-flex">Selesai</span>
                    @elseif ($angket->validasiPemohonan == 2)
                        <a href="/angket-kepuasan/ulasan/{{ $angket->noProposal }}">
                            <x-button class="bg-primary text-sm text-white px-2 py-1  mx-auto">
                                Ulas
                            </x-button>
                        </a>
                    @endif
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>

</div>
{{-- Alert --}}
@if (Session::has('success'))
<x-slot name="alert">
    <div x-data="{open : true }" x-show="open"
        x-transition:enter="transition ease-out duration-300"
        x-transition:enter-start="opacity-0 scale-90"
        x-transition:enter-end="opacity-100 scale-100"
        x-transition:leave="transition ease-in duration-300"
        x-transition:leave-start="opacity-100 scale-100"
        x-transition:leave-end="opacity-0 scale-90"

        class="flex p-4 mb-4 bg-green-100 rounded-lg dark:bg-green-200 shadow-lg" role="alert">
        <svg class="flex-shrink-0 w-5 h-5 text-green-700 dark:text-green-800" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M18 10a8 8 0 11-16 0 8 8 0 0116 0zm-7-4a1 1 0 11-2 0 1 1 0 012 0zM9 9a1 1 0 000 2v3a1 1 0 001 1h1a1 1 0 100-2v-3a1 1 0 00-1-1H9z" clip-rule="evenodd"></path></svg>
        <div class="ml-3 text-sm font-medium text-green-700 dark:text-green-800">
          Data Administrasi berhasil disimpan
        </div>
        <button @click="open = ! open" x-init="setTimeout(() => open = false, 4000)" type="button" class="ml-auto -mx-1.5 -my-1.5 bg-green-100 text-green-500 rounded-lg focus:ring-2 focus:ring-green-400 p-1.5 hover:bg-green-200 inline-flex h-8 w-8 dark:bg-green-200 dark:text-green-600 dark:hover:bg-green-300" data-dismiss-target="#alert-3" aria-label="Close">
          <span class="sr-only">Close</span>
          <svg class="w-5 h-5" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z" clip-rule="evenodd"></path></svg>
        </button>
      </div>
</x-slot>
@endif
<x-slot name="css">
    <link href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" rel="stylesheet">
    <!--Responsive Extension Datatables CSS-->
    <link href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.dataTables.min.css" rel="stylesheet"> 
</x-slot>
<x-slot name="script">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js" integrity="sha512-894YE6QWD5I59HgZOGReFYm4dnWc1Qt5NtvYSaNcOP+u1T9qYdvdihz0PPSiiqn/+/3e7Jo4EaG7TubfWGUrMQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.11.5/js/jquery.dataTables.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>

    <script>

    $(document).ready(function() {
            
        var table = $('#table_id').DataTable({
                info : false,
                ordering: false,
                "scrollX": true,
                language: { 
                    search: "Cari : ",
                    searchPlaceholder: "Cari disini ...",
                    lengthMenu: "Menampilkan _MENU_ Data",
                    "paginate": {
                                "previous": "Kembali",
                                "next": "Lanjut",
                                }
                }
            })
            .columns.adjust()
            .responsive.recalc();
                
        });                

       
    </script>
</x-slot>


    
    


</x-app-layout>
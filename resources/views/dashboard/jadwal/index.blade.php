<x-app-layout :title="__('Jadwal Pengajuan Proposal')">
    {{-- Title --}}
   <div>
       <h2 class="text-3xl lg:text-4xl mb-3">Jadwal Pengajuan Proposal</h2>
       <p class="text-accent">Menentukan Jadwal Untuk Pengajuan Proposal </p>
   </div>

    <div class="bg-white rounded-lg border-2 border-gray-400/30 p-4 lg:p-8 mt-14  text-sm lg:text-base">

        <div class="border-b border-primary/25">
            <h2 class="font-semibold text-2xl lg:text-3xl pb-5">Data Master</h2>
        </div>

        <form action="{{ route('jadwal') }}" method="post">
            @csrf
            <div x-data="{ select: {{ old('status', $jadwal->status) }} }">
            <div >
                <x-label for="kategori" >
                Status Pengajuan Proposal
                </x-label>
                <x-select x-model="select" class="w-full" name="status" :value="old('status')" required>
                    <option value="1">Buka</option>
                    <option value="0">Tutup</option>
                </x-select>
            </div>
            <x-label>Pengajuan Proposal</x-label>
            <div class="flex lg:flex-row flex-col gap-x-3">

                <div class="w-full lg:w-6/12">
                   <x-label for="jadwalBuka" class="sr-only">
                   Jadwal Buka
                   </x-label>
                   <x-input x-bind:disabled="(select == 0) ? true : false" x-bind:required="(select == 0) ? false : true" min="{{ \Carbon\Carbon::now()->toDateString() }}" class="w-full" type="date" name="jadwalBuka" :value="old('jadwalBuka', $jadwal->jadwalBuka)"  />
               </div>
               <p class="mt-auto mb-3 w-auto text-center">Sampai</p>
                <div class="w-full lg:w-6/12">
                   <x-label for="jadwalTutup" class="sr-only">
                   Jadwal Tutup
                   </x-label>
                   <x-input x-bind:disabled="(select == 0) ? true : false"  x-bind:required="(select == 0) ? false : true" min="{{ \Carbon\Carbon::now()->toDateString() }}" class="w-full" type="date" name="jadwalTutup" :value="old('jadwalTutup', $jadwal->jadwalTutup)"  />
               </div>
            </div>


            <x-label>Verifikasi</x-label>
            <div class="flex lg:flex-row flex-col gap-x-3">
                    <div class=" w-full lg:w-6/12">
                       <x-label for="mulaiVerifikasi" class="sr-only" >
                       Mulai verifikasi 
                       </x-label>
                       <x-input x-bind:disabled="(select == 0) ? true : false"  x-bind:required="(select == 0) ? false : true" min="{{ \Carbon\Carbon::now()->toDateString() }}" class="w-full" type="date" name="jadwalBukaVerifikasi" :value="old('jadwalBukaVerifikasi', $jadwal->jadwalBukaVerifikasi)"  />
                   </div>

                   <p class="mt-auto mb-3 w-auto text-center">Sampai</p>
       
                    <div class=" w-full lg:w-6/12">
                       <x-label for="akhirVerifikasi"  class="sr-only">
                       Akhir verifikasi 
                       </x-label>
                       <x-input x-bind:disabled="(select == 0) ? true : false"  x-bind:required="(select == 0) ? false : true" min="{{ \Carbon\Carbon::now()->toDateString() }}" class="w-full" type="date" name="jadwalTutupVerifikasi" :value="old('jadwalTutupVerifikasi', $jadwal->jadwalTutupVerifikasi)"  />
                   </div>
            </div>

            <x-label>Pengambilan Benih</x-label>
            <div class="flex lg:flex-row flex-col gap-x-3">
                    <div class=" w-full lg:w-6/12">
                       <x-label for="mulaiPengambilan" class="sr-only" >
                       Mulai verifikasi 
                       </x-label>
                       <x-input x-bind:disabled="(select == 0) ? true : false"  x-bind:required="(select == 0) ? false : true" min="{{ \Carbon\Carbon::now()->toDateString() }}" class="w-full" type="date" name="jadwalBukaPengambilan" :value="old('jadwalBukaPengambilan', $jadwal->jadwalBukaPengambilan)"  />
                   </div>

                   <p class="mt-auto mb-3 w-auto text-center">Sampai</p>
       
                    <div class=" w-full lg:w-6/12">
                       <x-label for="akhirPengambilan"  class="sr-only">
                       Akhir verifikasi 
                       </x-label>
                       <x-input x-bind:disabled="(select == 0) ? true : false"  x-bind:required="(select == 0) ? false : true" min="{{ \Carbon\Carbon::now()->toDateString() }}" class="w-full" type="date" name="jadwalTutupPengambilan" :value="old('jadwalTutupPengambilan', $jadwal->jadwalTutupPengambilan)"  />
                   </div>
            </div>
        </div>
            {{-- Navigasi --}}
            <div class="flex pt-8 pb-4 mt-12 border-t border-primary/25 justify-between gap-x-3">

                <!-- Button Dummy -->
                <a href="/dashboard" class="max-w-[280px] w-full">
                    <x-button type="button" class="border-red-600 text-red-600 max-w-[280px] w-full justify-center">
                    Batal
                </x-button></a>

                <!-- Button Asli -->
                {{-- <x-button type="button" class="border-red-600 text-red-600 max-w-[280px] w-full justify-center">
                    Kembali
                </x-button> --}}

                 <!-- Button Dummy -->
                {{-- <a href="/masyarakat-pembudidaya-ikan/data-lokasi" class="max-w-[280px] w-full"><x-button type="button" class=" text-white bg-primary max-w-[280px] w-full justify-center">
                    Selanjutnya
                </x-button></a> --}}

                <!-- Button Asli -->
                <x-button type="submit" class=" text-white bg-primary max-w-[280px] w-full justify-center">
                    Ubah
                </x-button>

            </div>

        </form>
    </div>

    {{-- Alert --}}
    @if (Session::has('success'))
    <x-slot name="alert">
        <div x-data="{open : true }" x-show="open"
            x-transition:enter="transition ease-out duration-300"
            x-transition:enter-start="opacity-0 scale-90"
            x-transition:enter-end="opacity-100 scale-100"
            x-transition:leave="transition ease-in duration-300"
            x-transition:leave-start="opacity-100 scale-100"
            x-transition:leave-end="opacity-0 scale-90"

            class="flex p-4 mb-4 bg-green-100 rounded-lg dark:bg-green-200 shadow-lg" role="alert">
            <svg class="flex-shrink-0 w-5 h-5 text-green-700 dark:text-green-800" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M18 10a8 8 0 11-16 0 8 8 0 0116 0zm-7-4a1 1 0 11-2 0 1 1 0 012 0zM9 9a1 1 0 000 2v3a1 1 0 001 1h1a1 1 0 100-2v-3a1 1 0 00-1-1H9z" clip-rule="evenodd"></path></svg>
            <div class="ml-3 text-sm font-medium text-green-700 dark:text-green-800">
              Jadwal Berhasil Diubah.
            </div>
            <button @click="open = ! open" x-init="setTimeout(() => open = false, 4000)" type="button" class="ml-auto -mx-1.5 -my-1.5 bg-green-100 text-green-500 rounded-lg focus:ring-2 focus:ring-green-400 p-1.5 hover:bg-green-200 inline-flex h-8 w-8 dark:bg-green-200 dark:text-green-600 dark:hover:bg-green-300" data-dismiss-target="#alert-3" aria-label="Close">
              <span class="sr-only">Close</span>
              <svg class="w-5 h-5" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z" clip-rule="evenodd"></path></svg>
            </button>
          </div>
    </x-slot>
    @endif
    


</x-app-layout>
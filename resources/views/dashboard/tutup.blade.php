<x-app-layout :title="__('Pengajuan Proposal')">
    <div class="h-[80vh] flex justify-center items-center">
        <div class="max-h-[800px] max-w-xl w-full h-min">
            <img class="h-full w-full object-contain" src="/assets/images/closed.svg" alt="">
            <h2 class="text-center text-gray-500 font-semibold text-2xl">{{ $message }}</h2>
        </div>
    </div>
</x-app-layout>
<x-app-layout :title="__('Pengambilan benih')">
    {{-- Title --}}
   <div>
        <h2 class="text-3xl lg:text-4xl mb-3">Pengambilan benih</h2>
        <p class="text-accent">Informasi pengambilan benih</p>
    </div>

    <div class="bg-white rounded-xl drop-shadow-3xl p-4 lg:p-8 mt-14  text-sm lg:text-base">

        <table id="table_id" class="stripe hover" style="width:100%; padding-top: 1em;  padding-bottom: 1em;">
            <thead>
                <tr>
                    <th>No</th>
                    <th>No Proposal</th>
                    <th>Kategory</th>
                    <th>Jenis Benih</th>
                    <th>Status</th>
                    <th>Detail</th>
                  
                </tr>
            </thead>
            <tbody>
                @foreach ($pemohons as $pemohon)
                <tr class="text-center">
                    <td>{{ $loop->iteration }}</td>
                    <td>{{ $pemohon->noProposal }}</td>
                    <td>{{ $pemohon->user->kelompokMasyarakat }}</td>
                    <td>{{ $pemohon->benih->jenisBenih }}</td>
                    <td>
                        @if ($pemohon->validasiPemohonan == 1)
                        <span class="bg-yellow-100 text-yellow-800 whitespace-nowrap text-xs font-semibold px-2.5 py-0.5 rounded inline-flex">Dalam Proses</span>
                        @elseif ($pemohon->validasiPemohonan == 2 || $pemohon->validasiPemohonan == 4)
                            <a href="/downloadSurat/{{ $pemohon->noProposal }}">
                                <x-button class="bg-primary text-sm text-white px-2 py-1  mx-auto">
                                    Download Surat
                                    </x-button>
                                 </a>
                        @elseif ($pemohon->validasiPemohonan == 3)
                        <span class="bg-red-100 text-red-800 text-xs font-semibold px-2.5 py-0.5 rounded">Ditolak</span>
                        @endif
                    </td>
                    <td>
                        <x-button class="open-modal bg-primary text-white" 
                        data-noproposal="{{ $pemohon->noProposal }}"
                        data-kategori="{{ $pemohon->user->kelompokMasyarakat }}"
                        data-jenisbenih="{{ $pemohon->benih->jenisBenih }}"
                        data-jmlbenih="{{ $pemohon->benih->jumlahBenih  }}"
                        data-tglpengambilan="{{ $pemohon->tglPengambilan  }}"
                        data-jampengambilan="{{ $pemohon->jamPengambilan  }}"
                        data-lokasipengambilan="{{ $pemohon->lokasiPengambilan  }}"
                        data-pegawaipengurus="{{ $pemohon->pegawaiPengurus  }}"
                        data-ket="{{ $pemohon->keterangan  }}"
                        >
                            Detail    
                        </x-button>    
                    </td>
                </tr>

                   

                @endforeach
            </tbody>
        </table>
{{-- Modal --}}
<x-slot name="modal">
    <div  class="relative z-50 modal">
        <div class="fixed inset-0 bg-gray-500/50 filter backdrop-blur-lg bg-opacity-75"></div>
        <div class="fixed z-10 inset-0 overflow-y-auto">
            <div class="flex items-end sm:items-center justify-center w-full min-h-full p-4 text-center sm:p-0">

                <div class="relative bg-white rounded-lg text-left overflow-hidden shadow-xl transform sm:my-8 sm:max-w-lg sm:w-full w-full"
                   >
                    <div class="bg-white px-4 pt-5 pb-4 sm:p-6 sm:pb-4">
                        <div class="sm:flex items-center">
                            <div
                                class="mx-auto flex-shrink-0 flex items-center justify-center h-12 w-12 rounded-full bg-blue-100 sm:mx-0 sm:h-10 sm:w-10">
                                <!-- Heroicon name: outline/exclamation -->
                                <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6 text-blue-600" fill="none" viewBox="0 0 24 24" stroke="currentColor" stroke-width="2">
                                    <path stroke-linecap="round" stroke-linejoin="round" d="M19 20H5a2 2 0 01-2-2V6a2 2 0 012-2h10a2 2 0 012 2v1m2 13a2 2 0 01-2-2V7m2 13a2 2 0 002-2V9a2 2 0 00-2-2h-2m-4-3H9M7 16h6M7 8h6v4H7V8z" />
                                  </svg>
                            </div>
                            <div class="mt-3 text-center sm:mt-0 sm:ml-4 sm:text-left w-full lg:flex lg:justify-between">
                                <h3 class="text-lg leading-6 font-medium text-gray-900" id="modal-title">Detail Proposal</h3>
                                <div class="font-semibold text-base">
                                   <p id="tnoproposal"> </p>
                                </div>

                            </div>
                        </div>
                    </div>

                    <div class="mx-6 text-gray-900 text-sm md:text-base mb-5">
                        <ul class="space-y-2">
                            <li class="flex w-full">
                                <span class="w-6/12 lg:w-5/12">Kategory </span>  <span class="w-6/12 lg:w-7/12" id="tkategori">: </span>
                            </li>
                            <li class="flex w-full">
                                <span class="w-6/12 lg:w-5/12">Jenis Benih </span>  <span class="w-6/12 lg:w-7/12" id="tjenisBenih">: </span>
                            </li>
                            <li class="flex w-full">
                                <span class="w-6/12 lg:w-5/12">Jumlah benih </span>  <span class="w-6/12 lg:w-7/12" id="tjmlBenih">: </span>
                            </li>
                            <li class="flex w-full">
                                <span class="w-6/12 lg:w-5/12">Tanggal Pengambilan </span>  <span class="w-6/12 lg:w-7/12" id="ttglPengambilan">: </span>
                            </li>
                            <li class="flex w-full">
                                <span class="w-6/12 lg:w-5/12">Jam Pengambilan </span>  <span class="w-6/12 lg:w-7/12" id="tjamPengambilan">: </span>
                            </li>
                            <li class="flex w-full">
                                <span class="w-6/12 lg:w-5/12">Lokasi Pengambilan </span>  <span class="w-6/12 lg:w-7/12" id="tlokasiPengambilan">: </span>
                            </li>
                            <li class="flex w-full">
                                <span class="w-6/12 lg:w-5/12">Karyawan Pengurus </span>  <span class="w-6/12 lg:w-7/12" id="tkaryawanPengurus">: </span>
                            </li>
                            <li class="flex w-full">
                                <span class="w-6/12 lg:w-5/12">Keterangan </span>  <span class="w-6/12 lg:w-7/12" id="tketerangan">: </span>
                            </li>
                        </ul>
                    </div>
    
                        <div class="bg-gray-50 px-4 py-3 sm:px-6 sm:flex sm:flex-row-reverse">
                            <button 
                                class="mt-3 w-full inline-flex justify-center rounded-md border border-gray-300 shadow-sm px-4 py-2 bg-white text-base font-medium text-gray-700 hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500 sm:mt-0 sm:ml-3 sm:w-auto sm:text-sm close-modal">Close</button>
                        </div>
                   
                </div>
            </div>
        </div>
    </div>
    </x-slot>
    </div>
    <x-slot name="css">
        <link href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" rel="stylesheet">
        <!--Responsive Extension Datatables CSS-->
        <link href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.dataTables.min.css" rel="stylesheet"> 
    </x-slot>
    <x-slot name="script">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js" integrity="sha512-894YE6QWD5I59HgZOGReFYm4dnWc1Qt5NtvYSaNcOP+u1T9qYdvdihz0PPSiiqn/+/3e7Jo4EaG7TubfWGUrMQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
        <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.11.5/js/jquery.dataTables.js"></script>
        <script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
    
        <script>

            $(".open-modal").click(function(){  
                    $(".modal").addClass("is-visible");
                    $("#tnoproposal").text('#'+$(this).data('noproposal'));
                    $("#tkategori").text(': '+$(this).data('kategori'));
                    $("#tjenisBenih").text(': '+$(this).data('jenisbenih'));
                    $("#tjmlBenih").text(': '+$(this).data('jmlbenih'));
                    $("#ttglPengambilan").text(': '+$(this).data('tglpengambilan'));
                    $("#tjamPengambilan").text(': '+$(this).data('jampengambilan'));
                    $("#tlokasiPengambilan").text(': '+$(this).data('lokasipengambilan'));
                    $("#tkaryawanPengurus").text(': '+$(this).data('pegawaipengurus'));
                    $("#tketerangan").text(': '+$(this).data('ket'));

                });

                $(".close-modal").click(function(){
                    $(".modal").removeClass("is-visible");
                });

        $(document).ready(function() {
                
            var table = $('#table_id').DataTable({
                    info : false,
                    ordering: false,
                    "scrollX": true,
                    language: { 
                        search: "Cari : ",
                        searchPlaceholder: "Cari disini ...",
                        lengthMenu: "Menampilkan _MENU_ Data",
                        "paginate": {
                                    "previous": "Kembali",
                                    "next": "Lanjut",
                                    }
                    }
                })
                .columns.adjust()
                .responsive.recalc();
                    
            });                

           
        </script>
    </x-slot>

</x-app-layout>
<x-app-layout :title="__('Upload Surat Keputusan')">
    {{-- Title --}}
   <div>
       <h2 class="text-3xl lg:text-4xl mb-3">Upload surat penetapan calon penerima benih</h2>
       <p class="text-accent">Merubah Surat Surat Keputusan dan Lampiran Daftar Penerima. </p>
   </div>

    <div class="bg-white rounded-lg border-2 border-gray-400/30 p-4 lg:p-8 mt-14  text-sm lg:text-base">

        <div class="border-b border-primary/25">
            <h2 class="font-semibold text-2xl lg:text-3xl pb-5">Surat Keputusan dan Lampiran Daftar Penerima</h2>
        </div>
            <div>
           {{-- Upload KTP --}}
           <div>
            <x-label for="fotoKtp" >
                Preview Surat
            </x-label>
            <div class="flex pt-2 pb-0 items-center">
                <a href="/view-surat" target="_blank"  class="btn mb-1 text-center cursor-pointer inline-block focus:outline-none py-2 px-5 text-xs rounded-md shadow-sm text-red-600 bg-transparent border border-red-600 font-semibold">Lihat Surat Kebijakan</a>
            </div>
        </div>
        <form action="{{ route('surat') }}" method="post" enctype="multipart/form-data">
            @csrf

            <div>
                <x-label for="suratKebijakan">
                    Upload Surat Baru <span class="text-xs">( .pdf )</span>
                    <p class="text-xs text-red-600">Max. 2Mb !</p>
                </x-label>
                <div class="flex items-center justify-center md:justify-start" x-data="{ files: null, input: ''  }">
    
                    {{-- input --}}
                    <label for="suratKebijakan" class="text-sm w-7/12 rounded-md shadow-sm border-gray-300 h-[38px] bg-gray-100 text-black  focus:outline-none focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50 px-2 py-2 border mb-1 overflow-x-hidden">
                        <input x-model="input" name="suratKebijakan" id="suratKebijakan" type="file" class="sr-only" x-on:change="files = Object.values($event.target.files)">
                        <span class="" x-text="files ? files.map(file => file.name).join(', ') : ' '"></span>
                    </label>
    
                    {{-- navigasi file kosong --}}
                    <template x-if="!files">
                        <div class="w-5/12 max-w-[250px] mx-auto">
                            <label class="ml-2 mb-1 w-full text-center cursor-pointer inline-block focus:outline-none py-2 text-sm rounded-md shadow-sm text-primary bg-transparent border border-primary btn" for="suratKebijakan"> Choose File</label>
                        </div>
                    </template>
    
                    {{-- navigasi file isi --}}
                    <template x-if="files">
                        <div class="w-5/12 flex">
                            <label class="ml-2 mb-1 w-6/12 text-center cursor-pointer inline-block focus:outline-none py-2 text-sm rounded-md shadow-sm text-primary bg-transparent border border-primary " for="suratKebijakan">Ubah</label>
                            <label id="btnReset" class="ml-2 mb-1 w-6/12 text-center cursor-pointer inline-block focus:outline-none py-2 text-sm rounded-md shadow-sm text-red-600 bg-transparent border border-red-600 " x-on:click="input = ''; files= false ">Hapus</label>
                        </div>
                    </template>
                </div>
                @error('suratKebijakan')
                <p class="text-xs text-red-600 font-normal">{{ $message }}</p>  
                @enderror
            </div>
        </div>
            {{-- Navigasi --}}
            <div class="flex pt-8 pb-4 mt-12 border-t border-primary/25 justify-between gap-x-3">

                <!-- Button Dummy -->
                <a href="/dashboard" class="max-w-[280px] w-full">
                    <x-button type="button" class="border-red-600 text-red-600 max-w-[280px] w-full justify-center">
                    Batal
                </x-button></a>

                <!-- Button Asli -->
                {{-- <x-button type="button" class="border-red-600 text-red-600 max-w-[280px] w-full justify-center">
                    Kembali
                </x-button> --}}

                 <!-- Button Dummy -->
                {{-- <a href="/masyarakat-pembudidaya-ikan/data-lokasi" class="max-w-[280px] w-full"><x-button type="button" class=" text-white bg-primary max-w-[280px] w-full justify-center">
                    Selanjutnya
                </x-button></a> --}}

                <!-- Button Asli -->
                <x-button type="submit" class=" text-white bg-primary max-w-[280px] w-full justify-center">
                    Ubah
                </x-button>

            </div>

        </form>
    </div>

    {{-- Alert --}}
    @if (Session::has('success'))
    <x-slot name="alert">
        <div x-data="{open : true }" x-show="open"
            x-transition:enter="transition ease-out duration-300"
            x-transition:enter-start="opacity-0 scale-90"
            x-transition:enter-end="opacity-100 scale-100"
            x-transition:leave="transition ease-in duration-300"
            x-transition:leave-start="opacity-100 scale-100"
            x-transition:leave-end="opacity-0 scale-90"

            class="flex p-4 mb-4 bg-green-100 rounded-lg dark:bg-green-200 shadow-lg" role="alert">
            <svg class="flex-shrink-0 w-5 h-5 text-green-700 dark:text-green-800" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M18 10a8 8 0 11-16 0 8 8 0 0116 0zm-7-4a1 1 0 11-2 0 1 1 0 012 0zM9 9a1 1 0 000 2v3a1 1 0 001 1h1a1 1 0 100-2v-3a1 1 0 00-1-1H9z" clip-rule="evenodd"></path></svg>
            <div class="ml-3 text-sm font-medium text-green-700 dark:text-green-800">
              Jadwal Berhasil Diubah.
            </div>
            <button @click="open = ! open" x-init="setTimeout(() => open = false, 4000)" type="button" class="ml-auto -mx-1.5 -my-1.5 bg-green-100 text-green-500 rounded-lg focus:ring-2 focus:ring-green-400 p-1.5 hover:bg-green-200 inline-flex h-8 w-8 dark:bg-green-200 dark:text-green-600 dark:hover:bg-green-300" data-dismiss-target="#alert-3" aria-label="Close">
              <span class="sr-only">Close</span>
              <svg class="w-5 h-5" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z" clip-rule="evenodd"></path></svg>
            </button>
          </div>
    </x-slot>
    @endif
    


</x-app-layout>
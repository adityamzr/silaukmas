@props(['value'])

<label {{ $attributes->merge(['class' => 'block font-medium text-accent text-base mt-3 mb-2 w-fit']) }}>
    {{ $value ?? $slot }}
</label>
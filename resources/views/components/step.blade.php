@props(['active', 'name'])
<li class="relative mb-6 sm:mb-0">
    <div class="flex items-center">
        <div class=" flex z-10 justify-center {{ $active }} items-center w-14 h-14 rounded-full shrink-0">
            {{ $slot }}
        </div>
        <div class="hidden sm:flex w-full {{ $active }} h-0.5 "></div>
    </div>
    <div class="mt-3 sm:pr-8">
        <h3 class="text-lg text-black font-semibold">{{ $name }}</h3>
    </div>
</li>
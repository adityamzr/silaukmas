@props(['name'])

<!-- dummy -->
{{-- <textarea id="{{ $name ?? '' }}" name="{{ $name ?? '' }}"  {!! $attributes->merge(['class' => 'text-sm rounded-md shadow-sm border-gray-300  bg-gray-100 text-black  focus:outline-none focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50 px-2 py-2 border mb-1']) !!}> {{  $slot }} </textarea>
<p class="text-xs text-red-600"> Tidak boleh Kosong !</p> --}}

<!-- Asli -->
@error($name)

    <textarea id="{{ $name ?? '' }}" name="{{ $name ?? '' }}"  {!! $attributes->merge(['class' => 'in-validate text-sm rounded-md shadow-sm border-gray-300  bg-gray-100 text-black  focus:outline-none focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50 px-2 py-2 border mb-1']) !!}> {{  $slot }} </textarea>

@else
    
    <textarea id="{{ $name ?? '' }}" name="{{ $name ?? '' }}"  {!! $attributes->merge(['class' => 'text-sm rounded-md shadow-sm border-gray-300  bg-gray-100 text-black  focus:outline-none focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50 px-2 py-2 border mb-1']) !!}> {{  $slot }} </textarea>

@enderror

@error($name)
<p class="text-xs text-red-600">{{$message}}</p>
@enderror
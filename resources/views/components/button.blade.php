<button {{ $attributes->merge(['type' => 'submit', 'class' => 'rounded-md py-3 px-3  inline-flex border border-transparent']) }}>
    {{ $slot }}
</button>
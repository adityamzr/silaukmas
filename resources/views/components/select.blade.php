
@props(['name'])

<!-- dummy -->
{{-- <input id="{{ $name ?? '' }}" name="{{ $name ?? '' }}"  {!! $attributes->merge(['class' => 'text-sm rounded-md shadow-sm border-gray-300  bg-gray-100 text-black  focus:outline-none focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50 px-2 py-2 border mb-1']) !!}>
<p class="text-xs text-red-600"> Tidak boleh Kosong !</p> --}}


<!-- Asli -->
@error($name)
    
<select id="{{ $name ?? '' }}" name="{{ $name ?? '' }}"  {!! $attributes->merge(['class' => 'in-validate text-sm rounded-md shadow-sm border-gray-300  bg-gray-100 text-black  focus:outline-none focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50 px-2 py-2 border mb-1']) !!}>
    {{ $slot }}
</select>

@error($name)
<p class="text-xs text-red-600"> {{ $message }}</p>
@enderror

@else  

<select id="{{ $name ?? '' }}" name="{{ $name ?? '' }}"  {!! $attributes->merge(['class' => ' text-sm rounded-md shadow-sm border-gray-300  bg-gray-100 text-black  focus:outline-none focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50 px-2 py-2 border mb-1']) !!}>
    {{ $slot }}
</select>
@enderror

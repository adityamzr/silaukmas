<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sungais', function (Blueprint $table) {
            $table->id();
            $table->string('noProposal');
            $table->foreign('noProposal')->references('noProposal')->on('pemohons')->onDelete('cascade');
            $table->string('namaSungai');
            $table->string('lebarSungai');
            $table->string('kedalamanSungai');
            $table->string('panjangSungai');
            $table->timestamps();
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sungais');
    }
};

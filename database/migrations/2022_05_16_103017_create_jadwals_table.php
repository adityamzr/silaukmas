<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jadwals', function (Blueprint $table) {
            $table->id();
            $table->string('status');
            $table->date('jadwalBuka')->nullable();
            $table->date('jadwalTutup')->nullable();
            $table->date('jadwalBukaVerifikasi')->nullable();
            $table->date('jadwalTutupVerifikasi')->nullable();
            $table->date('jadwalBukaPengambilan')->nullable();
            $table->date('jadwalTutupPengambilan')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jadwals');
    }
};

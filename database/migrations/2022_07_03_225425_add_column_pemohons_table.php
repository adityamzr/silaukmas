<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pemohons', function (Blueprint $table) {
            $table->string('jenis_benih_ikan')->after('pegawaiPengurus')->nullable();
            $table->string('jumlah_benih_disetujui')->after('jenis_benih_ikan')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pemohons', function (Blueprint $table) {
            $table->dropColumn('jenis_benih_ikan');
            $table->dropColumn('jumlah_benih_disetujui');
        });
    }
};

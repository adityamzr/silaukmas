<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('angket_kepuasans', function (Blueprint $table) {
            $table->string('noProposal');
            $table->foreign('noProposal')->references('noProposal')->on('pemohons')->onDelete('cascade');
            $table->string('rate');
            $table->text('kritik')->nullable();
            $table->text('saran')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('angket_kepuasans');
    }
};

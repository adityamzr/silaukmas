<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('administrasis', function (Blueprint $table) {
            $table->id();
            $table->string('noProposal');
            $table->foreign('noProposal')->references('noProposal')->on('pemohons')->onDelete('cascade');
            $table->string('namaKetua');
            $table->text('alamatLengkap');
            $table->integer('tahunPendirian');
            $table->string('nikKetua');
            $table->string('noTelp');
            $table->string('email');
            $table->string('instansiPendaftar');
            $table->string('tglSurat');
            $table->string('noSurat');
            $table->string('ttdPejabat');
            $table->string('fotoKtp');
            $table->string('fileKesanggupan')->nullable();
            $table->string('fileTidakMenerimaBantuan')->nullable();
            $table->string('fileBukti');
            $table->string('dokumenHukumAdat')->nullable();
            $table->string('dokumenSwadaya')->nullable();
            $table->string('dokumenPendidikan')->nullable();
            $table->string('dokumenKeagamaan')->nullable();
            $table->timestamps();
        });
        
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('administrasis');
    }
};

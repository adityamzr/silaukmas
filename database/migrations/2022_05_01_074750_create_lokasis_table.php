<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lokasis', function (Blueprint $table) {
            $table->id();
            $table->string('noProposal');
            $table->foreign('noProposal')->references('noProposal')->on('pemohons')->onDelete('cascade');
            $table->string('kategoriKolam');
            $table->string('statusLahan');
            $table->string('jarakLahan');
            $table->string('kondisiJalan');
            $table->string('aksesKendaraan');
            $table->string('buktiLahan');
            $table->string('buktiKondisiLahan');
            $table->timestamps();
        });

      

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lokasis');
    }
};

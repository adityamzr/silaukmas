<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pemohons', function (Blueprint $table) {
            $table->foreignId('userId');
            $table->string('noProposal')->index();
            $table->boolean('syaratAdministrasi')->default(0);
            $table->boolean('syaratTeknis')->default(0);
            $table->boolean('syaratLokasi')->default(0);
            $table->boolean('syaratPemohonan')->default(0);
            $table->date('tglPemohonan')->nullable();
            $table->string('hariPengambilan')->nullable();
            $table->date('tglPengambilan')->nullable();
            $table->string('jamPengambilan')->nullable();
            $table->string('lokasiPengambilan')->nullable();
            $table->string('pegawaiPengurus')->nullable();
            $table->string('validasiPemohonan')->nullable();
            $table->string('keterangan')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pemohons');
    }
};

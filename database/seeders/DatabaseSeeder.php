<?php

namespace Database\Seeders;

use App\Models\User;
use App\Models\Jadwal;
use App\Models\ComboBox;
use Illuminate\Database\Seeder;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        User::create([
            'namaLengkap' => 'Rivan',
            'kelompokMasyarakat' => '',
            'email' => 'admin@gmail.com',
            'noHp' => '01241231231',
            'password' => bcrypt('12345678'),
            'is_admin' => 1
        ]);
        ComboBox::create([
            'kategori' => 'Jenis Benih',
            'valueComboBox' => 'Lele',
        ]);
        ComboBox::create([
            'kategori' => 'Kondisi Jalan',
            'valueComboBox' => 'Aspal',
        ]);
        ComboBox::create([
            'kategori' => 'Akses Jalan',
            'valueComboBox' => 'Roda Empat',
        ]);
        ComboBox::create([
            'kategori' => 'Bangunan Kolam',
            'valueComboBox' => 'Tembok Permanent',
        ]);
        Jadwal::create([
            'status' => '0'
        ]);
    }
}

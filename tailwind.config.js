module.exports = {
  content: [
    "./resources/**/*.blade.php",
    "./resources/**/*.js",
    "./resources/**/*.vue",
  ],
  theme: {
    extend: {
      fontFamily: {
        'poppins': ['"Poppins"', 'sans-serif'],
      },
      dropShadow: {
        '3xl': '  0px 0px 8px rgba(197, 197, 197, 0.5)',
      },
      colors: {
        "primary": '#1C57AE',
        "secondary": '#7e3af2',
        "accent": '#626161',
        "abu": '#1C1C1C',
        "abu-200": '#141414',
        "hijau": '#31B057',
        "biru": '#3F8DFD',
        "merah": '#FF4242',
        
      },
      backgroundImage: {
        'ornament': "url('/assets/images/ornament.svg')",
      },
      animation: {
        blob: "blob 7s infinite",
      },
      keyframes: {
        blob: {
          "0%": {
            transform: "translate(0px, 0px) scale(1)",
          },
          "33%": {
            transform: "translate(30px, -50px) scale(1.1)",
          },
          "66%": {
            transform: "translate(-20px, 20px) scale(0.9)",
          },
          "100%": {
            transform: "tranlate(0px, 0px) scale(1)",
          },
        },
      },
      
    },
  },
  plugins: [],
}
<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    public function register(Request $request){
        $validateData = $request->validate([
            'namaLengkap' => 'required',
            'kelompokMasyarakat' => 'required',
            'noHp' => 'required|unique:users',
            'email' => 'required|email:dns|unique:users',
            'password' => 'required|min:8|max:32'
        ],[
            'required' => 'Tidak Boleh Kosong !',
            'email' => 'Format Email Tidak Valid !',
            'min' => 'Minimal 8 Karakter !',
            'max' => 'Maksimal 32 Karakter !',
            'unique' => 'Sudah Digunakan !',
        ]);

        $validateData['password'] = Hash::make($request['password']);
        $validateData['is_admin'] = 0;

        User::create($validateData);

        return redirect('/login')->with('success', 'Registrasi Berhasil !');
    }
    public function authenticate(Request $request){
        $validateData = $request->validate([
            'g-recaptcha-response' => 'captcha',
            'email' => 'email:dns|required',
            'password' => 'required'
        ],[
            'captcha' => 'reCaptcha Tidak Valid !',
            'required' => 'Tidak Boleh Kosong !',
            'email' => 'Format Email Tidak Valid !'
        ]);
        $fields['email'] = $validateData['email'];
        $fields['password'] = $validateData['password'];

        if (Auth::attempt($fields)) {
            $request->session()->regenerate();

            if (auth()->user()->is_admin == 1) {
                return redirect('/dashboard');
            }else{
                return redirect('/pengumuman');
            }
            
        }
        $request->session()->flash('LoginError', 'Login Gagal !');
        return back()->with('LoginError', 'Login Gagal !');
    }

    public function logout(){

        Auth::logout();

        request()->session()->invalidate();

        request()->session()->regenerateToken();

        return redirect('/');
    }
}

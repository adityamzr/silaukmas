<?php

namespace App\Http\Controllers;

use App\Models\ComboBox;
use Illuminate\Http\Request;

class ComboBoxController extends Controller
{
    public function index(){
        return view('dashboard.data-master.index',[
            'comboBox' => ComboBox::all()
        ]);
    }
    public function tambah(){
        return view('dashboard.data-master.tambah');
    }
    public function store(Request $request){
        $validateData = $request->validate([
            'kategori' => 'required',
            'valueComboBox' => 'required',
        ]);

        ComboBox::create($validateData);

        return redirect('/data-master')->with('success','Berhasil !');
    }
    public function destroy($id){
        ComboBox::where('id', $id)->delete();

        return redirect('/data-master');
    }

    public function show(ComboBox $id){

        return view('dashboard.data-master.edit',[
            'combobox' => $id
        ]);
    }

    public function update(Request $request, $id)
    {
        $validateData = $request->validate([
            'kategori' => 'required',
            'valueComboBox' => 'required',
        ]);
        ComboBox::where('id', $id)->update($validateData);

    
        return redirect('/data-master')->with('success','Berhasil !');
    }
}

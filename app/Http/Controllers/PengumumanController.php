<?php

namespace App\Http\Controllers;

use App\Models\Jadwal;
use App\Models\Pemohon;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;

class PengumumanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $jadwal = Jadwal::where('id','1')->first();
        
        $pengajuan['awalProposal'] = Carbon::parse($jadwal->jadwalBuka)->isoFormat('D MMMM Y');
        $pengajuan['akhirProposal'] = Carbon::parse($jadwal->jadwalTutup)->isoFormat('D MMMM Y');
        $pengajuan['awalVerifikasi'] = Carbon::parse($jadwal->jadwalBukaVerifikasi)->isoFormat('D MMMM Y');
        $pengajuan['akhirVerifikasi'] = Carbon::parse($jadwal->jadwalTutupVerifikasi)->isoFormat('D MMMM Y');
        $pengajuan['awalPengambilan'] = Carbon::parse($jadwal->jadwalBukaPengambilan)->isoFormat('D MMMM Y');
        $pengajuan['akhirPengambilan'] = Carbon::parse($jadwal->jadwalTutupPengambilan)->isoFormat('D MMMM Y');

        return view('dashboard.pengumuman',[
            'pemohons' => Pemohon::where('userId', auth()->user()->id)->where('validasiPemohonan', '!=', '0')->with(['administrasi','benih'])->get(),
            'jadwal' => $jadwal,
            'pengajuan' => $pengajuan
            // 'message' => $message
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

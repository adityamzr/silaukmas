<?php

namespace App\Http\Controllers;

use App\Models\Pemohon;
use Illuminate\Http\Request;

class LaporanController extends Controller
{
    public function index(){
        return view('dashboard.laporan.laporan', [
            'proposals' =>  Pemohon::where('validasiPemohonan', '2')->with(['administrasi','benih','user', 'lokasi', 'kolam', 'biovlog'])->get()
        ]);
    }
    public static function lokasi($id){
        if($id == '1'){
            return 'Kolam';
        }else if($id == '2'){
            return 'Sungai';
        }else if($id == '3'){
            return 'Danau / Situ';
        }else if($id == '4'){
            return 'biovlog';
        }
    }
}
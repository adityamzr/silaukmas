<?php

namespace App\Http\Controllers;

use App\Models\Jadwal;
use App\Models\Pemohon;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;

class DashboardController extends Controller
{
    public function index(){

        $jadwal = Jadwal::where('id','1')->first();
        
        $pengajuan['awalProposal'] = Carbon::parse($jadwal->jadwalBuka)->isoFormat('D MMMM Y');
        $pengajuan['akhirProposal'] = Carbon::parse($jadwal->jadwalTutup)->isoFormat('D MMMM Y');
   
        return view('dashboard.index',[
            'pemohons' => Pemohon::all(),
            'pengajuan' => $pengajuan
        ]);
    }
}

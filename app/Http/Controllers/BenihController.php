<?php

namespace App\Http\Controllers;

use App\Models\Benih;
use App\Models\Pemohon;
use App\Models\ComboBox;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class BenihController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $no = Session::get('noProposal');
       
        return view('dashboard.pengajuan.pemohonan-benih',[
            'judul' => auth()->user()->kelompokMasyarakat,
            'pemohonan' => Benih::where('noProposal', $no)->first(),
            'jenisBenihs' => ComboBox::where('kategori', 'Jenis Benih')->get()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $no = Session::get('noProposal');
        $validateData = $request->validate([
            '*' => 'required',
            // 'suratPemohon' => 'mimetypes:application/pdf',
        ],[
            'required' => 'Tidak Boleh Kosong !',
            'mimetypes' => 'Format Harus .pdf !'
        ]);
        unset($validateData['_token']);
        $validateData['noProposal'] = $no;

      
        $pemohon['syaratPemohonan'] = 1;
        $pemohon['validasiPemohonan'] = 1;
        Pemohon::where('noProposal', $no)->update($pemohon);
        if(Benih::where('noProposal', $no)->count() > 0){
            Benih::where('noProposal', $no)->update($validateData);
        }else{
            Benih::create($validateData);
        }
        // session()->forget('noProposal');
        return redirect('/pengajuan-proposal/lampiran')->with('success','Berhasil !');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

<?php

namespace App\Http\Controllers;

use App\Models\Benih;
use App\Models\Jadwal;
use App\Models\Lokasi;
use App\Models\Teknis;
use App\Models\Anggota;
use App\Models\LatLong;
use App\Models\Pemohon;
use App\Models\Administrasi;
use Illuminate\Http\Request;

class DataProposalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('dashboard.data-proposal.index',[
            'proposals' =>  Pemohon::where('validasiPemohonan', '!=', '0')->with(['administrasi','benih','user'])->get() 
        ]);
    }
    public function jadwal()
    {
        return view('dashboard.jadwal.index',[
            'jadwal' =>  Jadwal::where('id','1')->first() 
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      //
    }

    public function maps()
    {
       return view('dashboard.peta-kolam', [
            'petas' => LatLong::with('administrasi')->get()
       ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }
    public function storeJadwal(Request $request)
    {
        $validateData = $request->validate([
            '*' => 'required'
        ]);
        unset($validateData['_token']);
        if($validateData['status'] == 0){
            $validateData['jadwalBuka'] = null;
            $validateData['jadwalTutup'] = null;
            $validateData['jadwalBukaVerifikasi'] = null;
            $validateData['jadwalTutupVerifikasi'] = null;
            $validateData['jadwalBukaPengambilan'] = null;
            $validateData['jadwalTutupPengambilan'] = null;
        }

        Jadwal::where('id', '1')->update($validateData);

        return redirect()->back()->with('success', 'berhasil !');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
       $administrasi =  Administrasi::with('latlong')->where('noProposal', $id)->first();
       $proposal = $administrasi->jenisProposal;

        return view('dashboard.data-proposal.data',[
            'administrasi' => $administrasi,
            'teknis' => Teknis::where('noProposal', $id)->first(),
            'lokasi' => Lokasi::where('noProposal', $id)->first(),
            'anggotas' => Anggota::where('noProposal', $id)->get(),
            'pemohonan' => Benih::where('noProposal', $id)->first(),
            'proposal' => $proposal,
        ]);
    }
    public function showTerima($id)
    {
       
        return view('dashboard.data-proposal.terima',[
            'noProposal' => $id,
            'administrasi' => $administrasi =  Administrasi::where('noProposal', $id)->first(),
            'benih' => $benih =  Benih::where('noProposal', $id)->first()
            
        ]);
    }
    public function tolakProposal(Request $request)
    {
        $validateData['validasiPemohonan'] = 3;
        $validateData['keterangan'] = $request['keterangan'];
 
        Pemohon::where('noProposal', $request['noProposal'])->update($validateData);
 
        return redirect('/data-proposal');
    }
    public function konfirmasiProposal(Request $request, $id)
    {
       $validateData = $request->validate([
           '*' => 'required'
       ],[
           'required' => 'Tidak Boleh Kosong !'
       ]);
  
       unset($validateData['_token']);
       $validateData['validasiPemohonan'] = 2;

       Pemohon::where('noProposal', $id)->update($validateData);

       return redirect('/data-proposal');
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

<?php

namespace App\Http\Controllers;

use App\Models\Pemohon;
use App\Models\Administrasi;
use Illuminate\Http\Request;

class ViewController extends Controller
{
    public function view($jenis, $noProposal){
        $db =  Pemohon::where('noProposal' , $noProposal)->with(['administrasi','teknis','benih','lokasi'])->first();
        if($jenis == 'ktp'){
            $path = $db->administrasi->fotoKtp;
        }else if($jenis == 'kesanggupan'){
            $path = $db->administrasi->fileKesanggupan;
        }else if($jenis == 'bantuan-sejenis'){
            $path = $db->administrasi->fileTidakMenerimaBantuan;
        }else if($jenis == 'bukti-terdaftar'){
            $path = $db->administrasi->fileBukti;
        }else if($jenis == 'bukti-hukum-adat'){
            $path = $db->administrasi->dokumenHukumAdat;
        }else if($jenis == 'bukti-pendidikan'){
            $path = $db->administrasi->dokumenPendidikan;
        }else if($jenis == 'bukti-keagamaan'){
            $path = $db->administrasi->dokumenKeagamaan;
        }else if($jenis == 'bukti-swadaya'){
            $path = $db->administrasi->dokumenSwadaya;
        }else if($jenis == 'surat'){
            $path = $db->teknis->dokumenSurat;
        }else if($jenis == 'sertifikat'){
            $path = $db->teknis->dokumenSertifikat;
        }else if($jenis == 'bukti-lahan'){
            $path = $db->lokasi->kondisiJalan;
        }else if($jenis == 'bukti-kondisi-lahan'){
            $path = $db->lokasi->buktiKondisiLahan;
        }else if($jenis == 'surat-pemohon'){
            $path = $db->benih->suratPemohon;
        }
        $storagePath = storage_path('app/public/'.$path);

        return response()->file($storagePath);
    }
}

<?php

namespace App\Http\Controllers;
use App\Models\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function update(Request $request, $id){
        
        $validateData = $request->validate([
            'namaLengkap' => 'required',
            'email' => 'required',
            'noHp' => 'required'
        ],[
            'required' => 'Tidak Boleh Kosong !!'
        ]
        );


        User::where('id', $id)->update($validateData);
        return redirect('/pengumuman')->with('success', 'Berhasil !');

    } 
    
    public function show($id){
        return view('dashboard.edit-profile', [
            'user' => User::find($id)
        ]);
    }
}

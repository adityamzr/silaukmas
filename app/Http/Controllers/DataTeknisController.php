<?php

namespace App\Http\Controllers;

use App\Models\Teknis;
use App\Models\Pemohon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class DataTeknisController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $no = Session::get('noProposal');
        
        return view('dashboard.pengajuan.data-teknis',[
            'proposal' => auth()->user()->kelompokMasyarakat,
            'teknis' => Teknis::where('noProposal', $no)->first()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $no = Session::get('noProposal');
        $validateData = $request->validate([
            'dokumenSurat' => 'mimes:jpeg,bmp,png,gif,svg,pdf|max:2048',
            'dokumenSertifikat' => 'mimes:jpeg,bmp,png,gif,svg,pdf|max:2048',
        ],[
            'required' => 'Tidak Boleh Kosong !',
            'mimes' => 'Format Harus .png / .jpg / .jpeg / .pdf  !',
            'max' => 'Maksimal Ukuran File 1mb !',
        ]);

        if($request->hasFile('dokumenSurat')){
            $validateData['dokumenSurat'] = $request->file('dokumenSurat')->store('dokument-surat');
        }
        if($request->hasFile('dokumenSertifikat')){
            $validateData['dokumenSertifikat'] = $request->file('dokumenSertifikat')->store('dokument-sertifikat');
        }
        $validateData['noProposal'] = Session::get('noProposal');
        $pemohon['syaratTeknis'] = 1;
        Pemohon::where('noProposal', $no)->update($pemohon);

        if(Teknis::where('noProposal', $no)->count() > 0){
            Teknis::where('noProposal', $no)->update($validateData);
        }else{
            Teknis::create($validateData);
        }
        return redirect('/pengajuan-proposal/data-lokasi')->with('success', 'Berhasil !');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

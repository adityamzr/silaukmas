<?php

namespace App\Http\Controllers;

use App\Models\Pemohon;
use Illuminate\Http\Request;
use App\Models\AngketKepuasan;

class AngketKepuasanController extends Controller
{
    public function index(){

        if(auth()->user()->is_admin == 1){
            return view('dashboard.angket.angketAdmin',[
                'angkets' => AngketKepuasan::all()
            ]);
        }

        return view('dashboard.angket.angket',[
            'angkets' => Pemohon::where('userId', auth()->user()->id)->where('validasiPemohonan', '2')->orWhere('validasiPemohonan', '4')->get()
        ]);
    }
    public function ulasan($id){

       $validasi =  AngketKepuasan::where('noProposal', $id)->count();
        if($validasi > 0){
            return redirect()->back();
        }
        return view('dashboard.angket.ulasan',[
            'noProposal' => $id
        ]);
    }
    public function store(Request $request){
        $validateData = $request->validate([
            'rate' => 'required',
            'kritik' => '',
            'saran' => '',
            'noProposal' => '',
        ]);

        AngketKepuasan::create($validateData);
        // Pemohon::where('noProposal', $request['noProposal'])->update($pemohon);
        session()->forget('noProposal');

        return redirect('/pengambilan-benih')->with('success' , 'berhasil !');
    }
    public static function rating($no){
        if($no == 1){
            return 'Sangat Buruk';
        }else if($no == 2){
            return 'Buruk';
        }else if($no == 3){
            return 'Cukup';
        }else if($no == 4){
            return 'Baik';
        }else if($no == 5){
            return 'Sangat Baik';
        }
    }
}

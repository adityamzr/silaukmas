<?php

namespace App\Http\Controllers;

use App\Models\SK;
use Illuminate\Http\Request;

class SkController extends Controller
{
    public function index(){
        return view('dashboard.sk.index');
    }
    public function view(){
        $path = SK::where('id','1')->first();
        if($path){
            $storagePath = storage_path('app/public/'.$path['pathSurat']);
    
            return response()->file($storagePath);
        }else{
            abort(404);
        }
    }
    public function store(Request $request){

        if($request->hasFile('suratKebijakan')){
            $validateData['pathSurat'] = $request->file('suratKebijakan')->storeAs('dokument-suratKebijakan','Surat Kebijakan.pdf');
        }
        $validateData['id'] = 1;

        if(SK::count() == 0){
            SK::create($validateData);
        }else{
            SK::where('id','1')->update($validateData);
        }

        return redirect('/upload-surat')->with('success','berhasil');
    }
    public function download(){
        $path = SK::where('id','1')->first();
        if($path){
            $storagePath = storage_path('app/public/'.$path->pathSurat);
    
            return response()->download($storagePath);
        }else{
            abort(404);
        }
    }    
}

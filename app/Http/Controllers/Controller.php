<?php

namespace App\Http\Controllers;

use Illuminate\Http\Response;
use App\Models\Jadwal;
use Illuminate\Support\Carbon;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function index(){
        $jadwal = Jadwal::where('id','1')->first();
        
        $pengajuan['awalProposal'] = Carbon::parse($jadwal->jadwalBuka)->isoFormat('D MMMM Y');
        $pengajuan['akhirProposal'] = Carbon::parse($jadwal->jadwalTutup)->isoFormat('D MMMM Y');
        $pengajuan['awalVerifikasi'] = Carbon::parse($jadwal->jadwalBukaVerifikasi)->isoFormat('D MMMM Y');
        $pengajuan['akhirVerifikasi'] = Carbon::parse($jadwal->jadwalTutupVerifikasi)->isoFormat('D MMMM Y');
        $pengajuan['awalPengambilan'] = Carbon::parse($jadwal->jadwalBukaPengambilan)->isoFormat('D MMMM Y');
        $pengajuan['akhirPengambilan'] = Carbon::parse($jadwal->jadwalTutupPengambilan)->isoFormat('D MMMM Y');

        return view('index',[
            'pengajuan' => $pengajuan,
            'jadwal' => $jadwal,
        ]);
    }
    
    public function download(){
        $file= public_path(). "/test.pdf";

    $headers = array(
              'Content-Type: application/pdf',
            );

            return response()->download($file, 'filename.pdf', $headers);
    }
}

<?php

namespace App\Http\Controllers;

use App\Models\Danau;
use App\Models\Kolam;
use App\Models\Lokasi;
use App\Models\Sungai;
use App\Models\Biovlok;
use App\Models\LatLong;
use App\Models\Pemohon;
use App\Models\ComboBox;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class LokasiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $no = Session::get('noProposal');
        
        return view('dashboard.pengajuan.data-lokasi',[
            'judul' => auth()->user()->kelompokMasyarakat,
            'lokasi' => Lokasi::where('noProposal', $no)->first(),
            'kondisiJalans' => ComboBox::where('kategori', 'Kondisi Jalan')->get(),
            'aksesKendaraans' => ComboBox::where('kategori', 'Akses Jalan')->get(),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $no = Session::get('noProposal');
        $validateData = $request->validate([
            '*' => 'required',
            'buktiLahan' => 'mimes:jpeg,bmp,png,gif,svg,pdf|max:1028',
            'buktiKondisiLahan' => 'mimes:jpeg,bmp,png,gif,svg,pdf|max:1028',
        ],[
            'required' => 'Tidak Boleh Kosong !',
            'mimes' => 'Format Harus .png / .jpg / .jpeg / .pdf !',
            'max' => 'Maksimal Ukuran File 1mb !',
        ]);
        unset($validateData['_token']);
        
        $kolam = $request['kategoriKolam'];
    
        $kategori = $validateData;
        $kategori['noProposal'] = $no;
        unset($kategori['statusLahan']);
        unset($kategori['jarakLahan']);
        unset($kategori['kondisiJalan']);
        unset($kategori['aksesKendaraan']);
        unset($kategori['buktiLahan']);
        unset($kategori['buktiKondisiLahan']);
        unset($kategori['kategoriKolam']);
        unset($kategori['latitude']);
        unset($kategori['longitude']);

        $latlong['noProposal'] = $no;
        $latlong['latitude'] = $validateData['latitude'];
        $latlong['longitude'] = $validateData['longitude'];

        $lokasi['noProposal'] = $no;
        $lokasi['kategoriKolam'] = $validateData['kategoriKolam'];
        $lokasi['statusLahan'] = $validateData['statusLahan'];
        $lokasi['jarakLahan'] = $validateData['jarakLahan'];
        $lokasi['kondisiJalan'] = $validateData['kondisiJalan'];
        $lokasi['aksesKendaraan'] = $validateData['aksesKendaraan'];

        if($request->hasFile('buktiKondisiLahan')){
            $lokasi['buktiKondisiLahan'] = $request->file('buktiKondisiLahan')->store('dokument-buktiKondisiLahan');
        }
        if($request->hasFile('buktiLahan')){
            $lokasi['buktiLahan'] = $request->file('buktiLahan')->store('dokument-buktiLahan');
        }
        $pemohon['syaratLokasi'] = 1;
        Pemohon::where('noProposal', $no)->update($pemohon);

        if(Lokasi::where('noProposal', $no)->count() > 0){
            Lokasi::where('noProposal', $no)->update($lokasi);
            LatLong::where('noProposal', $no)->update($latlong);
        }else{
            Lokasi::create($lokasi);
            LatLong::create($latlong);
        }

        Kolam::where('noProposal', $no)->delete();
        Sungai::where('noProposal', $no)->delete();
        Danau::where('noProposal', $no)->delete();
        Biovlok::where('noProposal', $no)->delete();

        ($kolam == 1)? Kolam::create($kategori) : '';
        ($kolam == 2)? Sungai::create($kategori) : '';
        ($kolam == 3)? Danau::create($kategori) : '';
        ($kolam == 4)? Biovlok::create($kategori) : '';

        return redirect('/pengajuan-proposal/pemohonan-benih')->with('success','Berhasil !');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function ajaxKolam(Request $request){
        $kolam = $request['jenisKolam'];
        $no = Session::get('noProposal');

        if($kolam == 1){
            return view('dashboard.pengajuan.kategori.kolam',[
                'jenis' => $kolam,
                'kolam' => Kolam::where('noProposal', $no)->first(),
                'latlong' => LatLong::where('noProposal', $no)->first(),
                'bangunanKolams' => ComboBox::where('kategori', 'Bangunan Kolam')->get(),
            ]);
        }else if($kolam == 2){
            return view('dashboard.pengajuan.kategori.sungai',[
                'jenis' => $kolam,
                'sungai' => Sungai::where('noProposal', $no)->first(),
                'latlong' => LatLong::where('noProposal', $no)->first()
            ]);
        }else if($kolam == 3){
            return view('dashboard.pengajuan.kategori.danau',[
                'jenis' => $kolam,
                'danau' => Danau::where('noProposal', $no)->first(),
                'latlong' => LatLong::where('noProposal', $no)->first()
            ]);
        }else if($kolam == 4){
            return view('dashboard.pengajuan.kategori.biovlok',[
                'jenis' => $kolam,
                'biovlok' => Biovlok::where('noProposal', $no)->first(),
                'latlong' => LatLong::where('noProposal', $no)->first()
            ]);
        }
        
        
        
    }
    public function ajaxKolam2(Request $request){
        $kolam = $request['jenisKolam'];
        $no = $request['noProposal'];

        if($kolam == 1){
            return view('dashboard.pengajuan.kategori.kolam',[
                'jenis' => $kolam,
                'kolam' => Kolam::where('noProposal', $no)->first()
            ]);
        }else if($kolam == 2){
            return view('dashboard.pengajuan.kategori.sungai',[
                'jenis' => $kolam,
                'sungai' => Sungai::where('noProposal', $no)->first()
            ]);
        }else if($kolam == 3){
            return view('dashboard.pengajuan.kategori.danau',[
                'jenis' => $kolam,
                'danau' => Danau::where('noProposal', $no)->first()
            ]);
        }else if($kolam == 4){
            return view('dashboard.pengajuan.kategori.biovlok',[
                'jenis' => $kolam,
                'biovlok' => Biovlok::where('noProposal', $no)->first()
            ]);
        }
        
        
        
    }
}

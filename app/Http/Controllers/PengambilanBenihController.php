<?php

namespace App\Http\Controllers;

use App\Models\Pemohon;
use Illuminate\Http\Request;
use PDF;

class PengambilanBenihController extends Controller
{
    public function index(){
        return view('dashboard.pengambilan-benih',[
            'pemohons' => Pemohon::where('userId', auth()->user()->id)->where('validasiPemohonan', '!=', '0')->with(['administrasi','benih'])->get() 
        ]);
    }
    public function createPDF($id) {
        view()->share('pdf_view');
        $pdf = PDF::loadView('pdf_view',[
            'pemohons' => Pemohon::where('noProposal' , $id)->with(['administrasi','benih','user'])->first()
        ]);

        // download PDF file with download method
        return $pdf->download('Surat Pengambilan Benih.pdf');
      }
    

}



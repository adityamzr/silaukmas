<?php

namespace App\Http\Controllers;

use App\Models\Anggota;
use App\Models\Pemohon;
use Illuminate\Support\Str;
use App\Models\Administrasi;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use PDF;
use App\Models\Benih;

class AdministrasiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Session::put('noProposal','75sVUr9fYd');
        $no = Session::get('noProposal');
        
        $proposal = auth()->user()->kelompokMasyarakat;
    

    return view('dashboard.pengajuan.data-administrasi',[
        'proposal' => $proposal,
        'administrasi' => Administrasi::where('noProposal', $no)->first(),
        'anggotas' => Anggota::where('noProposal', $no)->get(),
    ]);

    }
    public function flush(){
         session()->flush();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $validateData = $request->validate([
            '*' => 'required',
            'fileBukti' => 'mimetypes:application/pdf|max:2048',
            'fotoKtp' => 'mimetypes:application/pdf|max:2048',
            'fileKesanggupan' => 'mimetypes:application/pdf|max:2048',
            'fileTidakMenerimaBantuan' => 'mimetypes:application/pdf|max:2048',
            'dokumenHukumAdat' => 'mimetypes:application/pdf|max:2048',
        ],[
            'required' => 'Tidak Boleh Kosong !',
            'mimetypes' => 'Format Harus .pdf !',
            'max' => 'File Tidak Boleh Lebih Dari 2Mb !',
        ]);
        $validate = 1;
        if(!Session::get('noProposal')){
            $validate = 0;
            Session::put('noProposal', Str::random(10));
        }
        if($request->hasFile('fotoKtp')){
            $validateData['fotoKtp'] = $request->file('fotoKtp')->store('dokument-ktp');
        }
        
        if($request->hasFile('fileBukti')){
            $validateData['fileBukti'] = $request->file('fileBukti')->store('dokument-bukti');
        }
        if($request->hasFile('dokumenHukumAdat')){
            $validateData['dokumenHukumAdat'] = $request->file('dokumenHukumAdat')->store('dokument-hukumadat');
        }
        $validateData['noProposal'] = Session::get('noProposal');

        unset($validateData['_token']);
        unset($validateData['namaAnggota']);
        unset($validateData['jmlAnggota']);

        $pemohon['userId'] = auth()->user()->id;;
        $pemohon['noProposal'] = Session::get('noProposal');
        $pemohon['syaratAdministrasi'] = 1;
        $pemohon['validasiPemohonan'] = 0;

       

        if($validate == 0 ){
            Pemohon::create($pemohon);
            Administrasi::create($validateData);
        }else{
            Administrasi::where('noProposal', $validateData['noProposal'])->update($validateData);
        }
        if($request['namaAnggota']){
            Anggota::where('noProposal', Session::get('noProposal'))->delete();
            for($i = 0 ; $i < count($request['namaAnggota']) ; $i++){
                if($request['namaAnggota'][$i]){
                    $anggota['noProposal'] = Session::get('noProposal');
                    $anggota['namaAnggota'] = $request['namaAnggota'][$i];
                    Anggota::create($anggota);
                }
            }
        }

        return redirect('/pengajuan-proposal/data-teknis')->with('success', 'Berhasil !');

    }

    public function storeLampiran(Request $request){
        $no = Session::get('noProposal');

        $validateData = $request->validate([
            'suratPemohon' => 'mimetypes:application/pdf|max:2048',
            'fileKesanggupan' => 'mimetypes:application/pdf|max:2048',
            'fileTidakMenerimaBantuan' => 'mimetypes:application/pdf|max:2048'
        ],[
            'required' => 'Tidak Boleh Kosong !',
            'mimetypes' => 'Format Harus .pdf !',
            'max' => 'File Tidak Boleh Lebih Dari 2Mb !',
        ]);

        if($request->hasFile('fileKesanggupan')){
            $administrasi['fileKesanggupan'] = $request->file('fileKesanggupan')->store('dokument-kesanggupan');
        }
        if($request->hasFile('fileTidakMenerimaBantuan')){
            $administrasi['fileTidakMenerimaBantuan'] = $request->file('fileTidakMenerimaBantuan')->store('dokument-tidakmenerimabantuan');
        }
        if($request->hasFile('suratPemohon')){
            $benih['suratPemohon'] = $request->file('suratPemohon')->store('dokument-suratPemohon');
        }

        Administrasi::where('noProposal', $no)->update($administrasi);
        Benih::where('noProposal', $no)->update($benih);

        return redirect('/pengajuan-proposal/angket-kepuasan/'.$no);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function batal()
    {
        session()->forget('noProposal');

        return redirect('/pengambilan-benih');
    }

    public function lampiran(){
        $no = Session::get('noProposal');
        return view('dashboard.pengajuan.lampiran',[
            'administrasi' => Administrasi::where('noProposal', $no)->first(),
            'benih' => Benih::where('noProposal', $no)->first(),
        ]);
    }
    
    public function kesanggupanPdf(){
        return response()->download(public_path('doc/Contoh Surat Usulan Bantuan Benih dari Kelompok.docx'));
      
    }
    // public function kesanggupanPdf(){
    //     $no = Session::get('noProposal');
    //     view()->share('dashboard.pengajuan.blanko.pernyataan_kesanggupan');
    //     $pdf = PDF::loadView('dashboard.pengajuan.blanko.pernyataan_kesanggupan',[
    //         'administrasi' => Administrasi::where('noProposal', $no)->first(),
    //     ]);

    //     // download PDF file with download method
    //     return $pdf->download('Penyataan Kesanggupan Pemohon.pdf');
      
    // }

    public function bantuanPdf(){

        // download PDF file with download method
        return response()->download(public_path('doc/Surat Pernyataan KelompokLembaga Calon Penerima Bantuan.docx'));
      
    }
    // public function bantuanPdf(){
    //     $no = Session::get('noProposal');

    //     view()->share('dashboard.pengajuan.blanko.bantuan');
    //     $pdf = PDF::loadView('dashboard.pengajuan.blanko.bantuan',[
    //         'administrasi' => Administrasi::where('noProposal', $no)->first(),
    //     ]);

    //     // download PDF file with download method
    //     return $pdf->download('Pernyataan Tidak Menerima Bantuan Sejenis.pdf');
      
    // }

    public function benihPdf(){

        return response()->download(public_path('doc/Contoh Surat Usulan Bantuan Benih dari Dinas KabKota.docx'));
      
    }
    // public function benihPdf(){
    //     $no = Session::get('noProposal');

    //     view()->share('dashboard.pengajuan.blanko.bantuan');
    //     $pdf = PDF::loadView('dashboard.pengajuan.blanko.benih',[
    //         'administrasi' => Administrasi::where('noProposal', $no)->first(),
    //         'benih' => Benih::where('noProposal', $no)->first(),
    //     ]);

    //     // download PDF file with download method
    //     return $pdf->download('Surat Permohonan Benih.pdf');
      
    // }


  
}

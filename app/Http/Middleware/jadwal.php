<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class jadwal
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        $jadwal = Jadwal::where('id','1')->first();

        if (Carbon::now()->between($jadwal->jadwalBuka, $jadwal->jadwalTutup)) {
            return $next($request);
        }
    }
}

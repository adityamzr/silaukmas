<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\Jadwal;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;

class checkjadwal
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        $jadwal = Jadwal::where('id','1')->first();

        if($jadwal->status == 1){
            if (Carbon::now()->between($jadwal->jadwalBuka, $jadwal->jadwalTutup)) {
                return $next($request);
            }else{
                $from = Carbon::parse($jadwal->jadwalBuka)->isoFormat('D MMMM Y');
                $to = Carbon::parse($jadwal->jadwalTutup)->isoFormat('D MMMM Y');

                $message = 'Jadwal Pengajuan Dibuka Dari '.$from.' Hingga '.$to;
                return response()->view('dashboard.tutup',[
                    'message' => $message
                ]);
            }
        }else{
            return response()->view('dashboard.tutup',[
                'message' => 'Pengajuan Proposal Sedang Ditutup !'
            ]);
        }
       
    }
}

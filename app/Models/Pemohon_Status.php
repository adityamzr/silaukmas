<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pemohon_Status extends Model
{
    use HasFactory;
    protected $guarded = [];
}

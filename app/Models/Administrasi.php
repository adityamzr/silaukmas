<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Administrasi extends Model
{
    use HasFactory;
    protected $guarded = [];

    public function latlong()
    {
        return $this->belongsTo(LatLong::class, 'noProposal', 'noProposal');
    }
}

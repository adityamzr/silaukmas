<?php

namespace App\Models;

use App\Models\Benih;
use App\Models\Lokasi;
use App\Models\Teknis;
use App\Models\Biovlok;
use App\Models\Administrasi;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Pemohon extends Model
{
    use HasFactory;
    protected $guarded = [];

    public function administrasi()
    {
        return $this->belongsTo(Administrasi::class, 'noProposal', 'noProposal');
    }
    public function benih()
    {
        return $this->belongsTo(Benih::class, 'noProposal', 'noProposal');
    }
    public function teknis()
    {
        return $this->belongsTo(Teknis::class, 'noProposal', 'noProposal');
    }
    public function lokasi()
    {
        return $this->belongsTo(Lokasi::class, 'noProposal', 'noProposal');
    }
    public function kolam()
    {
        return $this->belongsTo(Kolam::class, 'noProposal', 'noProposal');
    }
    public function biovlog()
    {
        return $this->belongsTo(Biovlok::class, 'noProposal', 'noProposal');
    }
    public function user()
    {
        return $this->belongsTo(User::class, 'userId');
    }
}

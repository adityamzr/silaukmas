<?php

use App\Models\Jadwal;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\SkController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\ViewController;
use App\Http\Controllers\BenihController;
use App\Http\Controllers\LokasiController;
use App\Http\Controllers\LaporanController;
use App\Http\Controllers\ComboBoxController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\DataTeknisController;
use App\Http\Controllers\PengumumanController;
use App\Http\Controllers\AdministrasiController;
use App\Http\Controllers\DataProposalController;
use App\Http\Controllers\AngketKepuasanController;
use App\Http\Controllers\PengambilanBenihController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/',[Controller::class , 'index']);

Route::get('/login', function () {
    return view('auth.login');
});
Route::get('/register', function () {
    return view('auth.register');
});

Route::post('/register', [AuthController::class, 'register'])->name('register');

Route::post('/login', [AuthController::class, 'authenticate'])->name('login');

Route::post('/logout', [AuthController::class, 'logout']);

Route::middleware(['auth', 'AdminAuth'])->group(function () {
    
    Route::get('/dashboard', [DashboardController::class, 'index']);

    Route::get('/data-proposal', [DataProposalController::class, 'index']);

    Route::get('/data-master', [ComboBoxController::class, 'index']);

    Route::get('/data-master/delete/{id}', [ComboBoxController::class, 'destroy']);

    Route::post('/data-master', [ComboBoxController::class, 'store'])->name('data-master');

    Route::get('/data-master/tambah', [ComboBoxController::class, 'tambah']);

    Route::get('/data-master/edit/{id}', [ComboBoxController::class, 'show']);

    Route::post('/data-master/edit/{id}', [ComboBoxController::class, 'update']);
    
    Route::get('/data-proposal/{id}', [DataProposalController::class, 'show']);

    Route::get('/jadwal-proposal', [DataProposalController::class, 'jadwal']);

    Route::post('/jadwal-proposal', [DataProposalController::class, 'storeJadwal'])->name('jadwal');

    Route::get('/upload-surat', [SkController::class, 'index']);

    Route::post('/ubah-surat', [SkController::class, 'store'])->name('surat');

    Route::get('/view-surat', [SkController::class, 'view']);

    Route::get('/view/{jenis}/{noProposal}', [ViewController::class, 'view']);

    Route::get('/terima/{id}', [DataProposalController::class, 'showTerima']);

    Route::post('/tolak-proposal', [DataProposalController::class, 'tolakProposal']);

    Route::post('/konfirmasi-proposal/{id}', [DataProposalController::class, 'konfirmasiProposal']);

    Route::get('/maps', [DataProposalController::class , 'maps']);

    Route::get('/laporan', [LaporanController::class, 'index']);

    
});

Route::middleware(['auth','checkjadwal'])->group(function () {

    Route::get('/pengajuan-proposal/data-administrasi', [AdministrasiController::class, 'index']);
        
    Route::get('/pengajuan-proposal/data-teknis', [DataTeknisController::class, 'index']);

    Route::get('/pengajuan-proposal/data-lokasi', [LokasiController::class, 'index']);

    Route::get('/pengajuan-proposal/pemohonan-benih', [BenihController::class, 'index']);
    
    Route::get('/pengajuan-proposal/lampiran', [AdministrasiController::class, 'lampiran']);
});

Route::middleware(['auth'])->group(function () {

    
    Route::get('/pengambilan-benih', [PengambilanBenihController::class, 'index']);
    
    Route::get('/batal', [AdministrasiController::class, 'batal']);
    
    Route::get('/angket-kepuasan', [AngketKepuasanController::class, 'index']);

    Route::get('pengajuan-proposal/angket-kepuasan/{id}', [AngketKepuasanController::class, 'ulasan']);

    Route::post('/ulasan', [AngketKepuasanController::class, 'store'])->name('ulasan');

    // Downloadfile
    Route::get('/download', [Controller::class , 'download']);

    Route::get('/downloadSuratKebijakan', [SkController::class, 'download']);

    Route::get('/flush', [AdministrasiController::class , 'flush']);

    Route::get('/pengumuman', [PengumumanController::class , 'index']);

    // ROUTE POST 

    Route::get('/downloadSurat/{id}', [PengambilanBenihController::class , 'createPDF']);

    Route::post('/administrasi', [AdministrasiController::class, 'store'])->name('administrasi');

    Route::post('/lampiran', [AdministrasiController::class, 'storeLampiran'])->name('lampiran');

    Route::get('/administrasi/pernyataan-kesanggupan-Pdf', [AdministrasiController::class, 'kesanggupanPdf']);

    Route::get('/administrasi/bantuan-Pdf', [AdministrasiController::class, 'bantuanPdf']);

    Route::get('/administrasi/benih-Pdf', [AdministrasiController::class, 'benihPdf']);

    // Route::get('/administrasi/pernyataan-kesanggupan-Pdf', [AdministrasiController::class, 'kesanggupanPdf']);

    // Route::get('/administrasi/bantuan-Pdf', [AdministrasiController::class, 'bantuanPdf']);

    // Route::get('/administrasi/benih-Pdf', [AdministrasiController::class, 'benihPdf']);

    Route::post('/teknis', [DataTeknisController::class, 'store'])->name('teknis');

    Route::post('/lokasi', [LokasiController::class, 'store'])->name('lokasi');

    Route::post('/benih', [BenihController::class, 'store'])->name('benih');

    Route::post('/ajaxKolam', [LokasiController::class, 'ajaxKolam']);

    Route::post('/ajaxKolam2', [LokasiController::class, 'ajaxKolam2']);

    Route::get('/edit-profile/{id}', [UserController::class, 'show']);

    Route::post('/edit-profile/{id}', [UserController::class, 'update']);

});